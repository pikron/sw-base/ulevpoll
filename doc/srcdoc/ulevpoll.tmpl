<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
                    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd"
[]>

<book id="ULEVPOLL">
 <bookinfo>
  <title>uLan/Universal Light Event Poll Library (ULEVPOLL)</title>
  
  <authorgroup>
   <author>
    <firstname>Pavel</firstname>
    <surname>Pisa</surname>
    <affiliation>
     <address>
      <email>pisa@cmp.felk.cvut.cz</email>
     </address>
    </affiliation>
   </author>
  </authorgroup>

  <copyright>
   <year>2009-2010</year>
   <holder>Pavel Pisa</holder>
  </copyright>

  <abstract>
   <para>The uLevPoll library provides infrastructure to process system
   level events in applications with well-defined and portable triggers
   register and modification operations.
   </para>
  </abstract>

 </bookinfo>

 <toc></toc>
 <chapter id="description">
  <title>Event Processing Library Concepts</title>
  <sect1 id="ulevpollgoals"><title>The Goals</title>
   <para>The processing of system level events is fundamental need
    of most of the applications. When multiple events should be processed
    in a single thread some mechanism to allows select which events should
    be wait for and how they should be processed is required.
   </para>
   <para>Many of projects are assembled from multiple libraries/components.
    These libraries should be portable and need to be able to integrate into
    different environments and applications. Some/many libraries can be written
    without a need to block on events or access system level data channels.
    The library is fed by data from the main application and provide data
    back (for example zlib). But there are many situations when even libraries
    depend on processing system level events and need to register
    into application wide events processing mechanism.
   </para>
   <para>
    But there is a critical problem for libraries that they have to follow
    application environment selected event processing mechanism or introduce
    own one and force the application use it. The problem is to combine
    libraries written for different environments or to select different
    application environment the library has been designed for.
   </para>
   <para>
    The goal of uLevPoll is to provide a common interface which allows
    to hide environment differences and allows to write libraries which
    can be used in different environments without the need of rewrite
    or recompile.
   </para>
   <para>
    Next list of requirements has been defined to achieve goal
   </para>
   <itemizedlist>
    <listitem>
     <para>use such FD monitoring mechanism, which would be well portable
     </para>
    </listitem>
    <listitem>
     <para>even a binary version of compiled libraries has to be independent
      of the application selected main loop mechanism used by applications
      which use components/libraries &rarr; libraries have to adapt
      for the main loop used by applications
     </para>
    </listitem>
    <listitem>
     <para>libraries should allow being used with a minimal set of external
      dependencies to allow their use in small embedded applications
     </para>
    </listitem>
    <listitem>
     <para>but components should integrate well even with graphical
      or large applications, so defined interface should not prevent
      the use of Gtk or Qt for the main loop in applications
     </para>
    </listitem>
    <listitem>
     <para>the used mechanism should allow switching to high throughput
      solution (as libevent is for example) when required in future.
     </para>
    </listitem>
   </itemizedlist>
   <para>The uLevPoll library API/ABI is defined on above basis.
    It exposes the minimal amount of information directly to the
    user - only "handler" like event trigger structure with
    the minimal set of fields and the pointer to one field
    of event base structure with information about used operations
    set.
   </para>
  </sect1>
 
  <sect1><title>Cascading of Event Processing Implementations</title>
   <para>The other interesting feature is to be able to switch or cascade
    event monitoring at runtime when some third party library enforcing
    different main loop implementation is dynamically loaded. This goal
    has been achieved by uLevPoll as well.
    Next complex scenarios work now
   </para>

   <itemizedlist>
    <listitem>
     <para>start an application with Linux epoll or Sys V poll base,
      when GLIB based library is required, create new uLevPoll based
      on GLIB, cascade original set with epoll above it (or transform
      Sys V poll triggers to GLIB based ones in a new main loop)
      and continue to run with GLIB main loop.
     </para>
    </listitem>
    <listitem>
     <para>start with GLIB base, wrap it as the uLevPoll base and
      when GLIB scalability fails, create new uLevPoll based
      on Linux epoll which can be cascaded into GLIB main loop
      and use this new better scaling base for most of the events
      registration.
     </para>
    </listitem>
   </itemizedlist>

   <para>The events can be inserted by GLIB based applications
    as glib event sources, by uLUt based applications as ul_evpoll
    events into uLevPoll wrapper or over original sysvpoll
    or lnxepoll event bases and all runs concurrently without
    noticing real bottom base in use. The Linus epoll cascaded
    over GLIB base can correct GLIB harmful behavior for the C10K
    problem for these events, which are registered over uLevPoll
    API as ul_evptrig_t into epoll based ul_evpbase_t.
   </para>
  </sect1>
  <sect1><title>History</title>
   <para>We have a need for system events/file handles monitoring
    for uLan project and other PiKRON company projects in 2007.
    The selected solution should provide the functionality of other
    older Sys V poll based code included in OCERA project CAN/CANopen
    VCA component as well.
   </para>
   <para>The libevent looked like a good candidate for our projects
    at that time, even that it would add yet another prerequisite
    for our projects. It was considered acceptable. But the goal
    of our libraries/components was to allow their combination
    in the environment based on other main-loop implementation
    (Qt, GTK, Python). But libevent enforces its own main loop
    and prevents to use libraries based on it to integrate into
    other environment main loop mechanism. This was considered
    as a fundamental problem and the goals (<xref linkend="ulevpollgoals"/>)
    for required solution has been defined.
   </para>
   <para>
    The minimal API conforming these requirements and allowing separation
    of libraries code from used system event processing method has been defined.
   </para>
   <para>Then the simple Sys V poll based implementation has been provided
    to allow stand-alone use of the API without enforcing external dependencies.
    The use of libevent-1 has been considered as next target. But after
    deeper look at libevent-1 code distributed with Debian stable, the
    analysis shown, that it is unusable for multi-threaded  environment -
    event_base_new() has not been provided by that version and sequence
    for creation and attaching of events to non default base has been
    considered strange as well.
   </para>
   <para>For above reasons, the own epoll based mechanism was implemented
    for uLevPoll. During its testing some misbehavior in epoll Linux kernel
    implementation has been found. Davide Libenzi has kindly provided help
    and result is  enhancement in Linux epoll implementation and introduction
    of keyed wake-ups which lead to significant speedup even for plain
    blocking read and write socket operations.
   </para>
   <para>Next experiment was to try if designed ABI really allows components
    to be compatible with Gtk/Glib and Qt. Fortunately, usual distributions
    Qt builds use Glib main loop so only support for that was added to uLevPoll.
    It allows to hide Glib event sources based API under uLevPoll API
    for our libraries which can then transparently use Glib main loop
    without notice of that. Yet for different threads better-performing
    epoll base main loop can be used. Even for main thread event loop
    it is possible to cascade over uLevPoll Glib abstraction another
    uLevPoll base with a different mechanism (epoll for example) which
    is a great win because Glib main loop has horrible scalability.
   </para>
   <para>Then the time to finally try move to libevent come.
    But version 1 has been disappointing. But new development version 2
    shows in much better light. It really allows multi-threaded support
    and when more available by distributions, it would allow to use it
    as high performance mechanism for uLevPoll. uLevPoll wrapper code
    has been adapted for libevent 2 now.
   </para>
  </sect1>
 </chapter>

 <chapter id="funcdes">
  <title>Functions Description</title>
  <sect1><title>Basic Level Public API</title>
!F../../ulevpoll/ul_evpoll.h
  </sect1>
 </chapter>
</book>
