/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpoll_default.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include "ul_evpoll.h"
#include "ul_evpoll_config.h"

#if defined(CONFIG_OC_UL_EVP_SYSVPOLL)
extern const ul_evpoll_ops_t ul_evpoll_ops_sysvpoll;
const ul_evpoll_ops_t *ul_evpoll_ops_compile_default = &ul_evpoll_ops_sysvpoll;
#elif  defined(CONFIG_OC_UL_EVP_SELECT)
extern const ul_evpoll_ops_t ul_evpoll_ops_select;
const ul_evpoll_ops_t *ul_evpoll_ops_compile_default = &ul_evpoll_ops_select;
#endif
