/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evp_winevent.h	- monitoring of open file handles

  (C) Copyright 2005-2012 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVP_WINEVENT_H
#define _UL_EVP_WINEVENT_H

#include "ul_utdefs.h"

#include <string.h>

#include "ul_evpoll.h"
#include <ul_list.h>
#include <ul_htimer.h>
#include <ul_gavl.h>

#ifdef __cplusplus
extern "C" {
#endif

#define EVP_WINEVENT_ARMED    0x01
#define EVP_WINEVENT_ONCE     0x02
#define EVP_WINEVENT_TIMED    0x04
#define EVP_WINEVENT_TIMEOUT  0x08
#define EVP_WINEVENT_PERIODIC 0x10

#define EVP_WINEVENT_EVMAX MAXIMUM_WAIT_OBJECTS

/*
 * Provide cascade support
 */
//#define UL_EVP_WINEVENT_CASCADE

/*
 * Use of delayd delete for fdnodes prevents above problem as well
 */
#define UL_EVP_WINEVENT_DELAYDEL

#define EVP_WINEVENT_EVRAW 1
#define EVP_WINEVENT_EVFD  2

typedef struct ul_evpwinevent_evdata_t {
  int            evtype;
  unsigned int   evidx;
} ul_evpwinevent_evdata_t;

typedef struct ul_evpwinevent_evdataraw_t {
  ul_evpwinevent_evdata_t evdata;
  ul_evptrig_t *evptrig;
} ul_evpwinevent_evdataraw_t;

typedef struct ul_evpwinevent_evdatafd_t {
  ul_evpwinevent_evdata_t evdata;
  ul_list_head_t fdnode_list;
  int            fdnode_cnt;
  int            fd_armed_cnt;
} ul_evpwinevent_evdatafd_t;

typedef struct ul_evpwinevent_fdnode_t {
  ul_list_head_t samefd_list;
  gavl_node_t    node;
  ul_evfd_t      fd;
  long           for_netevents;
  long           active_netevents;
  ul_evpwinevent_evdatafd_t *evdatafd;
  ul_list_node_t sameev_node;
 #ifdef UL_EVP_WINEVENT_DELAYDEL
  ul_list_node_t delaydel_node;
  unsigned int delaydel_generation;
 #endif
} ul_evpwinevent_fdnode_t;

typedef struct ul_evptrig_data_t {
  unsigned flags;
  unsigned what_events;
  unsigned pending_events;

  ul_evpwinevent_fdnode_t *fdnode;

  ul_evpwinevent_evdataraw_t *evdataraw;

  ul_htim_diff_t timeout;
  ul_htim_node_t htim;

  ul_list_node_t samefd_node;
  ul_list_node_t list_node;

  ul_evptrig_t *trig_ptr;

} ul_evptrig_winevent_t;

typedef struct ul_evpbase_winevent_t {
  ul_evpbase_t base;

  int time_next_expire_fl:1;
  int quit_loop:1;
  int cascade_armed_fl:1;
  int cascade_inerit_destroy_fl:1;
  int cascade_propagate_destroy_fl:1;

  int evused;
  int evactive;
  HANDLE evhandle[EVP_WINEVENT_EVMAX];
  ul_evpwinevent_evdata_t *evdata[EVP_WINEVENT_EVMAX];

  ul_list_head_t trig_list;
  ul_list_head_t report_list;
  gavl_cust_root_field_t fd_root;

  ul_htim_time_t time_act;
  ul_htim_time_t time_next_expire;
  ul_htim_queue_t htim_queue;
 #ifdef UL_EVP_WINEVENT_DELAYDEL
  ul_list_head_t delaydel_list;
  unsigned int delaydel_generation;
 #endif
 #ifdef UL_EVP_WINEVENT_CASCADE
  ul_evptrig_t   cascade_trig;
 #endif /*UL_EVP_WINEVENT_CASCADE*/
} ul_evpbase_winevent_t;


UL_LIST_CUST_DEC(ul_evpwinevent_trig, ul_evpbase_winevent_t, ul_evptrig_winevent_t,
                trig_list, list_node)

UL_LIST_CUST_DEC(ul_evpwinevent_report, ul_evpbase_winevent_t, ul_evptrig_winevent_t,
                report_list, list_node)

UL_LIST_CUST_DEC(ul_evpwinevent_fdnode_trig, ul_evpwinevent_fdnode_t, ul_evptrig_winevent_t,
                samefd_list, samefd_node)

UL_LIST_CUST_DEC(ul_evpwinevent_evdatafd_fdnode, ul_evpwinevent_evdatafd_t, ul_evpwinevent_fdnode_t,
                fdnode_list, sameev_node)

#ifdef UL_EVP_WINEVENT_DELAYDEL
UL_LIST_CUST_DEC(ul_evpwinevent_delaydel, ul_evpbase_winevent_t, ul_evpwinevent_fdnode_t,
                delaydel_list, delaydel_node)
#endif

extern const ul_evpoll_ops_t ul_evpoll_ops_winevent;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVP_WINEVENT_H */
