/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpoll.c	- monitoring of open file handles

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#include "ul_evpoll.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_evpoll);

ul_evpbase_t *ul_evpoll_base_implicit;

const ul_evpoll_ops_t *ul_evpoll_ops_implicit;

int ul_evpoll_chose_implicit_ops(void)
{
  ul_evpoll_ops_implicit = ul_evpoll_ops_compile_default;
  return 0;
}

ul_evpbase_t *ul_evpoll_new(const ul_evpoll_ops_t *ops, int flags)
{
  if(!ops) {
    ops = ul_evpoll_ops_implicit;
    if(!ops) {
      ul_evpoll_chose_implicit_ops();
      if(!ul_evpoll_ops_implicit)
        return NULL;
      ops = ul_evpoll_ops_implicit;
    }
  }
  return ops->base_new();
}

ul_evpbase_t *ul_evpoll_chose_implicit_base(void)
{
  ul_evpbase_t *base;
  base = ul_evpoll_base_implicit;
  if(!base) {
    base = ul_evpoll_new(NULL, 0);
    ul_evpoll_base_implicit = base;
  }
  return base;
}

void ul_evpoll_destroy(ul_evpbase_t *base)
{
  if(!base) {
    base = ul_evpoll_base_implicit;
    if(!base)
      return;
  }
  if(base == ul_evpoll_base_implicit)
    ul_evpoll_base_implicit=NULL;
  base->ops->base_destroy(base);
}

int ul_evpoll_update(ul_evpbase_t *base)
{
  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }
  return base->ops->base_update(base);
}

int ul_evpoll_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }
  return base->ops->base_dispatch(base, timeout);
}

int ul_evpoll_set_option(ul_evpbase_t *base, int option, int val)
{
  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }
  return base->ops->base_set_option(base, option, val);
}

int ul_evptrig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }
  return base->ops->trig_init(base, evptrig);
}

int ul_evpoll_loop(ul_evpbase_t *base, int flags)
{
  int ret;

  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }

  do {

    ret = ul_evpoll_dispatch(base, NULL);

  } while(ret >= 0);

  if(ret == UL_EVP_DISPRET_QUIT)
    ret = 0;

  return ret;
}

int ul_evptrig_set_and_arm_fd(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb, ul_evfd_t fd,
                            int what, ul_htim_diff_t *timeout, int once)
{
  int ret;

  if(!evptrig->base) {
    ul_logerr("ul_evptrig_set_and_arm_fd called for uninitialized evptrig\n");
    return -1;
  }

  ul_evptrig_set_callback(evptrig, cb);

  ul_evptrig_set_fd(evptrig, fd, what);

  ul_evptrig_set_timeout(evptrig, timeout);

  if(!once)
    ret = ul_evptrig_arm(evptrig);
  else
    ret = ul_evptrig_arm_once(evptrig);

  if(ret<0) {
    ul_logerr("ul_evptrig_set_and_arm_fd for fd #%d failed %d\n", fd, ret);
    return -1;
  }

  return 0;
}

int ul_evpoll_quilt_loop(ul_evpbase_t *base)
{
   return ul_evpoll_set_option(base, UL_EVP_OPTION_QUIT, 1);
}

int ul_evpoll_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  if(base == NULL)
    return -1;

  if(base->ops->base_cascade == NULL)
    return -1;

  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }

  return base->ops->base_cascade(base, new_base, prioshift,  bc_flags);
}
