/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evplibevent1.c	- monitoring of open file handles

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#include <poll.h>

#include "ul_evpoll.h"
#include "ul_evp_libevent1.h"

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);


#ifdef UL_HTIMER_WITH_MSTIME

ul_htim_time_t ul_evplibevent1_get_current_time(ul_evpbase_t *base)
{
  ul_mstime_t mstime;
  ul_htim_time_t htime;

  ul_mstime_now(&mstime);
  ul_mstime2htime(&htime, &mstime);

  return htime;
}

#endif /*UL_HTIMER_WITH_MSTIME*/

static inline ul_htim_time_t ul_evplnxepoll_next_timeout(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_htim_time_t htime = ul_evpoll_get_current_time(base);
  ul_htime_add(&htime, &htime, timeout);
  return htime;
}

static inline ul_htim_time_t levtimeval2htim(struct timeval *timeval)
{
  ul_htim_time_t htime;
  long sec=timeval->tv_sec;
  ul_mstime_t mstime = sec*1000+timeval->tv_usec/1000;
  ul_mstime2htime(&htime, &mstime);
  return htime;
}

static inline int ul_evplibevent1_is_fd(ul_evptrig_libevent1_t *ei)
{
  return (ei->lev_event.ev_fd != -1);
}

static inline ul_evptrig_t *ul_evplibevent1_ei2evptrig(ul_evptrig_libevent1_t *ei)
{
  return (ul_evptrig_t *)ei->lev_event.ev_arg;
}

static inline void ul_evplibevent1_set_once(ul_evptrig_libevent1_t *ei, int val)
{
  if(val)
    ei->lev_event.ev_events &= ~EV_PERSIST;
  else
    ei->lev_event.ev_events |= EV_PERSIST;
}

static inline int ul_evplibevent1_arm_internal(ul_evptrig_libevent1_t *ei)
{
  int res;
  struct timeval timeout_tv;
  ul_htim_diff_t time_diff;
  int timed_fl = 0;

  if(ei->flags & UL_EVPLIBEVENT1_ARMED) {
    ul_logerr("ul_evplibevent1_arm_internal: tring to arm armed evptrig\n");
    return 0;
  }

  if(ei->flags & UL_EVPLIBEVENT1_TIMED) {
    timed_fl = 1;
    if(ei->flags & UL_EVPLIBEVENT1_TIMEOUT) {
      time_diff = ei->timing.timeout;
    } else {
      ul_htim_time_t time_act;
      time_act = ul_evpoll_get_current_time(ul_evplibevent1_ei2evptrig(ei)->base);
      ul_htime_sub(&time_diff, &ei->timing.expire, &time_act);
    }
    if(time_diff < 0)
      time_diff = 0;

    timeout_tv.tv_sec = time_diff / UL_HTIM_RESOLUTION;
    timeout_tv.tv_usec = (time_diff % UL_HTIM_RESOLUTION) * (1000000 / UL_HTIM_RESOLUTION);
  }

  res = event_add(&ei->lev_event, timed_fl? &timeout_tv: NULL);

  if (res < 0) {
    ul_logerr("ul_evplibevent1_arm_internal: event_add failed\n");
    return res;
  }

  ei->flags |= UL_EVPLIBEVENT1_ARMED;

  return 0;
}

static inline int ul_evplibevent1_disarm_internal(ul_evptrig_libevent1_t *ei)
{
  int res;

  if(!(ei->flags & UL_EVPLIBEVENT1_ARMED)) {
    ul_logerr("ul_evplibevent1_arm_internal: tring to disarm idle evptrig\n");
    return 0;
  }

  res = event_del(&ei->lev_event);

  ei->flags &= ~UL_EVPLIBEVENT1_ARMED;

  if (res < 0) {
    ul_logerr("ul_evplibevent1_disarm_internal: event_del failed\n");
    return res;
  }

  return 0;
}

static void ul_evplibevent1_cb_proxy(int fd, short event, void *arg)
{
  int what = 0;
  ul_evptrig_t *evptrig = (ul_evptrig_t *)arg;
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  if(event & EV_READ)
    what |= UL_EVP_IN;
  if(event & EV_WRITE)
    what |= UL_EVP_OUT;
  if(event & EV_TIMEOUT)
    what |= UL_EVP_TIMEOUT;
  if(event & EV_SIGNAL)
    what |= 0;

  if(!(ei->lev_event.ev_events & EV_PERSIST)) {
    ei->flags &= ~UL_EVPLIBEVENT1_ARMED;
  } else if(event & EV_TIMEOUT) {
    //ul_evplibevent1_disarm_internal(ei);
    ei->flags &= ~UL_EVPLIBEVENT1_ARMED;
    ei->flags &= ~UL_EVPLIBEVENT1_TIMED;
    ul_evplibevent1_arm_internal(ei);
  } else if(ei->flags & UL_EVPLIBEVENT1_TIMEOUT) {
    ul_evplibevent1_disarm_internal(ei);
    ei->flags |= UL_EVPLIBEVENT1_TIMED;
    ul_evplibevent1_arm_internal(ei);
  }

  evptrig->cb(evptrig, what);
}

static int ul_evplibevent1_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evptrig_libevent1_t *ei;
  ul_evpbase_libevent1_t *eb = UL_CONTAINEROF(base, ul_evpbase_libevent1_t, base);

  evptrig->cb = NULL;
  evptrig->base = base;

  evptrig->impl_data = (ul_evptrig_libevent1_t*)malloc(sizeof(ul_evptrig_libevent1_t));
  if(!evptrig->impl_data) {
    ul_logerr("ul_evplibevent1_trig_init: malloc failed\n");
    return -1;
  }
  ei = evptrig->impl_data;
  memset(ei, 0, sizeof(ul_evptrig_libevent1_t));

  event_set(&ei->lev_event, -1, 0, ul_evplibevent1_cb_proxy, evptrig);

  if(event_base_set(eb->lev_base, &ei->lev_event)<0) {
    ul_logerr("ul_evplibevent1_trig_init: event_base_set failed\n");
    free(ei);
    evptrig->base = NULL;
    evptrig->impl_data = NULL;
    return -1;
  }

  ei->flags = 0;

  ul_evplibevent1_trig_insert(eb, ei);

  return 0;
}

static void ul_evplibevent1_trig_done(ul_evptrig_t *evptrig)
{
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  if(!ei)
    return;

  if(ei->flags & UL_EVPLIBEVENT1_ARMED)
    ul_evptrig_disarm(evptrig);

  ei->lev_event.ev_arg = NULL;
  evptrig->impl_data = NULL;
  ul_evplibevent1_trig_del_item(ei);
}

static int ul_evplibevent1_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;
  int events = 0;
  int armed_fl;
  int res = 0;

  if(what & UL_EVP_IN)
    events |= EV_READ;
  if(what & UL_EVP_OUT)
    events |= EV_WRITE;

  /* FIXME: what to do with UL_EVP_ERR */

  armed_fl = ei->flags & UL_EVPLIBEVENT1_ARMED;
  if(armed_fl)
    ul_evplibevent1_disarm_internal(ei);
  ei->lev_event.ev_fd = fd;
  ei->lev_event.ev_events &= ~(EV_READ | EV_WRITE);
  ei->lev_event.ev_events |= events & (EV_READ | EV_WRITE);
  if(armed_fl)
    res = ul_evplibevent1_arm_internal(ei);
  return res;
}

static int ul_evplibevent1_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  int res = 0;
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  ei->flags &= ~(UL_EVPLIBEVENT1_TIMED | UL_EVPLIBEVENT1_TIMEOUT | UL_EVPLIBEVENT1_PERIODIC);

  if(!time)
    return 0;

  ei->timing.expire = *time;

  ei->flags |= UL_EVPLIBEVENT1_TIMED;

  if(ei->flags & UL_EVPLIBEVENT1_ARMED) {
    ul_evplibevent1_disarm_internal(ei);
    res = ul_evplibevent1_arm_internal(ei);
  }

  return res;
}

static int ul_evplibevent1_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  int res = 0;
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  ei->flags &= ~(UL_EVPLIBEVENT1_TIMED | UL_EVPLIBEVENT1_TIMEOUT | UL_EVPLIBEVENT1_PERIODIC);

  if(!timeout)
    return 0;

  ei->timing.timeout = *timeout;

  ei->flags |= UL_EVPLIBEVENT1_TIMED | UL_EVPLIBEVENT1_TIMEOUT;

  if(ei->flags & UL_EVPLIBEVENT1_ARMED) {
    ul_evplibevent1_disarm_internal(ei);
    res = ul_evplibevent1_arm_internal(ei);
  }

  return res;
}

static int ul_evplibevent1_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  evptrig->cb = cb;
  return 0;
}

static int ul_evplibevent1_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  if(ei->flags & UL_EVPLIBEVENT1_ARMED)
    return 0;

  ul_evplibevent1_set_once(ei, 0);

  if(ei->flags & UL_EVPLIBEVENT1_TIMEOUT)
    ei->flags |= UL_EVPLIBEVENT1_TIMED;

  return ul_evplibevent1_arm_internal(ei);
}

static int ul_evplibevent1_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  if(!(ei->flags & UL_EVPLIBEVENT1_ARMED))
    return 0;

  ul_evplibevent1_disarm_internal(ei);

  return 0;
}

static int ul_evplibevent1_trig_arm_once(ul_evptrig_t *evptrig)
{
  ul_evptrig_libevent1_t *ei = evptrig->impl_data;

  ul_evplibevent1_set_once(ei, 1);

  if(ei->flags & UL_EVPLIBEVENT1_ARMED)
    return 0;

  if(ei->flags & UL_EVPLIBEVENT1_TIMEOUT)
    ei->flags |= UL_EVPLIBEVENT1_TIMED;

  return ul_evplibevent1_arm_internal(ei);
}

static int ul_evplibevent1_trig_set_param(ul_evptrig_t *evptrig,
                           int parnum, const void *parval, int parsize)
{
  return -1;
}

static int ul_evplibevent1_trig_get_param(ul_evptrig_t *evptrig,
                           int parnum, void *parval, int parmaxsize)
{
  return -1;
}

/*FIXME: hack to check, that library is initialized*/
extern struct event_base *current_base;

static ul_evpbase_t *ul_evplibevent1_base_new(void)
{
  ul_evpbase_libevent1_t *eb;

  eb = (ul_evpbase_libevent1_t*)malloc(sizeof(ul_evpbase_libevent1_t));
  if(!eb)
    return NULL;

  memset(eb,0,sizeof(ul_evpbase_libevent1_t));

  eb->base.ops = &ul_evpoll_ops_libevent1;

  ul_evplibevent1_trig_init_head(eb);

  /* FIXME: this is nasty hack */
  if(current_base==NULL) {
    if(event_init()==NULL) {
      ul_logerr("ul_evplibevent1_base_new: libevent base initialization failed\n");
      free(eb);
      return NULL;
    }
  }

  //eb->lev_base = event_init();
  //eb->lev_base = event_base_new();

  eb->lev_base = current_base;
  if(!eb->lev_base)
    goto init_error;

  return &eb->base;

init_error:
  free(eb);
  return NULL;
}

static void ul_evplibevent1_base_destroy(ul_evpbase_t *base)
{
  ul_evptrig_libevent1_t *ei;
  ul_evpbase_libevent1_t *eb = UL_CONTAINEROF(base, ul_evpbase_libevent1_t, base);

  while((ei = ul_evplibevent1_trig_first(eb))!=NULL){
    ul_evptrig_t *evptrig = ul_evplibevent1_ei2evptrig(ei);
    ul_evplibevent1_trig_disarm(evptrig);
    if(evptrig->cb)
      evptrig->cb(evptrig, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE for %i when poll is being destroyed\n", ei->lev_event.ev_fd);
    if(ei == ul_evplibevent1_trig_first(eb)) {
      ul_logdeb("UL_EVP_DONE not handled for %i when poll is being destroyed\n", ei->lev_event.ev_fd);
      ul_evplibevent1_trig_done(evptrig);
    }
  }

  if(eb->lev_base != NULL) {
    event_base_free(eb->lev_base);
  }
  eb->lev_base = NULL;

  free(eb);
}

static int ul_evplibevent1_base_update(ul_evpbase_t *base)
{
  return 0;
}

static int ul_evplibevent1_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  int res;
  ul_evpbase_libevent1_t *eb = UL_CONTAINEROF(base, ul_evpbase_libevent1_t, base);

  if(timeout == NULL) {
    res = event_base_dispatch(eb->lev_base);
  } else {
    res = event_base_loop(eb->lev_base, *timeout? EVLOOP_ONCE: EVLOOP_NONBLOCK);
  }

  if(res < 0)
    return UL_EVP_DISPRET_ERROR;

  return UL_EVP_DISPRET_QUIT;
}

static int ul_evplibevent1_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_libevent1_t *eb = UL_CONTAINEROF(base, ul_evpbase_libevent1_t, base);

  switch(option) {
    case UL_EVP_OPTION_QUIT: {
        struct timeval timeout_tv;
        timeout_tv.tv_sec = 0;
        timeout_tv.tv_usec = 0;
        event_base_loopexit(eb->lev_base, &timeout_tv);
        return 0;
      }
   }
  return -1;
}

const ul_evpoll_ops_t ul_evpoll_ops_libevent1 = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "libevent1",
  .trig_init = ul_evplibevent1_trig_init,
  .trig_done = ul_evplibevent1_trig_done,
  .trig_set_fd = ul_evplibevent1_trig_set_fd,
  .trig_set_time = ul_evplibevent1_trig_set_time,
  .trig_set_timeout = ul_evplibevent1_trig_set_timeout,
  .trig_set_callback = ul_evplibevent1_trig_set_callback,
  .trig_arm = ul_evplibevent1_trig_arm,
  .trig_disarm = ul_evplibevent1_trig_disarm,
  .trig_arm_once = ul_evplibevent1_trig_arm_once,
  .trig_set_param = ul_evplibevent1_trig_set_param,
  .trig_get_param = ul_evplibevent1_trig_get_param,
  .base_new = ul_evplibevent1_base_new,
  .base_destroy = ul_evplibevent1_base_destroy,
  .base_update = ul_evplibevent1_base_update,
  .base_dispatch = ul_evplibevent1_base_dispatch,
  .base_get_current_time = ul_evplibevent1_get_current_time,
  .base_set_option = ul_evplibevent1_base_set_option,
};
