/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpollint.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVPOLLINT_H
#define _UL_EVPOLLINT_H

#include "ul_utdefs.h"

#include <string.h>

#include "ul_evpoll.h"
#include <ul_list.h>
#include <ul_htimer.h>

/*
 * There is name collision between uLUt and recent libevent
 * list implementations
 */
#ifdef LIST_HEAD
#undef LIST_HEAD
#undef LIST_HEAD_INIT
#endif

#include <sys/types.h>
#include <event2/event.h>
#if 1
#include <event2/event_struct.h>
#else
struct event {
  long long dummy;
};
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define UL_EVPLIBEVENT2_ARMED    0x01
#define UL_EVPLIBEVENT2_ONCE     0x02
#define UL_EVPLIBEVENT2_TIMED    0x04
#define UL_EVPLIBEVENT2_TIMEOUT  0x08
#define UL_EVPLIBEVENT2_PERIODIC 0x10

typedef struct ul_evptrig_data_t {
  unsigned flags;

  union {
    ul_htim_time_t expire;
    ul_htim_diff_t timeout;
  } timing;

  ul_list_node_t list_node;

  struct event lev_event[];
} ul_evptrig_libevent2_t;

typedef struct ul_evpbase_libevent2_t {
  ul_evpbase_t base;
  struct event_base *lev_base;
  ul_list_head_t trig_list;
} ul_evpbase_libevent2_t;

UL_LIST_CUST_DEC(ul_evplibevent2_trig, ul_evpbase_libevent2_t, ul_evptrig_libevent2_t,
                trig_list, list_node)

extern const ul_evpoll_ops_t ul_evpoll_ops_libevent2;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVPOLLINT_H */
