/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpollint.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVPOLLINT_H
#define _UL_EVPOLLINT_H

#include "ul_utdefs.h"

#include <string.h>

#include "ul_evpoll.h"
#include <ul_list.h>
#include <ul_htimer.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Provide cascade support
 */
#define UL_EVP_SYSVPOLL_CASCADE

#define UL_SYSVPOLL_ARMED    0x01
#define UL_SYSVPOLL_ONCE     0x02
#define UL_SYSVPOLL_TIMED    0x04
#define UL_SYSVPOLL_TIMEOUT  0x08
#define UL_SYSVPOLL_PERIODIC 0x10

typedef struct ul_evptrig_data_t {
  unsigned flags;
  unsigned what_events;

  ul_evfd_t      fd;
  int            idx;

  ul_htim_diff_t timeout;
  ul_htim_node_t htim;

  ul_list_node_t list_node;

  ul_evptrig_t *trig_ptr;

} ul_evptrig_sysvpoll_t;

typedef struct ul_evpbase_sysvpoll_t {
  ul_evpbase_t base;

  ul_evptrig_sysvpoll_t **trig_array;
  struct pollfd *pollfd_array;
  int pollfd_count;
  int pollfd_capacity;

  int need_resync:1;
  int time_next_expire_fl:1;
  int quit_loop:1;
  int cascade_inerit_destroy_fl:1;
  int cascade_propagate_destroy_fl:1;

  int            activefd_count;
  ul_list_head_t active_list;
  ul_list_head_t idle_list;

  ul_htim_time_t time_act;
  ul_htim_time_t time_next_expire;
  ul_htim_queue_t htim_queue;

 #ifdef UL_EVP_SYSVPOLL_CASCADE
  ul_evpbase_t   *cascade_base;
 #endif /*UL_EVP_SYSVPOLL_CASCADE*/
} ul_evpbase_sysvpoll_t;


UL_LIST_CUST_DEC(ul_evpsysvpoll_active, ul_evpbase_sysvpoll_t, ul_evptrig_sysvpoll_t,
                active_list, list_node)

UL_LIST_CUST_DEC(ul_evpsysvpoll_idle, ul_evpbase_sysvpoll_t, ul_evptrig_sysvpoll_t,
                idle_list, list_node)

extern const ul_evpoll_ops_t ul_evpoll_ops_sysvpoll;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVPOLLINT_H */
