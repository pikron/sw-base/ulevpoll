#define WITH_TCP_PIPES_SUPPORT

#include <string.h>
#include "ul_utmalloc.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>

#if defined(_WIN32) && !defined(pipe)
#include <io.h>
#define pipe(x) _pipe(x, 256, O_BINARY)
#ifdef WITH_TCP_PIPES_SUPPORT
#include <winsock.h>
#include <fcntl.h>
#ifndef socklen_t
#define socklen_t int
#endif
#endif /*WITH_TCP_PIPES_SUPPORT*/

#else /*not _WIN32*/
#ifdef WITH_TCP_PIPES_SUPPORT
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define closesocket(s)  close(s)
#endif /*WITH_TCP_PIPES_SUPPORT*/
#endif /*_WIN32*/

#include "ul_evpoll.h"
#include "ul_htimer.h"

#include "ul_evpoll_config.h"

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

#ifdef CONFIG_OC_UL_EVP_SELECT
extern const ul_evpoll_ops_t ul_evpoll_ops_select;
#endif
#ifdef CONFIG_OC_UL_EVP_LNXEPOLL
extern const ul_evpoll_ops_t ul_evpoll_ops_lnxepoll;
#endif
#ifdef CONFIG_OC_UL_EVP_GLIB
extern const ul_evpoll_ops_t ul_evpoll_ops_glib;
#endif
#ifdef CONFIG_OC_UL_EVP_LIBEVENT1
extern const ul_evpoll_ops_t ul_evpoll_ops_libevent1;
#endif
#ifdef CONFIG_OC_UL_EVP_LIBEVENT2
extern const ul_evpoll_ops_t ul_evpoll_ops_libevent2;
#endif
#ifdef CONFIG_OC_UL_EVP_WINEVENT
extern const ul_evpoll_ops_t ul_evpoll_ops_winevent;
#endif

int batch_run_fl = 0;
int use_socket_pair_fl = 0;
char *batch_tests_list = NULL;

/******************************************************************************/

typedef struct my_timer_t {
  ul_evptrig_t evptrig;
  ul_htim_time_t time;
  ul_htim_diff_t interval;
  int timno;
  int count;
  int count_limit;
} my_timer_t;

void my_timer_cb(ul_evptrig_t *evptrig, int what)
{
  my_timer_t *mt = UL_CONTAINEROF(evptrig, my_timer_t, evptrig);

  mt->count++;

  ul_logdeb("@ timer #%d evptrig %d count %d\n", mt->timno, what, mt->count);

  if(what == UL_EVP_DONE) {
    ul_evptrig_done(&mt->evptrig);
    free(mt);
    return;
  }

  if(!(what & UL_EVP_TIMEOUT))
    return;

  if(mt->count_limit && (mt->count>=mt->count_limit)) {
    ul_evptrig_done(&mt->evptrig);
    ul_logerr("@ timer #%d reached limit %d\n", mt->timno, mt->count);
    free(mt);
    return;
  }

  mt->time += mt->interval;
  if(ul_evptrig_set_time(&mt->evptrig, &mt->time) < 0) {
    ul_logerr("ul_evptrig_set_time for timer #%d evptrig\n", mt->timno);
  }
}

int add_my_timer(int interval, int count_limit)
{
  static int timno = 0;
  my_timer_t *mt;

  timno++;

  mt = malloc(sizeof(my_timer_t));
  if(!mt) {
    ul_logerr("cannot allocate space for timer #%d\n", timno);
    return -1;
  }

  mt->timno = timno;
  mt->interval = interval;
  mt->count = 0;
  mt->count_limit = count_limit;
  mt->time = ul_evpoll_get_current_time(ul_evpoll_chose_implicit_base());

  if(ul_evptrig_init(NULL, &mt->evptrig)<0) {
    free(mt);
    ul_logerr("cannot init timer #%d evptrig\n", mt->timno);
    return -1;
  }

  ul_evptrig_set_callback(&mt->evptrig, my_timer_cb);

  mt->time += mt->interval;
  if(ul_evptrig_set_time(&mt->evptrig, &mt->time) < 0) {
    ul_logerr("ul_evptrig_set_time for timer #%d evptrig\n", mt->timno);
  }

  if(ul_evptrig_arm(&mt->evptrig)<0) {
    ul_evptrig_done(&mt->evptrig);
    ul_logerr("ul_evptrig_arm for timer #%d evptrig\n", mt->timno);
    free(mt);
  }


  return 0;
}

/******************************************************************************/

#ifdef WITH_TCP_PIPES_SUPPORT

int last_soc_err(void)
{
 #ifdef _WIN32
  return(WSAGetLastError());
 #else
  return errno;
 #endif
}

int create_local_tcp_socketpair(int pipefd[2])
{
  int sock;
  struct sockaddr_in name;
  socklen_t name_len;

  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    ul_logerr("socket create failed: %s (%d)\n", strerror(errno), last_soc_err());
    return -1;
  }

  name.sin_family = AF_INET;
  name.sin_port = htons(0); /*PORT_ANY*/
  name.sin_addr.s_addr = htonl (INADDR_ANY);
 #ifdef _WIN32
  name.sin_addr.s_addr = htonl(0x7f000001);
 #endif
  if (bind (sock, (struct sockaddr *) &name, sizeof (name)) < 0) {
    ul_logerr("socket bind failed: %s (%d)\n", strerror(errno), last_soc_err());
    goto error_close_sock;
  }

  name_len = sizeof(name);
  if (getsockname (sock, (struct sockaddr *) &name, &name_len) < 0) {
    ul_logerr("socket getsockname failed: %s (%d)\n", strerror(errno), last_soc_err());
    return -1;
  }

  if(listen(sock, 1) < 0) {
    ul_logerr("socket listen failed: %s (%d)\n", strerror(errno), last_soc_err());
    goto error_close_sock;
  }

  pipefd[0] = socket(PF_INET, SOCK_STREAM, 0);
  if(pipefd[0] < 0) {
    ul_logerr("in socket create failed: %s (%d)\n", strerror(errno), last_soc_err());
    goto error_close_sock;
  }
  /*inet_aton("127.0.0.1", &name.sin_addr);*/
 #ifdef _WIN32
  name.sin_addr.s_addr = htonl(0x7f000001);
 #endif
  if(connect(pipefd[0], (struct sockaddr *) &name, name_len) < 0) {
    ul_logerr("in socket connect to port %d address 0x%lx failed: %s (%d)\n",
              ntohs(name.sin_port), (unsigned long)ntohl(name.sin_addr.s_addr),
              strerror(errno), last_soc_err());
    goto error_close_in;
  }
  ul_logdeb("in socket connect to port %d address 0x%lx suceed\n",
            ntohs(name.sin_port), (unsigned long)ntohl(name.sin_addr.s_addr));

  pipefd[1] = accept(sock, (struct sockaddr *) &name, &name_len);
  if(pipefd[1] < 0) {
    ul_logerr("out socket accept failed: %s (%d)\n", strerror(errno), last_soc_err());
    goto error_close_in;
  }

#if 0
 #ifdef _WIN32
  ul=1;
  ioctlsocket(pipefd[0],FIONBIO,&ul);
  ioctlsocket(pipefd[1],FIONBIO,&ul);
 #else /*_WIN32*/
  ul=fcntl(pipefd[0],F_GETFL,0);
  fcntl(pipefd[0],F_SETFL, ul|O_NONBLOCK);
  ul=fcntl(pipefd[1],F_GETFL,0);
  fcntl(pipefd[1],F_SETFL, ul|O_NONBLOCK);
 #endif /*_WIN32*/
#endif

  closesocket(sock);

  return 0;

error_close_in:
  closesocket(pipefd[0]);

error_close_sock:
  closesocket(sock);

  return -1;
}

#endif /*WITH_TCP_PIPES_SUPPORT*/

/******************************************************************************/

typedef struct my_pipe_t {
  ul_evptrig_t rd_evptrig;
  ul_evptrig_t wr_evptrig;
  ul_evptrig_t timer_evptrig;
  int rd_fd;
  int wr_fd;
  int rd_count;
  int wr_count;
  int chunk_count;
  ul_htim_diff_t chunk_delay;
  int stop_count;
  int use_socket_pair;
} my_pipe_t;

int my_pipes_active_cnt;
ul_htim_diff_t my_pipes_start_time;
ul_htim_diff_t my_pipes_stop_time;

void my_pipe_destroy(my_pipe_t *mp, int show_time)
{
  ul_loginf("@ pipe rd #%d wr #%d destroyed\n", mp->rd_fd, mp->wr_fd);
  ul_evptrig_done(&mp->timer_evptrig);
  ul_evptrig_done(&mp->wr_evptrig);
  ul_evptrig_done(&mp->rd_evptrig);

 #ifdef WITH_TCP_PIPES_SUPPORT
  if(mp->use_socket_pair) {
    if(mp->rd_fd>=0)
      closesocket(mp->rd_fd);
    if(mp->wr_fd>=0)
      closesocket(mp->wr_fd);
  } else
 #endif /*WITH_TCP_PIPES_SUPPORT*/
  {
    if(mp->rd_fd>=0)
      close(mp->rd_fd);
    if(mp->wr_fd>=0)
      close(mp->wr_fd);
  }

  if(!--my_pipes_active_cnt) {
    if(show_time) {
      my_pipes_stop_time = ul_evpoll_get_current_time(ul_evpoll_chose_implicit_base());
      ul_logmsg("@ pipe active count reached 0 in %ld\n", (long)my_pipes_stop_time-my_pipes_start_time);
    } else {
      ul_logmsg("@ pipe active count reached 0 but no time to show\n");
    }
  }
  free(mp);
}

void my_pipe_rd_cb(ul_evptrig_t *evptrig, int what)
{
  int ret;
  my_pipe_t *mp = UL_CONTAINEROF(evptrig, my_pipe_t, rd_evptrig);

  if(what & UL_EVP_DONE) {
    ul_logdeb("@ pipe #%d evptrig rd done signalled\n", mp->rd_fd);
    my_pipe_destroy(mp, 0);
    return;
  }

  if(what & UL_EVP_TIMEOUT) {
    ul_logdeb("@ pipe #%d evptrig rd timeout\n", mp->rd_fd);
    return;
  }

  if(what & UL_EVP_READ) {
    char ch;

   #ifdef WITH_TCP_PIPES_SUPPORT
    if(mp->use_socket_pair) {
      ret = recv(mp->rd_fd, &ch, 1, 0);
    } else
   #endif /*WITH_TCP_PIPES_SUPPORT*/
    {
      ret = read(mp->rd_fd, &ch, 1);
    }
    if(ret != 1)
      ul_logerr("@ pipe #%d read returned %d\n", mp->rd_fd, ret);
    else {
      ul_logdeb("@ pipe #%d rd evptrig %d count wr %d rd %d\n", mp->rd_fd, what, mp->wr_count, mp->rd_count);
      mp->rd_count++;
      if(mp->stop_count && (mp->rd_count >= mp->stop_count)) {
        ul_logdeb("@ pipe #%d rd final count reached\n", mp->rd_fd);
        my_pipe_destroy(mp, 1);
      }
    }
  }
}

void my_pipe_wr_cb(ul_evptrig_t *evptrig, int what)
{
  int ret;
  my_pipe_t *mp = UL_CONTAINEROF(evptrig, my_pipe_t, wr_evptrig);

  if(what & UL_EVP_DONE) {
    ul_logdeb("@ pipe #%d evptrig wr done signalled\n", mp->wr_fd);
    my_pipe_destroy(mp, 0);
    return;
  }

  if(what & UL_EVP_TIMEOUT) {
    ul_logdeb("@ pipe #%d evptrig wr timeout\n", mp->wr_fd);
    return;
  }

  if(what & UL_EVP_WRITE) {
    char ch = 'a' + (mp->wr_count & 0xf);

    if(mp->stop_count && (mp->wr_count >= mp->stop_count)) {
      ul_logdeb("@ pipe #%d wr final count reached\n", mp->wr_fd);
      ul_evptrig_disarm(&mp->wr_evptrig);
      return;
    }

   #ifdef WITH_TCP_PIPES_SUPPORT
    if(mp->use_socket_pair) {
      ret = send(mp->wr_fd, &ch, 1, 0);
    } else
   #endif /*WITH_TCP_PIPES_SUPPORT*/
    {
      ret = write(mp->wr_fd, &ch, 1);
    }
    if(ret != 1)
      ul_logerr("@ pipe #%d write returned %d\n", mp->wr_fd, ret);
    else {
      mp->wr_count++;
      ul_logdeb("@ pipe #%d wr evptrig %d count wr %d rd %d\n", mp->wr_fd, what, mp->wr_count, mp->rd_count);
      mp->chunk_count++;
      if(mp->chunk_count>=50) {
        mp->chunk_count=0;
        ul_evptrig_disarm(&mp->wr_evptrig);

        ul_evptrig_set_timeout(&mp->timer_evptrig, &mp->chunk_delay);
        ul_evptrig_arm_once(&mp->timer_evptrig);
      }
    }
  }
}

void my_pipe_timer_cb(ul_evptrig_t *evptrig, int what)
{
  my_pipe_t *mp = UL_CONTAINEROF(evptrig, my_pipe_t, timer_evptrig);

  if(what & UL_EVP_DONE) {
    ul_logdeb("@ pipe #%d evptrig timer done signalled\n", mp->wr_fd);
    my_pipe_destroy(mp, 0);
    return;
  }

  if(what & UL_EVP_TIMEOUT) {
    ul_logdeb("@ pipe #%d evptrig timer\n", mp->wr_fd);
    ul_evptrig_disarm(&mp->timer_evptrig);
    ul_evptrig_arm(&mp->wr_evptrig);
  }
}

int add_my_pipe(int stop_count, int chunk_count, ul_htim_diff_t chunk_delay)
{
  my_pipe_t *mp;
  ul_htim_diff_t timo = 4000;

  mp = malloc(sizeof(my_pipe_t));
  if(!mp) {
    ul_logerr("cannot allocate space for pipe\n");
    return -1;
  }
  memset(mp, 0, sizeof(*mp));

  mp->stop_count=stop_count;
  mp->chunk_count=chunk_count;
  mp->chunk_delay=chunk_delay;
  mp->rd_fd=-1;
  mp->wr_fd=-1;
  mp->use_socket_pair=use_socket_pair_fl;

  do {
    if(ul_evptrig_init(NULL, &mp->rd_evptrig)>=0) {
      if(ul_evptrig_init(NULL, &mp->wr_evptrig)>=0) {
        if(ul_evptrig_init(NULL, &mp->timer_evptrig)>=0)
	  break;
        ul_evptrig_done(&mp->wr_evptrig);
      }
      ul_evptrig_done(&mp->rd_evptrig);
    }
    free(mp);
    ul_logerr("pipe test cannot init evptrigs\n");
    return -1;
  } while(0);

  ul_evptrig_set_callback(&mp->rd_evptrig, my_pipe_rd_cb);
  ul_evptrig_set_callback(&mp->wr_evptrig, my_pipe_wr_cb);
  ul_evptrig_set_callback(&mp->timer_evptrig, my_pipe_timer_cb);

  {
    int filedes[2];
    int res;

   #ifdef WITH_TCP_PIPES_SUPPORT
    if(mp->use_socket_pair) {
      /*res = socketpair(AF_LOCAL, SOCK_STREAM, 0, filedes);*/
      res = create_local_tcp_socketpair(filedes);
    } else
   #endif /*WITH_TCP_PIPES_SUPPORT*/
    {
      res = pipe(filedes);
    }

    if(res<0) {
      ul_logerr("pipe creation failed: %s\n", strerror(errno));
      my_pipe_destroy(mp, 0);
      return -1;
    }
    mp->rd_fd = filedes[0];
    mp->wr_fd = filedes[1];
  }

  ul_evptrig_set_fd(&mp->rd_evptrig, mp->rd_fd, UL_EVP_READ);
  ul_evptrig_set_fd(&mp->wr_evptrig, mp->wr_fd, UL_EVP_WRITE);

  ul_evptrig_set_timeout(&mp->rd_evptrig, &timo);
  ul_evptrig_set_timeout(&mp->wr_evptrig, &timo);

  if(ul_evptrig_arm(&mp->rd_evptrig)<0) {
    ul_logerr("ul_evptrig_arm rd for pipe rd #%d wr #%d evptrig\n", mp->rd_fd, mp->wr_fd);
    my_pipe_destroy(mp, 0);
    return -1;
  }

  if(ul_evptrig_arm(&mp->wr_evptrig)<0) {
    ul_logerr("ul_evptrig_arm wr for pipe rd #%d wr #%d evptrig\n", mp->rd_fd, mp->wr_fd);
    my_pipe_destroy(mp, 0);
    return -1;
  }

  if(!my_pipes_active_cnt++) {
    my_pipes_start_time = ul_evpoll_get_current_time(ul_evpoll_chose_implicit_base());
  }

  return 0;
}

/******************************************************************************/

void test_htimer_fnc(ul_htimer_fnc_data_t data)
{
  char s[30];
  ul_htim_time_t actual_time;
  ul_get_log_time_str(s);
  ul_root_htimer_current_time(0, &actual_time);
  ul_loginf("%6ld : ms %8ld real %s\n",(long)data,actual_time,s);
}

/******************************************************************************/

#ifdef _WIN32

typedef struct my_win32_event_t {
  ul_evptrig_t evptrig;
  HANDLE handle;
  int count;
} my_win32_event_t;

my_win32_event_t *my_win32_event_last_added;

void my_win32_event_cb(ul_evptrig_t *evptrig, int what)
{
  my_win32_event_t *mwe = UL_CONTAINEROF(evptrig, my_win32_event_t, evptrig);

  if(what & UL_EVP_DONE) {
    ul_loginf("@ win32_event #0x%lx done signalled\n", (unsigned long)mwe->handle);
    ul_evptrig_done(evptrig);
    CloseHandle(mwe->handle);
    free(mwe);
    return;
  }

  if(what & UL_EVP_SIGNAL) {
    ul_loginf("@ win32_event #0x%lx event signalled\n", (unsigned long)mwe->handle);
    ResetEvent(mwe->handle);
  }
}

int test_win32_event(ul_evpbase_t *base)
{
  my_win32_event_t *mwe;

  mwe = (my_win32_event_t *)malloc(sizeof(*mwe));
  if(mwe == NULL)
    return -1;
  memset(mwe, 0, sizeof(*mwe));
  mwe->handle = CreateEvent(NULL, TRUE, FALSE, NULL);
  if(mwe->handle == INVALID_HANDLE_VALUE)
    return -1;
  if(ul_evptrig_init(base, &mwe->evptrig)<0) {
    ul_logerr("test_win32_event: #0x%lx ul_evptrig_init failed\n",
              (unsigned long)mwe->handle);
    return -1;
  }
  ul_evptrig_set_callback(&mwe->evptrig, my_win32_event_cb);

  if(ul_evptrig_set_param(&mwe->evptrig, UL_EVPTRIG_PARAM_WIN32EVENT,
                          &mwe->handle, sizeof(mwe->handle)) < 0) {
    ul_logerr("test_win32_event: #0x%lx set UL_EVPTRIG_PARAM_WIN32EVENT failed\n",
              (unsigned long)mwe->handle);
    return -1;
  }
  if(ul_evptrig_arm(&mwe->evptrig) < 0) {
    ul_logerr("test_win32_event: #0x%lx ul_evptrig_arm failed\n",
              (unsigned long)mwe->handle);
    return -1;
  }

  my_win32_event_last_added = mwe;

  return 0;
}

#endif /* _WIN32 */

/******************************************************************************/

typedef struct my_fd_t {
  ul_evptrig_t evptrig;
  int fd;
  int count;
} my_fd_t;

int add_my_fd(int fd, int timeout);

int my_fd_process_char(ul_evpbase_t *base, int ch)
{
  int ret = 0;
  int i;

  switch(ch) {
    case 'q':
      ul_evpoll_set_option(base,UL_EVP_OPTION_QUIT,1);
      break;
    case 'p':
      add_my_pipe(500, 50, 2000);
      break;
    case 't':
      ulogd_evpoll.level = UL_LOGL_MSG;
      for(i=0; i<500; i++)
        add_my_pipe(500, 50, 10);
      break;
    case 'T':
      ulogd_evpoll.level = UL_LOGL_MSG;
      for(i=0; i<5000; i++)
        add_my_pipe(500, 50, 10);
      break;
    case 'l':
    case 'L':
      for(i=(ch=='L')?4500:500; i; i--) {
        int filedes[2];
        if(pipe(filedes)<0) {
          ul_logerr("pipe creation failed\n");
          break;
        }
        add_my_fd(filedes[0], 0);
        add_my_fd(filedes[1], 0);
      }
      break;
   #ifdef CONFIG_OC_UL_EVP_GLIB
    case 'c':
    case 'C': {
        ul_evpbase_t *glib_base;
        ul_evpbase_t *actual_base;
        glib_base = ul_evpoll_ops_glib.base_new();
        if(glib_base == NULL) {
          ul_logerr("glib base create error\n");
          break;
        }
        actual_base = base;
        if(ul_evpoll_cascade(actual_base, glib_base, 0, 0) < 0) {
          ul_logerr("cascade actual base error\n");
          break;
        }
        if(ch == 'C')
          ul_evpoll_base_implicit = glib_base;
        break;
      }
   #endif /* CONFIG_OC_UL_EVP_GLIB */
   #ifdef CONFIG_OC_UL_EVP_LNXEPOLL
    case 'e':
    case 'E':
    case 'f':
    case 'F':  {
        ul_evpbase_t *new_base;
        ul_evpbase_t *actual_base;
        int bc_flags = UL_EVP_CASFL_INHERIT_DESTROY | UL_EVP_CASFL_PROPAGATE_DESTROY;
        new_base = ul_evpoll_ops_lnxepoll.base_new();
        if(new_base == NULL) {
          ul_logerr("lnxepoll base create error\n");
          break;
        }
        actual_base = base;
        if((ch == 'f') || (ch == 'F'))
          ret = ul_evpoll_cascade(new_base, actual_base, 0, bc_flags);
        else
          ret = ul_evpoll_cascade(actual_base, new_base, 0, bc_flags);
        if(ret < 0) {
          ul_logerr("cascade actual base error\n");
          ul_evpoll_destroy(new_base);
          break;
        }
        if((ch == 'E') || (ch == 'F'))
          ul_evpoll_base_implicit = new_base;
        break;
      }
   #endif /* CONFIG_OC_UL_EVP_LNXEPOLL */
    case 'H': {
        int i;
        ul_htim_time_t htime;
        ul_htim_diff_t hdiff = 1000;
        if(ul_evpoll_setup_root_htimer(NULL, 0) < 0) {
          ul_logerr("setup root htimer error\n");
          break;
        }
        if(ul_root_htimer_current_time(0, &htime) < 0) {
          ul_logerr("ul_root_htimer_current_time failed\n");
          break;
        }
        for(i = 0; i< 100; i++) {
          ul_htimer_t *timer = malloc(sizeof(*timer));
          ul_htimer_init_detached(timer);
          timer->function=test_htimer_fnc;
          timer->data=(ul_htimer_fnc_data_t)i;
          ul_htimer_set_expire(timer, htime);
          ul_htime_add(&htime, &htime, &hdiff);
          if(ul_root_htimer_add(timer)<0) {
            ul_logerr("ul_root_htimer_add failed\n");
            free(timer);
          }
        }
        break;
      }
   #ifdef _WIN32
    case 'W':
      test_win32_event(base);
      break;
    case 'w':
      if(my_win32_event_last_added)
        SetEvent(my_win32_event_last_added->handle);
      break;
   #endif /* _WIN32 */
  }

  return ret;
}

void my_fd_cb(ul_evptrig_t *evptrig, int what)
{
  int ret;
  my_fd_t *mf = UL_CONTAINEROF(evptrig, my_fd_t, evptrig);

  if(what & UL_EVP_DONE) {
    ul_logdeb("@ fd #%d evptrig done signalled\n", mf->fd);
    ul_evptrig_done(&mf->evptrig);
    free(mf);
    return;
  }

  if(what & UL_EVP_TIMEOUT) {
    ul_logdeb("@ fd #%d evptrig timeout\n", mf->fd);
    return;
  }

  mf->count++;

  ul_logdeb("@ fd #%d evptrig %d count %d\n", mf->fd, what, mf->count);

  if(what & UL_EVP_READ) {
    char ch;
    ret = read(mf->fd, &ch, 1);
    if(ret != 1)
      ul_logerr("@ fd #%d read returned %d\n", mf->fd, ret);
    printf("'%c'\n", ch);

    my_fd_process_char(ul_evptrig_get_base(evptrig), ch);
  }
}

int add_my_fd(int fd, int timeout)
{
  my_fd_t *mf;
  ul_htim_diff_t timo = timeout;

  mf = malloc(sizeof(my_fd_t));
  if(!mf) {
    ul_logerr("cannot allocate space for fd #%d\n", fd);
    return -1;
  }

  mf->fd = fd;
  mf->count = 0;

  if(ul_evptrig_init(NULL, &mf->evptrig)<0) {
    free(mf);
    ul_logerr("cannot init fd #%d evptrig\n", mf->fd);
    return -1;
  }

  ul_evptrig_set_callback(&mf->evptrig, my_fd_cb);

  ul_evptrig_set_fd(&mf->evptrig, mf->fd, UL_EVP_READ);

  if(ul_evptrig_set_timeout(&mf->evptrig, timeout? &timo: NULL) < 0) {
    ul_logerr("ul_evptrig_set_timeout for fd #%d evptrig\n", mf->fd);
  }

  if(ul_evptrig_arm(&mf->evptrig)<0) {
    ul_evptrig_done(&mf->evptrig);
    ul_logerr("ul_evptrig_arm for fd #%d evptrig\n", mf->fd);
    free(mf);
  }

  return 0;
}

/******************************************************************************/

typedef struct arg_table_entry_t {
  const char *arg_token;
  int (*arg_fnc)(int argc, char *argv[], void *variant_ptr);
  void *variant_ptr;
} arg_table_entry_t;

const arg_table_entry_t arg_table[];

int arg_fnc_help(int argc, char *argv[], void *variant_ptr)
{
  const arg_table_entry_t *ae = arg_table;
  printf("Usage: ul_evpchk <option> ...\n");
  printf("available options are:\n");
  for (; ae->arg_token != NULL; ae++) {
    printf("  %s\n", ae->arg_token);
  }
  return 0;
}

int arg_fnc_clear_fl(int argc, char *argv[], void *variant_ptr)
{
  *(int *)variant_ptr = 0;
  return 1;
}

int arg_fnc_set_fl(int argc, char *argv[], void *variant_ptr)
{
  *(int *)variant_ptr = 1;
  return 1;
}

int arg_fnc_set_int(int argc, char *argv[], void *variant_ptr)
{
  char *p, *r;
  long l;

  if (argc <2)
    return -1;

  p = argv[1];

  l = strtol(p, &r, 0);

  if((p == r) || *r)
    return -1;

  *(int *)variant_ptr = l;

  return 2;
}

int arg_fnc_set_str(int argc, char *argv[], void *variant_ptr)
{
  char *p;

  if (argc <2)
    return -1;

  p = argv[1];

  *(char **)variant_ptr = strdup(p);

  return 2;
}

int arg_fnc_set_evpoll_ops(int argc, char *argv[], void *variant_ptr)
{
  ul_evpoll_ops_implicit = (const ul_evpoll_ops_t *)variant_ptr;
  return 1;
}

int arg_fnc_create_pipes(int argc, char *argv[], void *variant_ptr)
{
  char *p, *r;
  long l;

  if (argc <2)
    return -1;

  p = argv[1];

  l = strtol(p, &r, 0);

  if((p == r) || *r)
    return -1;

  while(l--) {
    if(add_my_pipe(500, 50, 10) < 0)
      return -1;
  }

  return 2;
}

#if defined(CONFIG_OC_UL_EVP_LNXEPOLL) && defined(CONFIG_OC_UL_EVP_GLIB)

int arg_fnc_setup_glib_cascaded_base(int argc, char *argv[], void *variant_ptr)
{
  ul_evpbase_t *glib_base;
  ul_evpbase_t *fast_base;

  if(ul_evpoll_base_implicit != NULL)
    return -1;

  glib_base = ul_evpoll_ops_glib.base_new();
  if(glib_base == NULL)
    return -1;

  ul_evpoll_base_implicit = glib_base;

  fast_base = ul_evpoll_ops_lnxepoll.base_new();
  if(fast_base == NULL)
    return -1;

  if(ul_evpoll_cascade(fast_base, glib_base, 0, 0) < 0) {
    ul_evpoll_destroy(fast_base);
    return -1;
  }

  ul_evpoll_base_implicit = fast_base;

  return 1;
}
#endif /* defined(CONFIG_OC_UL_EVP_LNXEPOLL) && defined(CONFIG_OC_UL_EVP_GLIB) */

const arg_table_entry_t arg_table[] = {
 #ifdef CONFIG_OC_UL_EVP_SELECT
  {"select", arg_fnc_set_evpoll_ops, (void *)&ul_evpoll_ops_select},
 #endif
 #ifdef CONFIG_OC_UL_EVP_LNXEPOLL
  {"lnxepoll", arg_fnc_set_evpoll_ops, (void *)&ul_evpoll_ops_lnxepoll},
 #endif
 #ifdef CONFIG_OC_UL_EVP_GLIB
  {"glib", arg_fnc_set_evpoll_ops, (void *)&ul_evpoll_ops_glib},
 #endif
 #ifdef CONFIG_OC_UL_EVP_LIBEVENT1
  {"libevent1", arg_fnc_set_evpoll_ops, (void *)&ul_evpoll_ops_libevent1},
 #endif
 #ifdef CONFIG_OC_UL_EVP_LIBEVENT2
  {"libevent2", arg_fnc_set_evpoll_ops, (void *)&ul_evpoll_ops_libevent2},
 #endif
 #ifdef CONFIG_OC_UL_EVP_WINEVENT
  {"winevent", arg_fnc_set_evpoll_ops, (void *)&ul_evpoll_ops_winevent},
 #endif
 #if defined(CONFIG_OC_UL_EVP_LNXEPOLL) && defined(CONFIG_OC_UL_EVP_GLIB)
  {"cascaded", arg_fnc_setup_glib_cascaded_base, NULL},
 #endif /* defined(CONFIG_OC_UL_EVP_LNXEPOLL) && defined(CONFIG_OC_UL_EVP_GLIB) */
  {"batch", arg_fnc_set_fl, &batch_run_fl},
  {"batch", arg_fnc_set_fl, &batch_run_fl},
 #ifdef WITH_TCP_PIPES_SUPPORT
  {"tcp", arg_fnc_set_fl, &use_socket_pair_fl},
 #endif /*WITH_TCP_PIPES_SUPPORT*/
  {"pipes", arg_fnc_create_pipes, NULL},
  {"tests", arg_fnc_set_str, &batch_tests_list},
  {"loglev", arg_fnc_set_int, &ulogd_evpoll.level},
  {"help", arg_fnc_help, NULL},
  {NULL, NULL, NULL},
};

int main(int argc, char *argv[])
{
  //ul_mstime_init();
  int ret;
  int i;
  char *p;

 #ifdef _WIN32
  WORD wVersionRequested;
  WSADATA wsaData;

  wVersionRequested = MAKEWORD(2, 0);
  WSAStartup(wVersionRequested, &wsaData);
 #endif

  ulogd_evpoll.level = UL_LOGL_DEB;
  //ulogd_evpoll.level = UL_LOGL_MSG;

  ul_logmsg("starting ul_evpchk application\n");

  for(i=1 ; i<argc; ) {
    const arg_table_entry_t *ae = arg_table;
    p = argv[i];
    while(*p == '-')
      p++;
    while (ae->arg_token != NULL) {
      if(!strcmp(ae->arg_token, p))
        break;
      ae++;
    }
    if(ae->arg_token == NULL) {
      ul_logerr("unknown argument token \"%s\"\n", argv[i]);
      exit(1);
    } else {
      ret = ae->arg_fnc(argc-i, argv+i, ae->variant_ptr);
      if(ret < 0) {
        ul_logerr("argument \"%s\" processing error\n", argv[i]);
        exit(1);
      }
      if(ret==0)
        return 0;
      i+=ret;
    }
  }

  if(!batch_run_fl && !batch_tests_list) {
    add_my_timer(5000, 0);
    add_my_timer(3000, 5);
    add_my_timer(1400, 10);
  }

  if(!batch_run_fl)
    add_my_fd(0, 20000);

  if(batch_tests_list) {
    for(i=0; batch_tests_list[i]; i++)
      my_fd_process_char(NULL, batch_tests_list[i]);
  }

  while(1) {
    if (batch_run_fl && (my_pipes_active_cnt <= 0))
      break;

    ul_logdeb("***** ul_evpoll_dispatch called *****\n");
    ret = ul_evpoll_dispatch(NULL, NULL);
    ul_logdeb("***** ul_evpoll_dispatch returned %d *****\n", ret);
    if(ret == UL_EVP_DISPRET_QUIT)
      break;
  }
  ul_logdeb("***** breaking loop destroying poll *****\n");
  ul_evpoll_destroy(NULL);
  ul_logdeb("***** breaking loop destroying poll *****\n");

  return 0;
}
