/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evp_winevent.c	- monitoring of open file handles

  (C) Copyright 2005-2012 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

//#include <ws2spi.h>
#include <winsock2.h>
#include <windows.h>

#include <errno.h>

#include "ul_evpoll.h"
#include "ul_evp_winevent.h"

#include <ul_gavl.h>
#include <ul_gavlcust.h>

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

UL_HTIMER_DEC_SCOPE(UL_ATTR_UNUSED static,
	      ul_evpwinevent_htimer, ul_evpbase_winevent_t, ul_evptrig_winevent_t, \
	      htim_queue, htim)

UL_HTIMER_IMP(ul_evpwinevent_htimer, ul_evpbase_winevent_t, ul_evptrig_winevent_t, \
	      htim_queue, htim)

static inline int
ul_evpwinevent_fd_cmp_fnc(const ul_evfd_t *a, const ul_evfd_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GAVL_CUST_NODE_INT_DEC_SCOPE(UL_ATTR_UNUSED static,
	ul_evpwinevent_fd, ul_evpbase_winevent_t, ul_evpwinevent_fdnode_t, ul_evfd_t,
	fd_root, node, fd, ul_evpwinevent_fd_cmp_fnc)

GAVL_CUST_NODE_INT_IMP(ul_evpwinevent_fd, ul_evpbase_winevent_t, ul_evpwinevent_fdnode_t, ul_evfd_t,
	fd_root, node, fd, ul_evpwinevent_fd_cmp_fnc)

#ifdef UL_HTIMER_WITH_MSTIME

ul_htim_time_t ul_evpwinevent_get_current_time(ul_evpbase_t *base)
{
  ul_mstime_t mstime;
  ul_htim_time_t htime;

  ul_mstime_now(&mstime);
  ul_mstime2htime(&htime, &mstime);

  return htime;
}

#endif /*UL_HTIMER_WITH_MSTIME*/

static inline ul_htim_time_t ul_evpwinevent_next_timeout(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_htim_time_t htime = ul_evpoll_get_current_time(base);
  ul_htime_add(&htime, &htime, timeout);
  return htime;
}

static inline int ul_evpwinevent_is_fd(ul_evptrig_winevent_t *ei)
{
  return (ei->fdnode != NULL);
}

static void ul_evpwinevent_print_state(ul_evpbase_winevent_t *eb)
{
  int i;
  ul_evpwinevent_evdata_t *evd;

  ul_logdeb("ul_evpwinevent: print state of base %p\n", eb);
  ul_logdeb("  win events used %d active %d:\n", eb->evused, eb->evactive);
  for(i=0; i < eb->evused; i++) {
    evd = eb->evdata[i];
    if(evd->evtype == EVP_WINEVENT_EVFD) {
      ul_evpwinevent_fdnode_t *efd;
      ul_evpwinevent_evdatafd_t *evdatafd;

      evdatafd = UL_CONTAINEROF(evd, ul_evpwinevent_evdatafd_t, evdata);
      ul_logdeb("    win event[%i] handle 0x%08lx type EVFD evidx %d:\n",
        i, (unsigned long)eb->evhandle[i], evd->evidx);

      ul_list_for_each(ul_evpwinevent_evdatafd_fdnode, evdatafd, efd) {
        ul_logdeb("      fd %d netevent 0x%lx\n",
          efd->fd, efd->for_netevents);
      }
    } else {
      ul_logdeb("    win event[%i] handle 0x%08lx type %d evidx %d\n",
        i, (unsigned long)eb->evhandle[i], evd->evtype, evd->evidx);
    }
  }
}


static inline void ul_evpwinevent_evdata_swap(ul_evpbase_winevent_t *eb,
                                 int idx1, int idx2)
{
  ul_evpwinevent_evdata_t *evd;
  HANDLE hndl;

  if(idx1 == idx2)
    return;

  evd = eb->evdata[idx2];
  hndl = eb->evhandle[idx2];

  eb->evhandle[idx2] = eb->evhandle[idx1];
  eb->evdata[idx2] = eb->evdata[idx1];
  eb->evdata[idx2]->evidx = idx2;

  eb->evhandle[idx1] = hndl;
  eb->evdata[idx1] = evd;
  evd->evidx = idx1;
}

static void ul_evpwinevent_evdata_arm(ul_evpbase_winevent_t *eb,
                                 ul_evpwinevent_evdata_t *evd)
{
  if(evd->evidx < eb->evactive)
    return;

  ul_evpwinevent_evdata_swap(eb, evd->evidx, eb->evactive++);
}

static void ul_evpwinevent_evdata_disarm(ul_evpbase_winevent_t *eb,
                                 ul_evpwinevent_evdata_t *evd)
{
  if(evd->evidx >= eb->evactive)
    return;

  ul_evpwinevent_evdata_swap(eb, evd->evidx, --eb->evactive);
}

static int ul_evpwinevent_evdata_insert(ul_evpbase_winevent_t *eb,
                                 ul_evpwinevent_evdata_t *evd, HANDLE hndl)
{
  if(eb->evused >= EVP_WINEVENT_EVMAX)
    return -1;

  if(hndl == INVALID_HANDLE_VALUE)
    hndl = CreateEvent(NULL, TRUE, FALSE, NULL);
  if(hndl == INVALID_HANDLE_VALUE)
    return -1;

  eb->evdata[eb->evused] = evd;
  eb->evhandle[eb->evused] = hndl;
  evd->evidx = eb->evused;

  eb->evused++;

  return 0;
}

static void ul_evpwinevent_evdata_delete(ul_evpbase_winevent_t *eb,
                                 ul_evpwinevent_evdata_t *evd)
{
  int evdidx = evd->evidx;

  if(--eb->evused != evdidx) {
    eb->evhandle[evdidx] = eb->evhandle[eb->evused];
    eb->evdata[evdidx] = eb->evdata[eb->evused];
    eb->evdata[evdidx]->evidx = evdidx;
  }

  eb->evhandle[eb->evused] = INVALID_HANDLE_VALUE;
  eb->evdata[eb->evused] = NULL;
}

static ul_evpwinevent_evdatafd_t *ul_evpwinevent_evdatafd_new(void)
{
  ul_evpwinevent_evdatafd_t *evdatafd;

  evdatafd = (ul_evpwinevent_evdatafd_t *)malloc(sizeof(ul_evpwinevent_evdatafd_t));

  evdatafd->evdata.evtype = EVP_WINEVENT_EVFD;
  ul_evpwinevent_evdatafd_fdnode_init_head(evdatafd);
  evdatafd->fdnode_cnt = 0;
  evdatafd->fd_armed_cnt = 0;

  return evdatafd;
}

static ul_evpwinevent_evdatafd_t *ul_evpwinevent_evdatafd_chose(ul_evpbase_winevent_t *eb)
{
  int i;
  int min_fdnode_cnt;
  ul_evpwinevent_evdatafd_t *evdatafd = NULL;

  if(eb->evused >= EVP_WINEVENT_EVMAX / 2) {
    for(i = 0; i < eb->evused; i++) {
      if(eb->evdata[i]->evtype == EVP_WINEVENT_EVFD) {
        ul_evpwinevent_evdatafd_t *evdatafd2;
        evdatafd2 = UL_CONTAINEROF(eb->evdata[i], ul_evpwinevent_evdatafd_t, evdata);
        if((evdatafd == NULL) || (evdatafd2->fdnode_cnt < min_fdnode_cnt)) {
          min_fdnode_cnt = evdatafd2->fdnode_cnt;
          evdatafd = evdatafd2;
        }
      }
    }
    if(evdatafd != NULL)
      return evdatafd;
  }

  evdatafd = ul_evpwinevent_evdatafd_new();
  if(evdatafd == NULL) {
    ul_logerr("malloc failed for winevent evdatafd\n");
    return NULL;
  }

  if(ul_evpwinevent_evdata_insert(eb, &evdatafd->evdata, INVALID_HANDLE_VALUE) < 0) {
    ul_logerr("insert failed for winevent evdatafd\n");
    free(evdatafd);
    return NULL;
  }
  return evdatafd;
}

static inline void ul_evpwinevent_evdatafd_armed_inc(ul_evpbase_winevent_t *eb,
                                             ul_evpwinevent_evdatafd_t *evdatafd)
{
  if(!evdatafd->fd_armed_cnt++)
    ul_evpwinevent_evdata_arm(eb, &evdatafd->evdata);

  if(evdatafd->fdnode_cnt < evdatafd->fd_armed_cnt)
        ul_logerr("ul_evpwinevent_evdatafd_armed_inc: fdnode_cnt < fd_armed_cnt\n");
}

static inline void ul_evpwinevent_evdatafd_armed_dec(ul_evpbase_winevent_t *eb,
                                             ul_evpwinevent_evdatafd_t *evdatafd)
{
  if(evdatafd->fd_armed_cnt <= 0) {
    ul_logerr("ul_evpwinevent_evdatafd_armed_dec: fd_armed_cnt <= 0\n");
    return;
  }
  if(!--evdatafd->fd_armed_cnt)
    ul_evpwinevent_evdata_disarm(eb, &evdatafd->evdata);
}

static void ul_evpwinevent_fdnode_sync(ul_evpbase_winevent_t *eb,
                                 ul_evpwinevent_fdnode_t *efd, int forced)
{
  long netevents = 0;
  unsigned what = 0;
  int errno;

  ul_evptrig_winevent_t *ei;
  ul_list_for_each(ul_evpwinevent_fdnode_trig, efd, ei) {
    if(ei->flags & EVP_WINEVENT_ARMED)
      what |= ei->what_events;
  }

  ul_evpwinevent_print_state(eb);

  if(what & UL_EVP_IN)
    netevents |= FD_READ | FD_ACCEPT | FD_CLOSE;
  if(what & UL_EVP_OUT)
    netevents |= FD_WRITE | FD_CONNECT | FD_CLOSE;
  if(what & UL_EVP_PRI)
    netevents |= FD_OOB;
  if(what & UL_EVP_ERR)
    netevents |= FD_CLOSE;
  if(what & UL_EVP_HUP)
    netevents |= FD_CLOSE;

  if((netevents != efd->for_netevents) || forced) {
    int op_add = netevents && !efd->for_netevents;
    int op_del = !netevents && efd->for_netevents;
    WSAEVENT evhandle = eb->evhandle[efd->evdatafd->evdata.evidx];

    if(op_del) {
      if(WSAEventSelect(efd->fd, NULL, netevents) == 0) {
        ul_logdeb("ul_evpwinevent_fdnode_sync: WSPEventSelect fd %d op DEL\n", efd->fd);

        efd->for_netevents = netevents;
        ul_evpwinevent_evdatafd_armed_dec(eb, efd->evdatafd);
      } else {
        ul_logerr("ul_evpwinevent_fdnode_sync: WSPEventSelect failed for fd %d op DEL err %d\n",
                 efd->fd, WSAGetLastError());
      }
    } else if(WSAEventSelect(efd->fd, evhandle, netevents) == 0) {
      ul_logdeb("ul_evpwinevent_fdnode_sync: WSPEventSelect fd %d op %s\n",
                 efd->fd, op_add? "ADD": op_del? "DEL": "MOD");

      if(FD_WRITE & (~efd->for_netevents & netevents)) {
        efd->active_netevents |= FD_WRITE;
        if(!SetEvent(evhandle))
            ul_logerr("ul_evpwinevent_fdnode_sync: SetEvent failed\n");
      }

      efd->for_netevents = netevents;

      if(op_add)
        ul_evpwinevent_evdatafd_armed_inc(eb, efd->evdatafd);

    } else {
      ul_logerr("ul_evpwinevent_fdnode_sync: WSPEventSelect failed for fd %d op %s err %d\n",
                 efd->fd, op_add? "ADD": op_del? "DEL": "MOD", WSAGetLastError());
    } 
  }

  ul_evpwinevent_print_state(eb);
}

static void ul_evpwinevent_fdnode_destroy(ul_evpbase_winevent_t *eb,
                                 ul_evpwinevent_fdnode_t *efd)
{
  ul_evpwinevent_evdatafd_t *evdatafd = efd->evdatafd;

  if(evdatafd != NULL) {
    ul_evpwinevent_evdatafd_fdnode_delete(evdatafd, efd);
    if(evdatafd->fdnode_cnt <= 0) {
      ul_logerr("ul_evpwinevent_fdnode_destroy: fdnode_cnt <= 0\n");
    } else {
      evdatafd->fdnode_cnt--;
      if(evdatafd->fdnode_cnt < evdatafd->fd_armed_cnt)
        ul_logerr("ul_evpwinevent_fdnode_destroy: fdnode_cnt < fd_armed_cnt\n");
      if(!evdatafd->fdnode_cnt) {
        HANDLE hndl = eb->evhandle[evdatafd->evdata.evidx];
        ul_evpwinevent_evdata_disarm(eb, &evdatafd->evdata);
        ul_evpwinevent_evdata_delete(eb, &evdatafd->evdata);

        if(hndl != INVALID_HANDLE_VALUE) {
          CloseHandle(hndl);
          ul_logdeb("ul_evpwinevent_fdnode_destroy: releasing event for fd %d\n",
                     efd->fd);
        }

        free(evdatafd);
      }
    }
  }

  free(efd);
}

static inline void ul_evpwinevent_arm_internal(ul_evpbase_winevent_t *eb,
                                 ul_evptrig_winevent_t *ei)
{
  if(ei->flags & EVP_WINEVENT_ARMED) {
    ul_logerr("ul_evpwinevent_arm_internal: tring to arm armed evptrig\n");
    return;
  }

  ei->pending_events = 0;

  ei->flags |= EVP_WINEVENT_ARMED;

  if((ei->evdataraw != NULL) && (ei->what_events & UL_EVP_SIGNAL))
    ul_evpwinevent_evdata_arm(eb, &ei->evdataraw->evdata);

  if(ul_evpwinevent_is_fd(ei))
    ul_evpwinevent_fdnode_sync(eb, ei->fdnode, 0);
}

static inline void ul_evpwinevent_disarm_internal(ul_evpbase_winevent_t *eb,
                                 ul_evptrig_winevent_t *ei)
{
  if(!(ei->flags & EVP_WINEVENT_ARMED)) {
    ul_logerr("ul_evpwinevent_arm_internal: tring to disarm idle evptrig\n");
    return;
  }

  ei->flags &= ~EVP_WINEVENT_ARMED;

  if(ei->evdataraw != NULL)
    ul_evpwinevent_evdata_disarm(eb, &ei->evdataraw->evdata);

  if(ul_evpwinevent_is_fd(ei))
    ul_evpwinevent_fdnode_sync(eb, ei->fdnode, 0);
}


static void ul_evpwinevent_trig_fd_unset(ul_evptrig_t *evptrig)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);

  if(ei->fdnode) {
    ul_evpwinevent_fdnode_t *efd = ei->fdnode;
    ul_evpwinevent_fdnode_trig_delete(efd, ei);
    ei->fdnode = NULL;
    if(ul_evpwinevent_fdnode_trig_is_empty(efd)) {
      efd->active_netevents = 0;
     #ifndef UL_EVP_WINEVENT_DELAYDEL
      ul_evpwinevent_fd_delete(eb, efd);
      ul_evpwinevent_fdnode_destroy(eb, efd);
     #else /*UL_EVP_WINEVENT_DELAYDEL*/
      ul_evpwinevent_delaydel_del_item(efd);
      efd->delaydel_generation = eb->delaydel_generation;
      ul_evpwinevent_delaydel_ins_tail(eb, efd);
     #endif /*UL_EVP_WINEVENT_DELAYDEL*/
    }
  }
}

static void ul_evpwinevent_trig_raw_unset(ul_evptrig_t *evptrig)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);

  if(ei->evdataraw) {
    ul_evpwinevent_evdata_disarm(eb, &ei->evdataraw->evdata);
    ul_evpwinevent_evdata_delete(eb, &ei->evdataraw->evdata);
    free(ei->evdataraw);
    ei->evdataraw = NULL;
  }
}

static int ul_evpwinevent_trig_raw_set(ul_evptrig_t *evptrig, HANDLE hndl)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int armed_fl = ei->flags & EVP_WINEVENT_ARMED;
  ul_evpwinevent_evdataraw_t *eraw;

  if(armed_fl)
    ul_evpwinevent_disarm_internal(eb, ei);

  ul_evpwinevent_trig_raw_unset(evptrig);

  if(hndl != INVALID_HANDLE_VALUE) {
    eraw = (ul_evpwinevent_evdataraw_t *)malloc(sizeof(*eraw));
    if(eraw == NULL) {
      ul_logerr("ul_evpwinevent_trig_raw_set: not enough memory\n");
      return -1;
    }
    memset(eraw, 0, sizeof(eraw));

    eraw->evdata.evtype = EVP_WINEVENT_EVRAW;
    eraw->evptrig = evptrig;

    if(ul_evpwinevent_evdata_insert(eb, &eraw->evdata, hndl) < 0) {
      ul_logerr("ul_evpwinevent_trig_raw_set: insert event failed\n");
      goto error_free_eraw;
    }

    ei->evdataraw = eraw;
    ei->what_events |= UL_EVP_SIGNAL;
  }

  if(armed_fl)
    ul_evpwinevent_arm_internal(eb, ei);

  return 0;

error_free_eraw:
  free(eraw);
  return -1;
}

static int ul_evpwinevent_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evptrig_winevent_t *ei;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);

  evptrig->cb = NULL;
  evptrig->base = base;
  evptrig->impl_data = (ul_evptrig_winevent_t*)malloc(sizeof(ul_evptrig_winevent_t));
  if(!evptrig->impl_data) {
    ul_logerr("ul_evpwinevent_trig_init: malloc failed\n");
    return -1;
  }
  ei = evptrig->impl_data;
  memset(ei, 0, sizeof(ul_evptrig_winevent_t));
  ei->fdnode = NULL;

  ul_evpwinevent_htimer_init_detached(ei);

  ei->trig_ptr = evptrig;

  ul_evpwinevent_trig_insert(eb, ei);

  return 0;
}

static void ul_evpwinevent_trig_done(ul_evptrig_t *evptrig)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;

  if(!ei)
    return;

  if(ei->flags & EVP_WINEVENT_ARMED)
    ul_evptrig_disarm(evptrig);

  ul_evpwinevent_trig_fd_unset(evptrig);
  ul_evpwinevent_trig_raw_unset(evptrig);

  ul_evpwinevent_trig_del_item(ei);

  free(ei);

  evptrig->impl_data = NULL;
}

static int ul_evpwinevent_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int armed_fl = ei->flags & EVP_WINEVENT_ARMED;
  ul_evpwinevent_fdnode_t *efd;
  ul_evpwinevent_evdatafd_t *evdatafd;

  if(armed_fl)
    ul_evpwinevent_disarm_internal(eb, ei);

  if(ei->fdnode)
    if(ei->fdnode->fd != fd)
      ul_evpwinevent_trig_fd_unset(evptrig);

  ei->what_events = what;
  if((fd>=0) && (!ei->fdnode)) {
    efd = ul_evpwinevent_fd_find(eb, &fd);
    if(efd == NULL) {
      efd = malloc(sizeof(ul_evpwinevent_fdnode_t));
      if(efd == NULL) {
        ul_logerr("malloc failed for winevent fdnode\n");
        return -1;
      }
      memset(efd, 0, sizeof(ul_evpwinevent_fdnode_t));
      ul_evpwinevent_fdnode_trig_init_head(efd);
     #ifdef UL_EVP_WINEVENT_DELAYDEL
      ul_evpwinevent_delaydel_init_detached(efd);
     #endif
      efd->fd = fd;

      evdatafd = ul_evpwinevent_evdatafd_chose(eb);
      if(evdatafd == NULL)
        goto error_free_efd;

      efd->evdatafd = evdatafd;
      ul_evpwinevent_evdatafd_fdnode_insert(evdatafd, efd);
      evdatafd->fdnode_cnt++;

      ul_evpwinevent_fd_insert(eb, efd);
    }
    ei->fdnode = efd;
    ul_evpwinevent_fdnode_trig_insert(efd, ei);
  }

  if(armed_fl)
    ul_evpwinevent_arm_internal(eb, ei);
  return 0;

error_free_efd:
  free(efd);
  return -1;
}

static int ul_evpwinevent_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);

  if((ei->flags & EVP_WINEVENT_ARMED) &&
     (ei->flags & EVP_WINEVENT_TIMED))
    ul_evpwinevent_htimer_detach(eb, ei);

  ei->flags &= ~(EVP_WINEVENT_TIMED | EVP_WINEVENT_TIMEOUT | EVP_WINEVENT_PERIODIC);

  if(!time)
    return 0;

  ul_evpwinevent_htimer_set_expire(ei, *time);

  ei->flags |= EVP_WINEVENT_TIMED;

  if(ei->flags & EVP_WINEVENT_ARMED)
    ul_evpwinevent_htimer_add(eb, ei);

  return 0;
}

static int ul_evpwinevent_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);

  if((ei->flags & EVP_WINEVENT_ARMED) &&
     (ei->flags & EVP_WINEVENT_TIMED))
    ul_evpwinevent_htimer_detach(eb, ei);

  ei->flags &= ~(EVP_WINEVENT_TIMED | EVP_WINEVENT_TIMEOUT | EVP_WINEVENT_PERIODIC);

  if(!timeout)
    return 0;

  ei->timeout = *timeout;

  ei->flags |= EVP_WINEVENT_TIMED | EVP_WINEVENT_TIMEOUT;

  if(ei->flags & EVP_WINEVENT_ARMED) {
    ul_evpwinevent_htimer_set_expire(ei, ul_evpwinevent_next_timeout(&eb->base, &ei->timeout));
    ul_evpwinevent_htimer_add(eb, ei);
  }

  return 0;
}

static int ul_evpwinevent_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  evptrig->cb = cb;
  return 0;
}

static int ul_evpwinevent_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);

  if(ei->flags & EVP_WINEVENT_ARMED)
    return 0;

  if(ei->flags & EVP_WINEVENT_TIMEOUT)
    ul_evpwinevent_htimer_set_expire(ei, ul_evpwinevent_next_timeout(&eb->base, &ei->timeout));

  if(ei->flags & EVP_WINEVENT_TIMED) {
    ul_evpwinevent_htimer_add(eb, ei);
  }

  ei->flags &= ~EVP_WINEVENT_ONCE;
  ul_evpwinevent_arm_internal(eb, ei);

  return 0;
}

static int ul_evpwinevent_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);

  if(!(ei->flags & EVP_WINEVENT_ARMED))
    return 0;

  if(ei->flags & EVP_WINEVENT_TIMED)
    ul_evpwinevent_htimer_detach(eb, ei);

  ul_evpwinevent_disarm_internal(eb, ei);

  return 0;
}

static int ul_evpwinevent_trig_arm_once(ul_evptrig_t *evptrig)
{
  int ret = 0;
  ul_evptrig_winevent_t *ei = evptrig->impl_data;
  if(!(ei->flags & EVP_WINEVENT_ARMED))
    ret = ul_evpwinevent_trig_arm(evptrig);
  ei->flags |= EVP_WINEVENT_ONCE;
  return ret;
}

static int ul_evpwinevent_trig_set_param(ul_evptrig_t *evptrig,
                           int parnum, const void *parval, int parsize)
{
  switch(parnum) {
    case UL_EVPTRIG_PARAM_WIN32EVENT:
       if(sizeof(HANDLE) != parsize)
         return -1;
       return ul_evpwinevent_trig_raw_set(evptrig, *(HANDLE*)parval);
  }
  return -1;
}

static int ul_evpwinevent_trig_get_param(ul_evptrig_t *evptrig,
                           int parnum, void *parval, int parmaxsize)
{
  return -1;
}

static ul_evpbase_t *ul_evpwinevent_base_new(void)
{
  ul_evpbase_winevent_t *eb;

  eb = (ul_evpbase_winevent_t*)malloc(sizeof(ul_evpbase_winevent_t));
  if(!eb)
    return NULL;

  memset(eb,0,sizeof(ul_evpbase_winevent_t));

  eb->evused = 0;
  eb->evactive = 0;

  ul_evpwinevent_htimer_init_queue(eb);

  ul_evpwinevent_trig_init_head(eb);
  ul_evpwinevent_report_init_head(eb);
 #ifdef UL_EVP_WINEVENT_DELAYDEL
  ul_evpwinevent_delaydel_init_head(eb);
 #endif /*UL_EVP_WINEVENT_DELAYDEL*/
 #ifdef UL_EVP_WINEVENT_CASCADE
  ul_evptrig_preinit_detached(&eb->cascade_trig);
 #endif /*UL_EVP_WINEVENT_CASCADE*/

  ul_evpwinevent_fd_init_root_field(eb);

  eb->base.ops = &ul_evpoll_ops_winevent;

  ul_logdeb("ul_evpwinevent_base_new: base created\n");

  return &eb->base;
}

static void ul_evpwinevent_base_destroy(ul_evpbase_t *base)
{
  ul_evptrig_winevent_t *ei;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);

  ul_list_for_each_cut(ul_evpwinevent_report, eb, ei) {
    ul_evpwinevent_trig_insert(eb, ei);
  }

  while((ei = ul_evpwinevent_trig_first(eb))!=NULL){
    if(ei->trig_ptr->cb)
      ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE for %i when poll is being destroyed\n", ei->fdnode?ei->fdnode->fd:-1);
    if(ei == ul_evpwinevent_trig_first(eb)) {
      ul_logdeb("UL_EVP_DONE not handled for %i when poll is being destroyed\n", ei->fdnode?ei->fdnode->fd:-1);
      ul_evpwinevent_trig_done(ei->trig_ptr);
    }
  }

 #ifdef UL_EVP_WINEVENT_DELAYDEL
  {
    ul_evpwinevent_fdnode_t *efd;
    ul_list_for_each_cut(ul_evpwinevent_delaydel, eb, efd) {
      if(ul_evpwinevent_fdnode_trig_is_empty(efd)) {
        ul_evpwinevent_fd_delete(eb, efd);
        ul_evpwinevent_fdnode_destroy(eb, efd);
      }
    }
  }
 #endif /*UL_EVP_WINEVENT_DELAYDEL*/

  if(!ul_evpwinevent_fd_is_empty(eb)) {
    ul_logerr("ul_evpwinevent_base_destroy: not all fdnode destroyed\n");
  }

 #ifdef UL_EVP_WINEVENT_CASCADE
  if(!ul_evptrig_is_detached(&eb->cascade_trig)) {
    ul_evptrig_disarm(&eb->cascade_trig);
    ul_evptrig_done(&eb->cascade_trig);
  }
 #endif /*UL_EVP_WINEVENT_CASCADE*/

  ul_logdeb("ul_evpwinevent_base_destroy: base done\n");

  free(eb);
}

static int ul_evpwinevent_base_update(ul_evpbase_t *base)
{
  return 0;
}

static int ul_evpwinevent_dispatch_evraw_internal(ul_evpbase_winevent_t *eb, int evidx)
{
  ul_evpwinevent_evdataraw_t *evdataraw;
  ul_evptrig_winevent_t *ei;

  evdataraw = UL_CONTAINEROF(eb->evdata[evidx], ul_evpwinevent_evdataraw_t, evdata);
  if(evdataraw->evptrig != NULL) {
    ei = evdataraw->evptrig->impl_data;

    ei->pending_events = UL_EVP_SIGNAL;
    ul_evpwinevent_trig_del_item(ei);
    ul_evpwinevent_report_ins_tail(eb,ei);
  }

  return 0;
}

#define UL_EVPWIN_SELECT_CHUNK 10

static int ul_evpwinevent_dispatch_evfd_internal(ul_evpbase_winevent_t *eb, int evidx)
{
  /* The signaled event corresponds to WSAEventSelect FD/SOCKET notification */
  WSANETWORKEVENTS netretevents;
  int what = 0;
  int set_event_again = 0;
  ul_evpwinevent_fdnode_t *efd;
  ul_evpwinevent_evdatafd_t *evdatafd;
  ul_evptrig_winevent_t *ei;
  int fds_cnt;
  fd_set *readfds, *writefds, *exceptfds;
  struct { char data[
      UL_OFFSETOF(fd_set, fd_count) > UL_OFFSETOF(fd_set, fd_array)?
        sizeof(fd_set):
        UL_OFFSETOF(fd_set, fd_array[UL_EVPWIN_SELECT_CHUNK])
  ]; } readfds_space, writefds_space, exceptfds_space;
  struct timeval timeout_tv;

  evdatafd = UL_CONTAINEROF(eb->evdata[evidx], ul_evpwinevent_evdatafd_t, evdata);

  if(evdatafd->fdnode_cnt <= 0)
    return 0;

  readfds = (fd_set *)&readfds_space.data;
  writefds = (fd_set *)&writefds_space.data;
  exceptfds = (fd_set *)&exceptfds_space.data;

  fds_cnt = 0;
  efd = ul_evpwinevent_evdatafd_fdnode_first(evdatafd);

  ul_list_for_each(ul_evpwinevent_evdatafd_fdnode, evdatafd, efd) {

    if(!efd->for_netevents)
      continue;

    if(WSAEnumNetworkEvents(efd->fd, 0, &netretevents) != 0) {
      ul_logerr("ul_evpwinevent_base_dispatch: WSAEnumNetworkEvents failed for fd %d err %d\n",
                 efd->fd, WSAGetLastError());
      continue;
    }
    ul_logdeb("ul_evpwinevent_base_dispatch: WSAEnumNetworkEvents fd %d events 0x%lx | 0x%lx\n",
               efd->fd, netretevents.lNetworkEvents, efd->active_netevents);

    if(efd->active_netevents & FD_WRITE) {
      int ret;

      timeout_tv.tv_sec = 0;
      timeout_tv.tv_usec = 0;

      FD_ZERO(writefds);
      FD_SET(efd->fd, writefds);
      ret = select(1, NULL, writefds,
               NULL, &timeout_tv);
      if(ret <= 0)
        efd->active_netevents &= ~FD_WRITE;

      /*ul_evpwinevent_fdnode_sync(eb, efd, 1);*/
    }

    netretevents.lNetworkEvents |= efd->active_netevents;

    if(!netretevents.lNetworkEvents) continue;

    efd->active_netevents |= netretevents.lNetworkEvents & FD_WRITE;

    if(efd->active_netevents)
      set_event_again = 1;

    if(netretevents.lNetworkEvents & (FD_READ | FD_ACCEPT | FD_CLOSE))
      what |= UL_EVP_IN;
    if(netretevents.lNetworkEvents & (FD_WRITE | FD_CONNECT | FD_CLOSE))
      what |= UL_EVP_OUT;
    if(netretevents.lNetworkEvents & FD_OOB)
      what |= UL_EVP_PRI;
    /*if(netretevents.lNetworkEvents & FD_???)
      what |= UL_EVP_ERR;*/
    if(netretevents.lNetworkEvents & FD_CLOSE)
      what |= UL_EVP_HUP;

    ul_list_for_each(ul_evpwinevent_fdnode_trig, efd, ei) {
      if(!(ei->what_events & what) || !(ei->flags & EVP_WINEVENT_ARMED))
        continue;
      ei->pending_events |= ei->what_events & what;
      ul_evpwinevent_trig_del_item(ei);
      ul_evpwinevent_report_ins_tail(eb,ei);
    }
  }
  if(set_event_again)
    SetEvent(eb->evhandle[evidx]);

  return 0;
}

static int ul_evpwinevent_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);
  ul_msdiff_t mstimeout = -1;
  int ret;
  int i;
  DWORD waitforres;

  if(eb->quit_loop)
    return UL_EVP_DISPRET_QUIT;

  if(timeout!=NULL) {
    ul_htimdiff2ms(&mstimeout, timeout);
    if(mstimeout < 0)
      mstimeout = 0;
  }

  if(ul_evpwinevent_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evpwinevent_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    ul_msdiff_t msdiff;
    eb->time_act = ul_evpoll_get_current_time(base);
    ul_htime_sub2ms(&msdiff, &eb->time_next_expire, &eb->time_act);
    if(msdiff < 0)
      msdiff = 0;
    if((mstimeout < 0) || (mstimeout > msdiff))
      mstimeout = msdiff;
  }

  if(!eb->evactive) {
    waitforres = SleepEx (mstimeout, TRUE);
    /* 0 or WAIT_IO_COMPLETION */
    if(waitforres == 0)
      ret = UL_EVP_DISPRET_TIMEOUT;
    else
      ret = UL_EVP_DISPRET_ERROR;
  } else {
    waitforres = WaitForMultipleObjectsEx(eb->evactive, eb->evhandle, FALSE,
                               mstimeout, TRUE);

    /* WAIT_OBJECT_0 WAIT_ABANDONED_0 WAIT_IO_COMPLETION WAIT_TIMEOUT WAIT_FAILED */

    ret = UL_EVP_DISPRET_ERROR;
    if(waitforres == WAIT_FAILED) {
      ul_logerr("ul_evpwinevent_base_dispatch: WaitForMultipleObjectsEx WAIT_FAILED\n");
      ret = UL_EVP_DISPRET_ERROR;
    } else if(waitforres == WAIT_TIMEOUT) {
      ul_logdeb("ul_evpwinevent_base_dispatch: WaitForMultipleObjectsEx WAIT_TIMEOUT\n");
      ret = UL_EVP_DISPRET_TIMEOUT;
    } else if((waitforres >= WAIT_OBJECT_0) && (waitforres < WAIT_ABANDONED_0+EVP_WINEVENT_EVMAX)) {
      /* At least one event is in signaled state. */
      ul_evptrig_winevent_t *ei;

      ul_logdeb("ul_evpwinevent_base_dispatch: WaitForMultipleObjectsEx 0x%lx\n", (unsigned long)waitforres);

      ret = UL_EVP_DISPRET_HANDLED;
      int firstev;
      if(waitforres < WAIT_ABANDONED_0)
        firstev = waitforres - WAIT_OBJECT_0;
      else
        firstev = waitforres - WAIT_ABANDONED_0;

      for(i=firstev; i<eb->evactive; i++) {
        waitforres = WaitForSingleObject(eb->evhandle[i], 0);
        ul_logdeb("ul_evpwinevent_base_dispatch: WaitForSingleObject 0x%lx\n", (unsigned long)waitforres);

        if(waitforres == WAIT_TIMEOUT)
          continue;

        if(waitforres != WAIT_OBJECT_0) {
          ul_logerr("ul_evpwinevent_base_dispatch: WaitForSingleObject failed for handle %ld err %ld\n",
            (long)eb->evhandle[i], waitforres);
          continue;
        } else {
          ResetEvent(eb->evhandle[i]);

          if(eb->evdata[i]->evtype == EVP_WINEVENT_EVRAW) {
            ul_evpwinevent_dispatch_evraw_internal(eb, i);
          } else if(eb->evdata[i]->evtype == EVP_WINEVENT_EVFD) {
            ul_evpwinevent_dispatch_evfd_internal(eb, i);
          }
        }
      }

      ul_list_for_each_cut(ul_evpwinevent_report, eb, ei) {
        unsigned pending = ei->pending_events;
        ul_evpwinevent_trig_insert(eb, ei);
        if(!(ei->what_events & pending) ||
           !(ei->flags & EVP_WINEVENT_ARMED))
          continue;
        ei->pending_events &= ~ei->what_events;
        if(ei->flags & EVP_WINEVENT_ONCE)
          ul_evpwinevent_trig_disarm(ei->trig_ptr);
        if(ei->flags & EVP_WINEVENT_TIMEOUT)
          ul_evpwinevent_trig_set_timeout(ei->trig_ptr, &ei->timeout);
        if(ei->trig_ptr->cb) {
          ei->trig_ptr->cb(ei->trig_ptr, ei->what_events & pending);
        }
      }
    }
  }

 #ifdef UL_EVP_WINEVENT_DELAYDEL
  {
    ul_evpwinevent_fdnode_t *efd;
    while((efd = ul_evpwinevent_delaydel_first(eb)) != NULL) {
      if((int)(efd->delaydel_generation - eb->delaydel_generation) >= 0)
        break;
      ul_evpwinevent_delaydel_del_item(efd);
      if(ul_evpwinevent_fdnode_trig_is_empty(efd)) {
        ul_evpwinevent_fd_delete(eb, efd);
        ul_evpwinevent_fdnode_destroy(eb, efd);
      }
    }
    eb->delaydel_generation++;
  }
 #endif /*UL_EVP_WINEVENT_DELAYDEL*/

  if(ul_evpwinevent_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evpwinevent_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    ul_evptrig_winevent_t *ei;
    eb->time_act = ul_evpoll_get_current_time(base);

    while((ei=ul_evpwinevent_htimer_cut_expired(eb, &eb->time_act))) {
      if(ei->trig_ptr->cb)
        ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_TIMEOUT);
      ret = UL_EVP_DISPRET_HANDLED;
    }

    if(ul_evpwinevent_htimer_first_changed(eb))
      eb->time_next_expire_fl=ul_evpwinevent_htimer_next_expire(eb,&eb->time_next_expire);
  }

  return ret;
}

static int ul_evpwinevent_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);

  switch(option) {
    case UL_EVP_OPTION_QUIT:
      if(val)
        eb->quit_loop = 1;
      return 0;
   }
  return -1;
}

#ifdef UL_EVP_WINEVENT_CASCADE
static int ul_evpwinevent_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags);
#endif /*UL_EVP_WINEVENT_CASCADE*/

const ul_evpoll_ops_t ul_evpoll_ops_winevent = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "winevent",
  .trig_init = ul_evpwinevent_trig_init,
  .trig_done = ul_evpwinevent_trig_done,
  .trig_set_fd = ul_evpwinevent_trig_set_fd,
  .trig_set_time = ul_evpwinevent_trig_set_time,
  .trig_set_timeout = ul_evpwinevent_trig_set_timeout,
  .trig_set_callback = ul_evpwinevent_trig_set_callback,
  .trig_arm = ul_evpwinevent_trig_arm,
  .trig_disarm = ul_evpwinevent_trig_disarm,
  .trig_arm_once = ul_evpwinevent_trig_arm_once,
  .trig_set_param = ul_evpwinevent_trig_set_param,
  .trig_get_param = ul_evpwinevent_trig_get_param,
  .base_new = ul_evpwinevent_base_new,
  .base_destroy = ul_evpwinevent_base_destroy,
  .base_update = ul_evpwinevent_base_update,
  .base_dispatch = ul_evpwinevent_base_dispatch,
  .base_get_current_time = ul_evpwinevent_get_current_time,
  .base_set_option = ul_evpwinevent_base_set_option,
 #ifdef UL_EVP_WINEVENT_CASCADE
  .base_cascade = ul_evpwinevent_base_cascade,
 #endif /*UL_EVP_WINEVENT_CASCADE*/
};

#ifdef UL_EVP_WINEVENT_CASCADE

static const ul_evpoll_ops_t ul_evpoll_ops_winevent_cascade;

static int ul_evpwineventcas_update(ul_evpbase_winevent_t *eb, int update_exptime)
{
  int arm_fl;

  if(ul_evptrig_is_detached(&eb->cascade_trig))
    return 0;

  if(ul_evpwinevent_htimer_first_changed(eb)) {
    eb->time_next_expire_fl=ul_evpwinevent_htimer_next_expire(eb,&eb->time_next_expire);
    update_exptime = 1;
  }
  arm_fl = eb->time_next_expire_fl;

 #ifdef UL_EVP_WINEVENT_DELAYDEL
  if(!ul_evpwinevent_delaydel_is_empty(eb)) {
    arm_fl = 1;
    update_exptime = 2;
  }
 #endif

  if(!ul_evpwinevent_fd_is_empty(eb))
    arm_fl = 1;

  if(eb->cascade_armed_fl && !arm_fl) {
    ul_evptrig_disarm(&eb->cascade_trig);
    eb->cascade_armed_fl = 0;
  }

  if(update_exptime) {
    int exptime_fl = eb->time_next_expire_fl;
    ul_htim_time_t exptime = eb->time_next_expire;
    if(update_exptime == 2) {
      exptime_fl = 1;
      exptime = ul_evpoll_get_current_time(eb->cascade_trig.base);
    }
    ul_evptrig_set_time(&eb->cascade_trig, exptime_fl? &exptime: NULL);
  }

  if(!eb->cascade_armed_fl) {
    ul_evptrig_arm(&eb->cascade_trig);
    eb->cascade_armed_fl = 1;
  }

  return 0;
}

static void ul_evpwineventcas_cb(ul_evptrig_t *evptrig, int what)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig, ul_evpbase_winevent_t, cascade_trig);

  if(what == UL_EVP_DONE) {
    ul_evptrig_done(&eb->cascade_trig);
    eb->base.ops = &ul_evpoll_ops_winevent;
    if(eb->cascade_inerit_destroy_fl)
      ul_evpoll_destroy(&eb->base);
    return;
  }

  if(what & (UL_EVP_TIMEOUT | UL_EVP_IN)) {
    int exptime_fl = eb->time_next_expire_fl;
    ul_htim_time_t exptime = eb->time_next_expire;
    ul_htim_diff_t timeout_0 = 0;
    int update_exptime;
    int ret;

    ret = ul_evpwinevent_base_dispatch(&eb->base, &timeout_0);
    if(ret == UL_EVP_DISPRET_QUIT) {
      /* ul_evptrig_disarm(&eb->cascade_trig);
         eb->cascade_armed_fl = 0; */
      /* ul_evpoll_destroy(&eb->base); */
      return;
    }

    if(what & UL_EVP_TIMEOUT)
      exptime_fl = 0;

    update_exptime = (exptime_fl != eb->time_next_expire_fl) || (exptime != eb->time_next_expire) ||
                     (what & UL_EVP_TIMEOUT);
    ul_evpwineventcas_update(eb, update_exptime);
  }
}

static int ul_evpwinevent_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);
  if(!ul_evptrig_is_detached(&eb->cascade_trig)) {
    ul_logerr("ul_evpwinevent_base_cascade: already cascaded\n");
    return -1;
  }

  if(ul_evptrig_init(new_base, &eb->cascade_trig) < 0)
    return -1;

  if(ul_evptrig_set_callback(&eb->cascade_trig, ul_evpwineventcas_cb) < 0)
    goto cascade_error;

  if(ul_evptrig_set_fd(&eb->cascade_trig, eb->epoll_fd, UL_EVP_IN) < 0)
    goto cascade_error;

  eb->base.ops = &ul_evpoll_ops_winevent_cascade;

  eb->cascade_inerit_destroy_fl = (bc_flags & UL_EVP_CASFL_INHERIT_DESTROY)? 1: 0;
  eb->cascade_propagate_destroy_fl = (bc_flags & UL_EVP_CASFL_PROPAGATE_DESTROY)? 1: 0;

  ul_evpwineventcas_update(eb, 1);

  return 0;

cascade_error:
  ul_logerr("ul_evpwinevent_base_cascade: cascade setup error\n");
  ul_evptrig_done(&eb->cascade_trig);
  return -1;
}

static void ul_evpwineventcas_trig_done(ul_evptrig_t *evptrig)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  ul_evpwinevent_trig_done(evptrig);
  ul_evpwineventcas_update(eb, 0);
}

static int ul_evpwineventcas_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int ret;
  ret = ul_evpwinevent_trig_set_fd(evptrig, fd, what);
  ul_evpwineventcas_update(eb, 0);
  return ret;
}

static int ul_evpwineventcas_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int ret;
  ret = ul_evpwinevent_trig_set_time(evptrig, time);
  ul_evpwineventcas_update(eb, 0);
  return ret;
}

static int ul_evpwineventcas_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int ret;
  ret = ul_evpwinevent_trig_set_timeout(evptrig, timeout);
  ul_evpwineventcas_update(eb, 0);
  return ret;
}

static int ul_evpwineventcas_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int ret;
  ret = ul_evpwinevent_trig_arm(evptrig);
  ul_evpwineventcas_update(eb, 0);
  return ret;
}

static int ul_evpwineventcas_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int ret;
  ret = ul_evpwinevent_trig_disarm(evptrig);
  ul_evpwineventcas_update(eb, 0);
  return ret;
}

static int ul_evpwineventcas_trig_arm_once(ul_evptrig_t *evptrig)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_winevent_t, base);
  int ret;
  ret = ul_evpwinevent_trig_arm_once(evptrig);
  ul_evpwineventcas_update(eb, 0);
  return ret;
}

static void ul_evpwineventcas_base_destroy(ul_evpbase_t *base)
{
  ul_evpbase_t *cascade_base = NULL;
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);
  if(eb->cascade_propagate_destroy_fl)
    cascade_base = ul_evptrig_get_base(&eb->cascade_trig);
  ul_evptrig_disarm(&eb->cascade_trig);
  ul_evptrig_done(&eb->cascade_trig);
  ul_evpwinevent_base_destroy(base);
  if(cascade_base != NULL)
    ul_evpoll_destroy(cascade_base);
}

static int ul_evpwineventcas_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);
  return ul_evpoll_dispatch(eb->cascade_trig.base, timeout);
}

static ul_htim_time_t ul_evpwineventcas_get_current_time(ul_evpbase_t *base)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);
  return ul_evpoll_get_current_time(eb->cascade_trig.base);
}

static int ul_evpwineventcas_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_winevent_t *eb = UL_CONTAINEROF(base, ul_evpbase_winevent_t, base);
  ul_evpwinevent_base_set_option(base, option, val);
  return ul_evpoll_set_option(eb->cascade_trig.base, option, val);
}

static const ul_evpoll_ops_t ul_evpoll_ops_winevent_cascade = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "winevent_cascade",
  .trig_init = ul_evpwinevent_trig_init,
  .trig_done = ul_evpwineventcas_trig_done,
  .trig_set_fd = ul_evpwineventcas_trig_set_fd,
  .trig_set_time = ul_evpwineventcas_trig_set_time,
  .trig_set_timeout = ul_evpwineventcas_trig_set_timeout,
  .trig_set_callback = ul_evpwinevent_trig_set_callback,
  .trig_arm = ul_evpwineventcas_trig_arm,
  .trig_disarm = ul_evpwineventcas_trig_disarm,
  .trig_arm_once = ul_evpwineventcas_trig_arm_once,
  .trig_set_param = ul_evpwinevent_trig_set_param,
  .trig_get_param = ul_evpwinevent_trig_get_param,
  .base_new = ul_evpwinevent_base_new,
  .base_destroy = ul_evpwineventcas_base_destroy,
  .base_update = ul_evpwinevent_base_update,
  .base_dispatch = ul_evpwineventcas_base_dispatch,
  .base_get_current_time = ul_evpwineventcas_get_current_time,
  .base_set_option = ul_evpwineventcas_base_set_option,
  .base_cascade = NULL,
};

#endif /*UL_EVP_WINEVENT_CASCADE*/
