/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpglib.c	- monitoring of open file handles

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#include <poll.h>

#include "ul_evpoll.h"
#include "ul_evp_glib.h"

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

/*
GSource *g_source_new(GSourceFuncs *source_funcs, guint struct_size);
GSource *g_source_ref(GSource *source);
void g_source_unref(GSource *source);

gboolean (*GSourceFunc)(gpointer data); - FALSE to remove
void g_source_set_callback(GSource *source, GSourceFunc func, gpointer data, GDestroyNotify notify);

guint g_source_attach(GSource *source, GMainContext *context);

void g_source_add_poll(GSource *source, GPollFD *fd);
void g_source_remove_poll(GSource *source, GPollFD *fd);
void g_source_get_current_time(GSource *source, GTimeVal *timeval);
*/

static inline ul_htim_time_t gtimeval2htim(GTimeVal *timeval)
{
  ul_htim_time_t htime;
  glong sec=timeval->tv_sec;
  ul_mstime_t mstime = sec*1000+timeval->tv_usec/1000;
  ul_mstime2htime(&htime, &mstime);
  return htime;
}

ul_htim_time_t ul_evpglib_get_current_time(ul_evpbase_t *base)
{
  GTimeVal timeval;
  g_get_current_time(&timeval);
  return gtimeval2htim(&timeval);
}

ul_htim_time_t ul_evpglib_source_get_current_time(GSource *source)
{
  GTimeVal timeval;
  g_source_get_current_time(source, &timeval);
  return gtimeval2htim(&timeval);
}

static inline ul_htim_time_t ul_evpglib_next_timeout(ul_evptrig_glib_t *ei, ul_htim_diff_t *timeout)
{
  ul_htim_time_t htime = ul_evpglib_source_get_current_time(&ei->source);
  ul_htime_add(&htime, &htime, timeout);
  return htime;
}

static inline int ul_evpglib_is_fd(ul_evptrig_glib_t *ei)
{
  return ei->pollfd.events && (ei->pollfd.fd >= 0);
}

static inline void ul_evpglib_arm_internal(ul_evptrig_glib_t *ei)
{
  if(ei->flags & UL_EVPGLIB_ARMED) {
    ul_logerr("ul_evpglib_arm_internal: tring to arm armed evptrig\n");
    return;
  }

  ei->flags |= UL_EVPGLIB_ARMED;

  if(ul_evpglib_is_fd(ei)) {
    ei->pollfd.revents = 0;
    g_source_add_poll(&ei->source, &ei->pollfd);
  }
}

static inline void ul_evpglib_disarm_internal(ul_evptrig_glib_t *ei)
{
  if(!(ei->flags & UL_EVPGLIB_ARMED)) {
    ul_logerr("ul_evpglib_arm_internal: tring to disarm idle evptrig\n");
    return;
  }

  if(ul_evpglib_is_fd(ei)) {
    g_source_remove_poll(&ei->source, &ei->pollfd);
  }

  ei->flags &= ~UL_EVPGLIB_ARMED;
}

static gboolean ul_evpglib_source_prepare  (GSource     *source,
                                    gint        *timeout);
static gboolean ul_evpglib_source_check    (GSource     *source);
static gboolean ul_evpglib_source_dispatch (GSource     *source,
                                    GSourceFunc  callback,
                                    gpointer     user_data);
static void     ul_evpglib_source_finalize (GSource     *source);

static GSourceFuncs ul_evpglib_source_funcs = {
  ul_evpglib_source_prepare,
  ul_evpglib_source_check,
  ul_evpglib_source_dispatch,
  ul_evpglib_source_finalize
};

static int ul_evpglib_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evptrig_glib_t *ei;
  ul_evpbase_glib_t *eb = UL_CONTAINEROF(base, ul_evpbase_glib_t, base);
  GSource           *gsrc;
  GMainContext      *gmc = eb->main_context;

  evptrig->cb = NULL;
  evptrig->base = base;

  gsrc = g_source_new(&ul_evpglib_source_funcs, sizeof(ul_evptrig_glib_t));
  ei = UL_CONTAINEROF(gsrc, ul_evptrig_glib_t, source);
  evptrig->impl_data = ei;

  if(!ei) {
    ul_logerr("ul_evpglib_trig_init: g_source_new failed\n");
    return -1;
  }

  ei->flags = 0;
  ei->pollfd.fd = -1;
  ei->pollfd.events = 0;
  ei->pollfd.revents = 0;
  ei->trig_ptr = evptrig;

  ul_evpglib_trig_insert(eb, ei);

  ei->source_id = g_source_attach(&ei->source, gmc);

  return 0;
}

static void ul_evpglib_trig_done(ul_evptrig_t *evptrig)
{
  ul_evptrig_glib_t *ei = evptrig->impl_data;

  if(!ei)
    return;

  if(ei->flags & UL_EVPGLIB_ARMED)
    ul_evptrig_disarm(evptrig);

  ei->trig_ptr = NULL;
  evptrig->impl_data = NULL;
  ul_evpglib_trig_del_item(ei);

  g_source_destroy(&ei->source);
}

static int ul_evpglib_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evptrig_glib_t *ei = evptrig->impl_data;
  int cond = 0;

  if((UL_EVP_IN == G_IO_IN) && (UL_EVP_OUT == G_IO_OUT) &&
     (UL_EVP_PRI == G_IO_PRI) && (UL_EVP_ERR == G_IO_ERR) &&
     (UL_EVP_HUP == G_IO_HUP) && (UL_EVP_NVAL == G_IO_NVAL)) {
    cond |= what & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_PRI |
                    UL_EVP_ERR | UL_EVP_HUP | UL_EVP_NVAL);
  } else {
    if(what & UL_EVP_IN)
      cond |= G_IO_IN;
    if(what & UL_EVP_OUT)
      cond |= G_IO_OUT;
    if(what & UL_EVP_PRI)
      cond |= G_IO_PRI;
    if(what & UL_EVP_ERR)
      cond |= G_IO_ERR;
    if(what & UL_EVP_HUP)
      cond |= G_IO_HUP;
    if(what & UL_EVP_NVAL)
      cond |= G_IO_NVAL;
  }

  if(ei->flags & UL_EVPGLIB_ARMED) {
    ul_evpglib_disarm_internal(ei);
    ei->pollfd.fd = fd;
    ei->pollfd.events = cond;
    ul_evpglib_arm_internal(ei);
  } else {
    ei->pollfd.fd = fd;
    ei->pollfd.events = cond;
  }
  return 0;
}

static int ul_evpglib_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evptrig_glib_t *ei = evptrig->impl_data;

  ei->flags &= ~(UL_EVPGLIB_TIMED | UL_EVPGLIB_TIMEOUT | UL_EVPGLIB_PERIODIC);

  if(!time)
    return 0;

  ei->expire = *time;

  ei->flags |= UL_EVPGLIB_TIMED;

  return 0;
}

static int ul_evpglib_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evptrig_glib_t *ei = evptrig->impl_data;

  ei->flags &= ~(UL_EVPGLIB_TIMED | UL_EVPGLIB_TIMEOUT | UL_EVPGLIB_PERIODIC);

  if(!timeout)
    return 0;

  ei->timeout = *timeout;

  ei->flags |= UL_EVPGLIB_TIMED | UL_EVPGLIB_TIMEOUT;

  if(ei->flags & UL_EVPGLIB_ARMED) {
    ei->expire = ul_evpglib_next_timeout(ei, &ei->timeout);
  }

  return 0;
}

static int ul_evpglib_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  evptrig->cb = cb;
  return 0;
}

static int ul_evpglib_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evptrig_glib_t *ei = evptrig->impl_data;

  if(ei->flags & UL_EVPGLIB_ARMED)
    return 0;

  if(ei->flags & UL_EVPGLIB_TIMEOUT) {
    ei->expire = ul_evpglib_next_timeout(ei, &ei->timeout);
    ei->flags |= UL_EVPGLIB_TIMED;
  }

  ei->flags &= ~UL_EVPGLIB_ONCE;
  ul_evpglib_arm_internal(ei);

  return 0;
}

static int ul_evpglib_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evptrig_glib_t *ei = evptrig->impl_data;

  if(!(ei->flags & UL_EVPGLIB_ARMED))
    return 0;

  ul_evpglib_disarm_internal(ei);

  return 0;
}

static int ul_evpglib_trig_arm_once(ul_evptrig_t *evptrig)
{
  int ret = 0;
  ul_evptrig_glib_t *ei = evptrig->impl_data;
  if(!(ei->flags & UL_EVPGLIB_ARMED))
    ret = ul_evpglib_trig_arm(evptrig);
  ei->flags |= UL_EVPGLIB_ONCE;
  return ret;
}

static int ul_evpglib_trig_set_param(ul_evptrig_t *evptrig,
                           int parnum, const void *parval, int parsize)
{
  return -1;
}

static int ul_evpglib_trig_get_param(ul_evptrig_t *evptrig,
                           int parnum, void *parval, int parmaxsize)
{
  return -1;
}

static gboolean ul_evpglib_source_prepare(GSource *source, gint *timeout)
{
  ul_msdiff_t mstimeout = -1;
  ul_evptrig_glib_t *ei = UL_CONTAINEROF(source, ul_evptrig_glib_t, source);

  if(!(~ei->flags & (UL_EVPGLIB_TIMED | UL_EVPGLIB_ARMED))) {
    ul_htim_time_t actual_time;
    actual_time = ul_evpglib_source_get_current_time(&ei->source);

    ul_htime_sub2ms(&mstimeout, &ei->expire, &actual_time);

    if(mstimeout < 0)
      mstimeout = 0;
  }
  *timeout = mstimeout;

  return mstimeout? FALSE: TRUE;
}

static gboolean ul_evpglib_source_check(GSource *source)
{
  ul_evptrig_glib_t *ei = UL_CONTAINEROF(source, ul_evptrig_glib_t, source);
  ul_htim_diff_t diff;
  ul_htim_time_t actual_time;

  if(!(ei->flags & UL_EVPGLIB_ARMED))
    return FALSE;

  if(ei->pollfd.revents & ei->pollfd.events)
    return TRUE;

  if(!(ei->flags & UL_EVPGLIB_TIMED))
    return FALSE;

  actual_time = ul_evpglib_source_get_current_time(&ei->source);
  ul_htime_sub(&diff, &ei->expire, &actual_time);
  if(diff < 0)
    return TRUE;

  return FALSE;
}

static gboolean ul_evpglib_source_dispatch (GSource *source,
                                    GSourceFunc  callback,
                                    gpointer     user_data)
{
  /* it should return FALSE if the source should be removed. */
  ul_evptrig_glib_t *ei = UL_CONTAINEROF(source, ul_evptrig_glib_t, source);
  int what = 0;
  int cond = ei->pollfd.events & ei->pollfd.revents;

  if(!(ei->flags & UL_EVPGLIB_ARMED))
    return TRUE;


  if((UL_EVP_IN == G_IO_IN) && (UL_EVP_OUT == G_IO_OUT) &&
     (UL_EVP_PRI == G_IO_PRI) && (UL_EVP_ERR == G_IO_ERR) &&
     (UL_EVP_HUP == G_IO_HUP) && (UL_EVP_NVAL == G_IO_NVAL)) {
    what |= cond & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_PRI |
                    UL_EVP_ERR | UL_EVP_HUP | UL_EVP_NVAL);
  } else {
    if(cond & G_IO_IN)
      what |= UL_EVP_IN;
    if(cond & G_IO_OUT)
      what |= UL_EVP_OUT;
    if(cond & G_IO_PRI)
      what |= UL_EVP_PRI;
    if(cond & G_IO_ERR)
      what |= UL_EVP_ERR;
    if(cond & G_IO_HUP)
      what |= UL_EVP_HUP;
    if(cond & G_IO_NVAL)
      what |= UL_EVP_NVAL;
  }

  if((ei->flags & UL_EVPGLIB_TIMEOUT) && what) {
    ei->expire = ul_evpglib_next_timeout(ei, &ei->timeout);
    ei->flags |= UL_EVPGLIB_TIMED;
  } else {
    if(ei->flags & UL_EVPGLIB_TIMED) {
      ul_htim_diff_t diff;
      diff = ei->expire - ul_evpglib_source_get_current_time(&ei->source);
      if(diff <= 0) {
        what |= UL_EVP_TIMEOUT;
        ei->flags &= ~UL_EVPGLIB_TIMED;
      }
    }
  }

  if(!what)
    return TRUE;

  if(ei->flags & UL_EVPGLIB_ONCE) {
    ul_evpglib_disarm_internal(ei);
  }

  if (!ei->trig_ptr->cb)
    return TRUE;

  ei->trig_ptr->cb(ei->trig_ptr, what);

  return TRUE;
}

static void ul_evpglib_source_finalize (GSource *source)
{
  ul_evptrig_glib_t *ei = UL_CONTAINEROF(source, ul_evptrig_glib_t, source);
  if(ei->trig_ptr) {
    if(ei->trig_ptr->cb)
      ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE when poll is being destroyed\n");
  }
  ei->trig_ptr = NULL;
}

static ul_evpbase_t *ul_evpglib_base_new(void)
{
  ul_evpbase_glib_t *eb;
  GMainContext *mcontext;

  eb = (ul_evpbase_glib_t*)malloc(sizeof(ul_evpbase_glib_t));
  if(!eb)
    return NULL;

  memset(eb,0,sizeof(ul_evpbase_glib_t));

  eb->base.ops = &ul_evpoll_ops_glib;

  ul_evpglib_trig_init_head(eb);

  mcontext = g_main_context_default();
  if(!mcontext)
    goto init_error;

  if(g_main_context_acquire(mcontext)) {
    eb->main_context = mcontext;
    eb->acquired_context = 1;
    g_main_context_ref(eb->main_context);
  } else {
    eb->main_context = g_main_context_new();
    if(!eb->main_context)
      goto init_error;
  }

  return &eb->base;

init_error:
  free(eb);
  return NULL;
}

static void ul_evpglib_base_destroy(ul_evpbase_t *base)
{
  ul_evptrig_glib_t *ei;
  ul_evpbase_glib_t *eb = UL_CONTAINEROF(base, ul_evpbase_glib_t, base);

  while((ei = ul_evpglib_trig_first(eb))!=NULL){
    ul_evpglib_trig_disarm(ei->trig_ptr);
    if(ei->trig_ptr->cb)
      ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE for %i when poll is being destroyed\n", ei->pollfd.fd);
    if(ei == ul_evpglib_trig_first(eb)) {
      ul_logdeb("UL_EVP_DONE not handled for %i when poll is being destroyed\n", ei->pollfd.fd);
      ul_evpglib_trig_done(ei->trig_ptr);
    }
  }

  if(eb->main_context != NULL) {
    if(eb->acquired_context)
      g_main_context_release(eb->main_context);
    g_main_context_unref(eb->main_context);
  }
  eb->main_context = NULL;

  free(eb);
}

static int ul_evpglib_base_update(ul_evpbase_t *base)
{
  return 0;
}

static int ul_evpglib_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_glib_t *eb = UL_CONTAINEROF(base, ul_evpbase_glib_t, base);
  GMainLoop    *mloop;

  eb->main_loop = g_main_loop_new(eb->main_context, TRUE);
  if(eb->main_loop == NULL)
    return UL_EVP_DISPRET_ERROR;

  g_main_loop_run(eb->main_loop);

  mloop = eb->main_loop;
  eb->main_loop = NULL;
  g_main_loop_unref(mloop);

  /*g_main_context_iteration(eb->main_context, TRUE);*/

  return UL_EVP_DISPRET_QUIT;
}

static int ul_evpglib_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_glib_t *eb = UL_CONTAINEROF(base, ul_evpbase_glib_t, base);

  switch(option) {
    case UL_EVP_OPTION_QUIT:
      if(val && (eb->main_loop != NULL))
        g_main_loop_quit(eb->main_loop);
      return 0;
   }
  return -1;
}

const ul_evpoll_ops_t ul_evpoll_ops_glib = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "glib",
  .trig_init = ul_evpglib_trig_init,
  .trig_done = ul_evpglib_trig_done,
  .trig_set_fd = ul_evpglib_trig_set_fd,
  .trig_set_time = ul_evpglib_trig_set_time,
  .trig_set_timeout = ul_evpglib_trig_set_timeout,
  .trig_set_callback = ul_evpglib_trig_set_callback,
  .trig_arm = ul_evpglib_trig_arm,
  .trig_disarm = ul_evpglib_trig_disarm,
  .trig_arm_once = ul_evpglib_trig_arm_once,
  .trig_set_param = ul_evpglib_trig_set_param,
  .trig_get_param = ul_evpglib_trig_get_param,
  .base_new = ul_evpglib_base_new,
  .base_destroy = ul_evpglib_base_destroy,
  .base_update = ul_evpglib_base_update,
  .base_dispatch = ul_evpglib_base_dispatch,
  .base_get_current_time = ul_evpglib_get_current_time,
  .base_set_option = ul_evpglib_base_set_option,
};
