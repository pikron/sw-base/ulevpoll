/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evp_lnxepoll.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVP_LNXEPOLL_H
#define _UL_EVP_LNXEPOLL_H

#include "ul_utdefs.h"

#include <string.h>

#include "ul_evpoll.h"
#include <ul_list.h>
#include <ul_htimer.h>
#include <ul_gavl.h>

#ifdef __cplusplus
extern "C" {
#endif

#define EVP_LNXEPOLL_ARMED    0x01
#define EVP_LNXEPOLL_ONCE     0x02
#define EVP_LNXEPOLL_TIMED    0x04
#define EVP_LNXEPOLL_TIMEOUT  0x08
#define EVP_LNXEPOLL_PERIODIC 0x10

/* 
 * This option allows to check validity of user data returned
 * by epoll system call => use of event.data.ptr could lead
 * to disaster if data structures are already freed
 */
//#define UL_EVP_LNXEPOLL_CHECKFD

/*
 * Provide cascade support
 */
#define UL_EVP_LNXEPOLL_CASCADE

/*
 * Use of delayd delete for fdnodes prevents above problem as well
 */
#define UL_EVP_LNXEPOLL_DELAYDEL

struct epoll_event;

typedef struct ul_evplnxepoll_fdnode_t {
  ul_list_head_t samefd_list;
  gavl_node_t    node;
  ul_evfd_t      fd;
  unsigned int   for_events;
 #ifdef UL_EVP_LNXEPOLL_DELAYDEL
  ul_list_node_t delaydel_node;
  unsigned int delaydel_generation;
 #endif
} ul_evplnxepoll_fdnode_t;

typedef struct ul_evptrig_data_t {
  unsigned flags;
  unsigned what_events;
  unsigned pending_events;

  ul_evplnxepoll_fdnode_t *fdnode;

  ul_htim_diff_t timeout;
  ul_htim_node_t htim;

  ul_list_node_t samefd_node;
  ul_list_node_t list_node;

  ul_evptrig_t *trig_ptr;

} ul_evptrig_lnxepoll_t;

typedef struct ul_evpbase_lnxepoll_t {
  ul_evpbase_t base;

  int epoll_fd;

  int time_next_expire_fl:1;
  int quit_loop:1;
  int cascade_armed_fl:1;
  int cascade_inerit_destroy_fl:1;
  int cascade_propagate_destroy_fl:1;
  int epoll_buff_auto_fl:1;

  unsigned int epoll_buff_full;
  unsigned int epoll_buff_capacity;
  struct epoll_event *epoll_buff;

  ul_list_head_t trig_list;
  ul_list_head_t report_list;
  gavl_cust_root_field_t fd_root;

  ul_htim_time_t time_act;
  ul_htim_time_t time_next_expire;
  ul_htim_queue_t htim_queue;
 #ifdef UL_EVP_LNXEPOLL_DELAYDEL
  ul_list_head_t delaydel_list;
  unsigned int delaydel_generation;
 #endif
 #ifdef UL_EVP_LNXEPOLL_CASCADE
  ul_evptrig_t   cascade_trig;
 #endif /*UL_EVP_LNXEPOLL_CASCADE*/
} ul_evpbase_lnxepoll_t;


UL_LIST_CUST_DEC(ul_evplnxepoll_trig, ul_evpbase_lnxepoll_t, ul_evptrig_lnxepoll_t,
                trig_list, list_node)

UL_LIST_CUST_DEC(ul_evplnxepoll_report, ul_evpbase_lnxepoll_t, ul_evptrig_lnxepoll_t,
                report_list, list_node)

UL_LIST_CUST_DEC(ul_evplnxepoll_fdnode_trig, ul_evplnxepoll_fdnode_t, ul_evptrig_lnxepoll_t,
                samefd_list, samefd_node)

#ifdef UL_EVP_LNXEPOLL_DELAYDEL
UL_LIST_CUST_DEC(ul_evplnxepoll_delaydel, ul_evpbase_lnxepoll_t, ul_evplnxepoll_fdnode_t,
                delaydel_list, delaydel_node)
#endif

extern const ul_evpoll_ops_t ul_evpoll_ops_lnxepoll;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVP_LNXEPOLL_H */
