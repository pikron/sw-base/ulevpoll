/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpollint.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVPOLLINT_H
#define _UL_EVPOLLINT_H

#include "ul_utdefs.h"

#include <string.h>

#include "ul_evpoll.h"
#include <ul_list.h>
#include <ul_htimer.h>

#include <sys/types.h>
#include <event.h>

#ifdef __cplusplus
extern "C" {
#endif

#define UL_EVPLIBEVENT1_ARMED    0x01
#define UL_EVPLIBEVENT1_TIMED    0x04
#define UL_EVPLIBEVENT1_TIMEOUT  0x08
#define UL_EVPLIBEVENT1_PERIODIC 0x10

typedef struct ul_evptrig_data_t {
  struct event  lev_event;

  unsigned flags;

  union {
    ul_htim_time_t expire;
    ul_htim_diff_t timeout;
  } timing;

  ul_list_node_t list_node;

} ul_evptrig_libevent1_t;

typedef struct ul_evpbase_libevent1_t {
  ul_evpbase_t base;
  struct event_base *lev_base;
  ul_list_head_t trig_list;
} ul_evpbase_libevent1_t;

UL_LIST_CUST_DEC(ul_evplibevent1_trig, ul_evpbase_libevent1_t, ul_evptrig_libevent1_t,
                trig_list, list_node)

extern const ul_evpoll_ops_t ul_evpoll_ops_libevent1;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVPOLLINT_H */
