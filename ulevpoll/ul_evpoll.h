/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpoll.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVPOLL_H
#define _UL_EVPOLL_H

#include "ul_utdefs.h"

#include <string.h>
#include <stdint.h>

#include "ul_htimdefs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define UL_EVP_OPS_VERSION   1

#define UL_EVP_IN	0x0001
#define UL_EVP_PRI	0x0002
#define UL_EVP_OUT	0x0004
#define UL_EVP_ERR	0x0008
#define UL_EVP_HUP	0x0010
#define UL_EVP_NVAL	0x0020
#define UL_EVP_TIMEOUT	0x0040
#define UL_EVP_SIGNAL	0x0080
#define UL_EVP_DONE	0x0100

#define UL_EVP_READ	UL_EVP_IN
#define UL_EVP_WRITE	UL_EVP_OUT
#define UL_EVP_STATE	(UL_EVP_PRI|UL_EVP_ERR|UL_EVP_HUP|UL_EVP_NVAL)

#define UL_EVPTRIG_PARAM_WIN32EVENT 100

#define UL_EVP_OPTION_QUIT      1

#define UL_EVP_DISPRET_ERROR   -1
#define UL_EVP_DISPRET_QUIT    -2
#define UL_EVP_DISPRET_NOEVENT  0
#define UL_EVP_DISPRET_HANDLED  1
#define UL_EVP_DISPRET_TIMEOUT  2

#define UL_EVP_CASFL_INHERIT_DESTROY   1
#define UL_EVP_CASFL_PROPAGATE_DESTROY 2

#ifdef _WIN64
typedef intptr_t ul_evfd_t;
#else
typedef int ul_evfd_t;
#endif

struct ul_evptrig_data_t;
typedef struct ul_evpbase_t ul_evpbase_t;
typedef struct ul_evptrig_t ul_evptrig_t;

typedef void (*ul_evpoll_cb_t)(ul_evptrig_t *evptrig, int what);

/**
 * struct ul_evptrig_t - event trigger public structure
 * @impl_data:	pointer to implementation data set by base in ul_evptrig_init()
 * @base:	pointer to the base which event is member of
 * @cb:		pointer to user callback function
 *
 * The event trigger is basic element of whole library. One or more instances
 * of &ul_evptrig_t structure are typically contained by some user
 * data structure holding state for given communication object.
 * Structure is initiated and assigned to selected event poll base by
 * ul_evptrig_init(). From this point it is associated to base until
 * ul_evptrig_done() is called. If poll base is destroyed / ul_evpoll_destroy()
 * called before ul_evptrig_done(), %UL_EVP_DONE event is delivered to the
 * assigned callback function. It should call ul_evptrig_done() in such case.
 *
 * The trigger is setup to accept selected events, functions ul_evptrig_set_fd(),
 * ul_evptrig_set_callback(), ul_evptrig_set_time()/ul_evptrig_set_timeout()
 * and then it is marked active by ul_evptrig_arm() or ul_evptrig_arm_once()
 * call. When event occurs, the callback @cb is activated with set of active
 * events. The argument of @evptrig is pointer to corresponding &ul_evptrig_t
 * structure. Use of %UL_CONTAINEROF is expected to obtain pointer
 * communication object data structure containing activated event trigger.
 * Monitoring of given event can be (temporarily) disabled by
 * ul_evptrig_disarm().
 */
struct ul_evptrig_t {
  struct ul_evptrig_data_t *impl_data;
  ul_evpbase_t *base;
  ul_evpoll_cb_t   cb;
};

typedef struct ul_evpoll_ops_t {
  uintptr_t ops_version;
  uintptr_t ops_size;
  const char *name;
  int (*trig_init)(ul_evpbase_t *base, ul_evptrig_t *evptrig);
  void (*trig_done)(ul_evptrig_t *evptrig);
  int (*trig_set_fd)(ul_evptrig_t *evptrig, ul_evfd_t fd, int what);
  int (*trig_set_time)(ul_evptrig_t *evptrig, ul_htim_time_t *time);
  int (*trig_set_timeout)(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout);
  int (*trig_set_callback)(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb);
  int (*trig_arm)(ul_evptrig_t *evptrig);
  int (*trig_disarm)(ul_evptrig_t *evptrig);
  int (*trig_arm_once)(ul_evptrig_t *evptrig);
  int (*trig_set_param)(ul_evptrig_t *evptrig, int parnum, const void *parval, int parsize);
  int (*trig_get_param)(ul_evptrig_t *evptrig, int parnum, void *parval, int parmaxsize);

  ul_evpbase_t *(*base_new)(void);
  void (*base_destroy)(ul_evpbase_t *base);
  int (*base_update)(ul_evpbase_t *base);
  int (*base_dispatch)(ul_evpbase_t *base, ul_htim_diff_t *timeout);
  ul_htim_time_t (*base_get_current_time)(ul_evpbase_t *base);
  int (*base_set_option)(ul_evpbase_t *base, int option, int val);
  int (*base_cascade)(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags);
} ul_evpoll_ops_t;

/**
 * struct ul_evpbase_t - common part of event poll base structure
 * @ops:	pointer set of operations provided by this base
 *
 * There is typically one such base for each thread which needs
 * to process events. The poll base is created by ul_evpoll_new()
 * call. ul_evpoll_destroy() informs all attached triggers (%UL_EVP_DONE),
 * about base cease, ensures, that implementation specific data
 * are released even for triggers, which do not call ul_evptrig_done() /
 * handle %UL_EVP_DONE, closes and deallocates base.
 *
 * The call ul_evpoll_dispatch() starts single iteration waiting
 * for events. If there is no need to implement own loop in application
 * the ul_evpoll_loop() can be called to handle all events for given thread.
 * The loop is terminated when call ul_evpoll_quilt_loop() is used
 * during iteration.
 */ 
struct ul_evpbase_t {
  const ul_evpoll_ops_t *ops;
};

extern ul_evpbase_t *ul_evpoll_base_implicit;

extern const ul_evpoll_ops_t *ul_evpoll_ops_implicit;
extern const ul_evpoll_ops_t *ul_evpoll_ops_compile_default;

/**
 * ul_evptrig_preinit_detached - mark trigger structure as not initialized yet
 * @evptrig:	event trigger
 *
 * This call allows user application to initialize communication object data
 * structure and later check, if given trigger is already initialized
 * or not. Only valid operation for uninitialized trigger is
 * ul_evptrig_is_detached()
 */
static inline void ul_evptrig_preinit_detached(ul_evptrig_t *evptrig)
{
  evptrig->impl_data = NULL;
  evptrig->base = NULL;
}

/**
 * ul_evptrig_is_detached - test if trigger is not initialized/attached to base
 * @evptrig:	event trigger
 */
static inline int ul_evptrig_is_detached(ul_evptrig_t *evptrig)
{
  return evptrig->impl_data == NULL;
}

/**
 * ul_evptrig_init - initialization of event trigger structure
 * @evptrig:	event trigger
 * @base:	event poll base
 *
 * If the @base parameter is %NULL, default base is found/created
 * and event trigger is attached to that default base.
 * Base allocates required event rigger implementation data
 * and fills @impl_data pointer.
 */
int ul_evptrig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig);

/**
 * ul_evptrig_done - detach and done event trigger
 * @evptrig:	event trigger
 *
 * Operation can be called only to previously initialized trigger
 */
static inline void ul_evptrig_done(ul_evptrig_t *evptrig)
{
  evptrig->base->ops->trig_done(evptrig);
}

/**
 * ul_evptrig_set_fd - set file descriptor monitored for specified events
 * @evptrig:	event trigger
 * @fd:		file descriptor
 * @what:	which events to monitor - set of
 *               %UL_EVP_READ, %UL_EVP_WRITE %UL_EVP_STATE
 */
static inline int ul_evptrig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  return evptrig->base->ops->trig_set_fd(evptrig, fd, what);
}

/**
 * ul_evptrig_set_time - set absolute time to trigger event
 * @evptrig:	event trigger
 * @time:	pointer to absolute time specification to trigger event
 *
 * The call back is activated with %UL_EVP_TIMEOUT set for armed event
 * when time elapses. The time can be changed even for armed
 * event trigger freely and is set to never if @time is %NULL
 */
static inline int ul_evptrig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  return evptrig->base->ops->trig_set_time(evptrig, time);
}

/**
 * ul_evptrig_set_timeout - inactivity timeout for trigger event
 * @evptrig:	event trigger
 * @timeout:	pointer relative time inactivity interval triggering event
 *
 * The call back is activated with %UL_EVP_TIMEOUT if there is no activity
 * on given trigger for given time interval. The timeout value
 * and start time can be re-trigger by call to %ul_evptrig_set_timeout
 * even for armed event. The disarm and arm sequence re-triggers timeout
 * interval start as well. @timeout equal to %NULL disables timeout monitoring.
 */
static inline int ul_evptrig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  return evptrig->base->ops->trig_set_timeout(evptrig, timeout);
}

/**
 * ul_evptrig_set_callback - set user calback function for given event trigger
 * @evptrig:	event trigger
 * @cb:		callback function
 */
static inline int ul_evptrig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  return evptrig->base->ops->trig_set_callback(evptrig, cb);
}

/**
 * ul_evptrig_arm - activates trigger to monitor for selected events
 * @evptrig:	event trigger
 */
static inline int ul_evptrig_arm(ul_evptrig_t *evptrig)
{
  return evptrig->base->ops->trig_arm(evptrig);
}

/**
 * ul_evptrig_disarm - stop monitoring of events by this trigger
 * @evptrig:	event trigger
 */
static inline int ul_evptrig_disarm(ul_evptrig_t *evptrig)
{
  return evptrig->base->ops->trig_disarm(evptrig);
}

/**
 * ul_evptrig_arm_once - activates trigger to wait for first of events only
 * @evptrig:	event trigger
 */
static inline int ul_evptrig_arm_once(ul_evptrig_t *evptrig)
{
  return evptrig->base->ops->trig_arm_once(evptrig);
}

/**
 * ul_evptrig_set_param - set extended/system specific parameter
 * @evptrig:	event trigger
 * @parnum:	parameter number %UL_EVPTRIG_PARAM_xxx
 * @parval:	pointer to value to be set
 * @parsize:	the size of the parameter 
 */
static inline int ul_evptrig_set_param(ul_evptrig_t *evptrig, int parnum,
                                       const void *parval, int parsize)
{
  return evptrig->base->ops->trig_set_param(evptrig, parnum, parval, parsize);
}

/**
 * ul_evptrig_get_param - get extended/system specific parameter
 * @evptrig:	event trigger
 * @parnum:	parameter number %UL_EVPTRIG_PARAM_xxx
 * @parval:	pointer to buffer to store value
 * @parsize:	size of buffer to hold returned value
 */
static inline int ul_evptrig_get_param(ul_evptrig_t *evptrig, int parnum,
                                       void *parval, int parmaxsize)
{
  return evptrig->base->ops->trig_get_param(evptrig, parnum, parval, parmaxsize);
}

/**
 * ul_evptrig_get_base - get pointer to poll base trigger is member of
 * @evptrig:	event trigger
 */
static inline ul_evpbase_t *ul_evptrig_get_base(ul_evptrig_t *evptrig)
{
  return evptrig->base;
}

int ul_evptrig_set_and_arm_fd(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb, ul_evfd_t fd,
                            int what, ul_htim_diff_t *timeout, int once);

/**
 * ul_evpoll_new - create new event base
 * @ops:	pointer to preferred mechanism/operations set
 * @flags:	set of option flags
 */
ul_evpbase_t *ul_evpoll_new(const ul_evpoll_ops_t *ops, int flags);

/**
 * ul_evpoll_destroy - destroy base and inform all attached triggers
 * @base:	event poll base
 */
void ul_evpoll_destroy(ul_evpbase_t *base);

/**
 * ul_evpoll_update - mostly reserve for some mechanisms requiring update calls
 * @base:	event poll base
 */
int ul_evpoll_update(ul_evpbase_t *base);

/**
 * ul_evpoll_update - start single iteration of the wait and process events cycle
 * @base:	event poll base
 * @timeout:	pointer relative time maximal wait interval or %NULL - forever
 */
int ul_evpoll_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout);

/**
 * ul_evpoll_get_current_time - get current time in given base epoch and units
 * @base:	event poll base
 */
static inline ul_htim_time_t ul_evpoll_get_current_time(ul_evpbase_t *base)
{
  return base->ops->base_get_current_time(base);
}

int ul_evpoll_set_option(ul_evpbase_t *base, int option, int val);

/**
 * ul_evpoll_loop - run event loop as long as required
 * @base:	event poll base
 * @flags:	none defined yet, provide 0
 */
int ul_evpoll_loop(ul_evpbase_t *base, int flags);

/**
 * ul_evpoll_quilt_loop - mark event loop to terminate before next iteration
 * @base:	event poll base
 */
int ul_evpoll_quilt_loop(ul_evpbase_t *base);

/**
 * ul_evpoll_cascade - cascade base or event triggers attached to it onto another base
 * @base:	event poll base which should be part of event processing of @new_base
 * @new_base:	upper level base which will include @base if operation succeed
 * @prioshift:	possible priority shift - not implemented yet
 * @bc_flags:	combination of %UL_EVP_CASFL_INHERIT_DESTROY,
 *                %UL_EVP_CASFL_PROPAGATE_DESTROY
 */
int ul_evpoll_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags);

ul_evpbase_t *ul_evpoll_chose_implicit_base(void);

int ul_evpoll_setup_root_htimer(ul_evpbase_t *base, int options);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVPOLL_H */
