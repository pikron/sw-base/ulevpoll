/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evp_lnxepoll.c	- monitoring of open file handles

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#include <sys/epoll.h>
#include <unistd.h>
#include <errno.h>

#include "ul_evpoll.h"
#include "ul_evp_lnxepoll.h"

#include <ul_gavl.h>
#include <ul_gavlcust.h>

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

UL_HTIMER_DEC_SCOPE(UL_ATTR_UNUSED static,
	      ul_evplnxepoll_htimer, ul_evpbase_lnxepoll_t, ul_evptrig_lnxepoll_t, \
	      htim_queue, htim)

UL_HTIMER_IMP(ul_evplnxepoll_htimer, ul_evpbase_lnxepoll_t, ul_evptrig_lnxepoll_t, \
	      htim_queue, htim)

static inline int
ul_evplnxepoll_fd_cmp_fnc(const ul_evfd_t *a, const ul_evfd_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GAVL_CUST_NODE_INT_DEC_SCOPE(UL_ATTR_UNUSED static,
	ul_evplnxepoll_fd, ul_evpbase_lnxepoll_t, ul_evplnxepoll_fdnode_t, ul_evfd_t,
	fd_root, node, fd, ul_evplnxepoll_fd_cmp_fnc)

GAVL_CUST_NODE_INT_IMP(ul_evplnxepoll_fd, ul_evpbase_lnxepoll_t, ul_evplnxepoll_fdnode_t, ul_evfd_t,
	fd_root, node, fd, ul_evplnxepoll_fd_cmp_fnc)

#ifdef UL_HTIMER_WITH_MSTIME

ul_htim_time_t ul_evplnxepoll_get_current_time(ul_evpbase_t *base)
{
  ul_mstime_t mstime;
  ul_htim_time_t htime;

  ul_mstime_now(&mstime);
  ul_mstime2htime(&htime, &mstime);

  return htime;
}

#endif /*UL_HTIMER_WITH_MSTIME*/

static inline ul_htim_time_t ul_evplnxepoll_next_timeout(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_htim_time_t htime = ul_evpoll_get_current_time(base);
  ul_htime_add(&htime, &htime, timeout);
  return htime;
}

static inline int ul_evplnxepoll_is_fd(ul_evptrig_lnxepoll_t *ei)
{
  return (ei->fdnode != NULL);
}

static void ul_evplnxepoll_fdnode_sync(ul_evpbase_lnxepoll_t *eb,
                                 ul_evplnxepoll_fdnode_t *efd)
{
  struct epoll_event event;
  unsigned what = 0;

  ul_evptrig_lnxepoll_t *ei;
  ul_list_for_each(ul_evplnxepoll_fdnode_trig, efd, ei) {
    if(ei->flags & EVP_LNXEPOLL_ARMED)
      what |= ei->what_events;
  }

  memset(&event, 0, sizeof(event));
  event.events = 0;
 #ifndef UL_EVP_LNXEPOLL_CHECKFD
  event.data.ptr = efd;
 #else /*UL_EVP_LNXEPOLL_CHECKFD*/
  event.data.fd = efd->fd;
 #endif /*UL_EVP_LNXEPOLL_CHECKFD*/

  if((UL_EVP_IN == EPOLLIN) && (UL_EVP_OUT == EPOLLOUT) &&
     (UL_EVP_PRI == EPOLLPRI) && (UL_EVP_ERR == EPOLLERR) &&
     (UL_EVP_HUP == EPOLLHUP)) {
    event.events |= what & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_PRI |
                            UL_EVP_ERR | UL_EVP_HUP);
  } else {
    if(what & UL_EVP_IN)
      event.events |= EPOLLIN;
    if(what & UL_EVP_OUT)
      event.events |= EPOLLOUT;
    if(what & UL_EVP_PRI)
      event.events |= EPOLLPRI;
    if(what & UL_EVP_ERR)
      event.events |= EPOLLERR;
    if(what & UL_EVP_HUP)
      event.events |= EPOLLHUP;
  }

  if(event.events != efd->for_events) {
    int ret;
    int op = !efd->for_events? EPOLL_CTL_ADD: !event.events? EPOLL_CTL_DEL: EPOLL_CTL_MOD;

    ret = epoll_ctl(eb->epoll_fd, op, efd->fd, &event);
    if(ret >= 0) {
      efd->for_events = event.events;
      ul_logdeb("ul_evplnxepoll_fdnode_sync: epoll_ctl fd %d op %s OK\n",
                 efd->fd,
                 op==EPOLL_CTL_ADD? "EPOLL_CTL_ADD":
                 op==EPOLL_CTL_DEL? "EPOLL_CTL_DEL": "EPOLL_CTL_MOD");
    } else {
      ul_logerr("ul_evplnxepoll_fdnode_sync: epoll_ctl failed for fd %d op %s err %s\n",
                 efd->fd,
                 op==EPOLL_CTL_ADD? "EPOLL_CTL_ADD":
                 op==EPOLL_CTL_DEL? "EPOLL_CTL_DEL": "EPOLL_CTL_MOD",
                 strerror(errno));
    }
  }
}

static inline void ul_evplnxepoll_arm_internal(ul_evpbase_lnxepoll_t *eb,
                                 ul_evptrig_lnxepoll_t *ei)
{
  if(ei->flags & EVP_LNXEPOLL_ARMED) {
    ul_logerr("ul_evplnxepoll_arm_internal: tring to arm armed evptrig\n");
    return;
  }

  ei->pending_events = 0;

  ei->flags |= EVP_LNXEPOLL_ARMED;

  if(ul_evplnxepoll_is_fd(ei))
    ul_evplnxepoll_fdnode_sync(eb, ei->fdnode);
}

static inline void ul_evplnxepoll_disarm_internal(ul_evpbase_lnxepoll_t *eb,
                                 ul_evptrig_lnxepoll_t *ei)
{
  if(!(ei->flags & EVP_LNXEPOLL_ARMED)) {
    ul_logerr("ul_evplnxepoll_arm_internal: tring to disarm idle evptrig\n");
    return;
  }

  ei->flags &= ~EVP_LNXEPOLL_ARMED;

  if(ul_evplnxepoll_is_fd(ei))
    ul_evplnxepoll_fdnode_sync(eb, ei->fdnode);
}


static void ul_evplnxepoll_trig_fd_unset(ul_evptrig_t *evptrig)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);

  if(ei->fdnode) {
    ul_evplnxepoll_fdnode_t *efd = ei->fdnode;
    ul_evplnxepoll_fdnode_trig_delete(efd, ei);
    ei->fdnode = NULL;
    if(ul_evplnxepoll_fdnode_trig_is_empty(efd)) {
     #ifndef UL_EVP_LNXEPOLL_DELAYDEL
      ul_evplnxepoll_fd_delete(eb, efd);
      free(efd);
     #else /*UL_EVP_LNXEPOLL_DELAYDEL*/
      ul_evplnxepoll_delaydel_del_item(efd);
      efd->delaydel_generation = eb->delaydel_generation;
      ul_evplnxepoll_delaydel_ins_tail(eb, efd);
     #endif /*UL_EVP_LNXEPOLL_DELAYDEL*/
    }
  }
}

static int ul_evplnxepoll_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evptrig_lnxepoll_t *ei;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);

  evptrig->cb = NULL;
  evptrig->base = base;
  evptrig->impl_data = (ul_evptrig_lnxepoll_t*)malloc(sizeof(ul_evptrig_lnxepoll_t));
  if(!evptrig->impl_data) {
    ul_logerr("ul_evplnxepoll_trig_init: malloc failed\n");
    return -1;
  }
  ei = evptrig->impl_data;
  memset(ei, 0, sizeof(ul_evptrig_lnxepoll_t));
  ei->fdnode = NULL;

  ul_evplnxepoll_htimer_init_detached(ei);

  ei->trig_ptr = evptrig;

  ul_evplnxepoll_trig_insert(eb, ei);

  return 0;
}

static void ul_evplnxepoll_trig_done(ul_evptrig_t *evptrig)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;

  if(!ei)
    return;

  if(ei->flags & EVP_LNXEPOLL_ARMED)
    ul_evptrig_disarm(evptrig);

  ul_evplnxepoll_trig_fd_unset(evptrig);

  ul_evplnxepoll_trig_del_item(ei);

  free(ei);

  evptrig->impl_data = NULL;
}

static int ul_evplnxepoll_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int armed_fl = ei->flags & EVP_LNXEPOLL_ARMED;
  ul_evplnxepoll_fdnode_t *efd;

  if(armed_fl)
    ul_evplnxepoll_disarm_internal(eb, ei);

  if(ei->fdnode)
    if(ei->fdnode->fd != fd)
      ul_evplnxepoll_trig_fd_unset(evptrig);

  ei->what_events = what;
  if((fd>=0) && (!ei->fdnode)) {
    efd = ul_evplnxepoll_fd_find(eb, &fd);
    if(efd == NULL) {
      efd = malloc(sizeof(ul_evplnxepoll_fdnode_t));
      if(efd == NULL) {
        ul_logerr("malloc failed for epoll fdnode\n");
        return -1;
      }
      memset(efd, 0, sizeof(ul_evplnxepoll_fdnode_t));
      ul_evplnxepoll_fdnode_trig_init_head(efd);
     #ifdef UL_EVP_LNXEPOLL_DELAYDEL
      ul_evplnxepoll_delaydel_init_detached(efd);
     #endif
      efd->fd = fd;
      ul_evplnxepoll_fd_insert(eb, efd);
    }
    ei->fdnode = efd;
    ul_evplnxepoll_fdnode_trig_insert(efd, ei);
  }

  if(armed_fl)
    ul_evplnxepoll_arm_internal(eb, ei);
  return 0;
}

static int ul_evplnxepoll_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);

  if((ei->flags & EVP_LNXEPOLL_ARMED) &&
     (ei->flags & EVP_LNXEPOLL_TIMED))
    ul_evplnxepoll_htimer_detach(eb, ei);

  ei->flags &= ~(EVP_LNXEPOLL_TIMED | EVP_LNXEPOLL_TIMEOUT | EVP_LNXEPOLL_PERIODIC);

  if(!time)
    return 0;

  ul_evplnxepoll_htimer_set_expire(ei, *time);

  ei->flags |= EVP_LNXEPOLL_TIMED;

  if(ei->flags & EVP_LNXEPOLL_ARMED)
    ul_evplnxepoll_htimer_add(eb, ei);

  return 0;
}

static int ul_evplnxepoll_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);

  if((ei->flags & EVP_LNXEPOLL_ARMED) &&
     (ei->flags & EVP_LNXEPOLL_TIMED))
    ul_evplnxepoll_htimer_detach(eb, ei);

  ei->flags &= ~(EVP_LNXEPOLL_TIMED | EVP_LNXEPOLL_TIMEOUT | EVP_LNXEPOLL_PERIODIC);

  if(!timeout)
    return 0;

  ei->timeout = *timeout;

  ei->flags |= EVP_LNXEPOLL_TIMED | EVP_LNXEPOLL_TIMEOUT;

  if(ei->flags & EVP_LNXEPOLL_ARMED) {
    ul_evplnxepoll_htimer_set_expire(ei, ul_evplnxepoll_next_timeout(&eb->base, &ei->timeout));
    ul_evplnxepoll_htimer_add(eb, ei);
  }

  return 0;
}

static int ul_evplnxepoll_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  evptrig->cb = cb;
  return 0;
}

static int ul_evplnxepoll_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);

  if(ei->flags & EVP_LNXEPOLL_ARMED)
    return 0;

  if(ei->flags & EVP_LNXEPOLL_TIMEOUT)
    ul_evplnxepoll_htimer_set_expire(ei, ul_evplnxepoll_next_timeout(&eb->base, &ei->timeout));

  if(ei->flags & EVP_LNXEPOLL_TIMED) {
    ul_evplnxepoll_htimer_add(eb, ei);
  }

  ei->flags &= ~EVP_LNXEPOLL_ONCE;
  ul_evplnxepoll_arm_internal(eb, ei);

  return 0;
}

static int ul_evplnxepoll_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);

  if(!(ei->flags & EVP_LNXEPOLL_ARMED))
    return 0;

  if(ei->flags & EVP_LNXEPOLL_TIMED)
    ul_evplnxepoll_htimer_detach(eb, ei);

  ul_evplnxepoll_disarm_internal(eb, ei);

  return 0;
}

static int ul_evplnxepoll_trig_arm_once(ul_evptrig_t *evptrig)
{
  int ret = 0;
  ul_evptrig_lnxepoll_t *ei = evptrig->impl_data;
  if(!(ei->flags & EVP_LNXEPOLL_ARMED))
    ret = ul_evplnxepoll_trig_arm(evptrig);
  ei->flags |= EVP_LNXEPOLL_ONCE;
  return ret;
}

static int ul_evplnxepoll_trig_set_param(ul_evptrig_t *evptrig,
                           int parnum, const void *parval, int parsize)
{
  return -1;
}

static int ul_evplnxepoll_trig_get_param(ul_evptrig_t *evptrig,
                           int parnum, void *parval, int parmaxsize)
{
  return -1;
}

static int ul_evplnxepoll_buff_capacity_set_internal(ul_evpbase_lnxepoll_t *eb, int new_capacity)
{
  struct epoll_event *buff;
  buff = malloc(new_capacity * sizeof(eb->epoll_buff[0]));
  if(buff == NULL)
    return -1;
  eb->epoll_buff_full = 0;
  eb->epoll_buff_capacity = new_capacity;
  if(eb->epoll_buff)
    free(eb->epoll_buff);
  eb->epoll_buff = buff;
  return 0;
}

static ul_evpbase_t *ul_evplnxepoll_base_new(void)
{
  ul_evpbase_lnxepoll_t *eb;

  eb = (ul_evpbase_lnxepoll_t*)malloc(sizeof(ul_evpbase_lnxepoll_t));
  if(!eb)
    return NULL;

  memset(eb,0,sizeof(ul_evpbase_lnxepoll_t));

  eb->epoll_buff_auto_fl = 1;
  eb->epoll_buff_full = 0;
  eb->epoll_buff_capacity = 16;
  eb->epoll_buff = malloc(eb->epoll_buff_capacity * sizeof(eb->epoll_buff[0]));
  if(eb->epoll_buff == NULL)
    return NULL;

  eb->epoll_fd = epoll_create(eb->epoll_buff_capacity);
  if(eb->epoll_fd < 0) {
    ul_logerr("epoll system call failed\n");
    return NULL;
  }

  ul_evplnxepoll_htimer_init_queue(eb);

  ul_evplnxepoll_trig_init_head(eb);
  ul_evplnxepoll_report_init_head(eb);
 #ifdef UL_EVP_LNXEPOLL_DELAYDEL
  ul_evplnxepoll_delaydel_init_head(eb);
 #endif /*UL_EVP_LNXEPOLL_DELAYDEL*/
 #ifdef UL_EVP_LNXEPOLL_CASCADE
  ul_evptrig_preinit_detached(&eb->cascade_trig);
 #endif /*UL_EVP_LNXEPOLL_CASCADE*/

  ul_evplnxepoll_fd_init_root_field(eb);

  eb->base.ops = &ul_evpoll_ops_lnxepoll;

  ul_logdeb("ul_evplnxepoll_base_new: new epoll fd %ld created\n",
            (long)eb->epoll_fd);

  return &eb->base;
}

static void ul_evplnxepoll_base_destroy(ul_evpbase_t *base)
{
  ul_evptrig_lnxepoll_t *ei;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);

  ul_list_for_each_cut(ul_evplnxepoll_report, eb, ei) {
    ul_evplnxepoll_trig_insert(eb, ei);
  }

  while((ei = ul_evplnxepoll_trig_first(eb))!=NULL){
    if(ei->trig_ptr->cb)
      ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE for %i when poll is being destroyed\n", ei->fdnode?ei->fdnode->fd:-1);
    if(ei == ul_evplnxepoll_trig_first(eb)) {
      ul_logdeb("UL_EVP_DONE not handled for %i when poll is being destroyed\n", ei->fdnode?ei->fdnode->fd:-1);
      ul_evplnxepoll_trig_done(ei->trig_ptr);
    }
  }

 #ifdef UL_EVP_LNXEPOLL_DELAYDEL
  {
    ul_evplnxepoll_fdnode_t *efd;
    ul_list_for_each_cut(ul_evplnxepoll_delaydel, eb, efd) {
      if(ul_evplnxepoll_fdnode_trig_is_empty(efd)) {
        ul_evplnxepoll_fd_delete(eb, efd);
        free(efd);
      }
    }
  }
 #endif /*UL_EVP_LNXEPOLL_DELAYDEL*/

  if(!ul_evplnxepoll_fd_is_empty(eb)) {
    ul_logerr("ul_evplnxepoll_base_destroy: not all fdnode destroyed\n");
  }

 #ifdef UL_EVP_LNXEPOLL_CASCADE
  if(!ul_evptrig_is_detached(&eb->cascade_trig)) {
    ul_evptrig_disarm(&eb->cascade_trig);
    ul_evptrig_done(&eb->cascade_trig);
  }
 #endif /*UL_EVP_LNXEPOLL_CASCADE*/

  close(eb->epoll_fd);

  ul_logdeb("ul_evplnxepoll_base_destroy: epoll fd %ld close OK, final epoll buffer capacity %d\n",
            (long)eb->epoll_fd, eb->epoll_buff_capacity);

  if(eb->epoll_buff != NULL)
    free(eb->epoll_buff);
  eb->epoll_buff = NULL;

  free(eb);
}

static int ul_evplnxepoll_base_update(ul_evpbase_t *base)
{
  return 0;
}

#define UL_EVPLNXEPOLL_BUFF_CAPACITY_MAX 128

static int ul_evplnxepoll_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);
  ul_msdiff_t mstimeout = -1;
  int events_cnt=0;
  int ret;
  int i;

  if(eb->quit_loop)
    return UL_EVP_DISPRET_QUIT;

  if(timeout!=NULL) {
    ul_htimdiff2ms(&mstimeout, timeout);
    if(mstimeout < 0)
      mstimeout = 0;
  }

  if(ul_evplnxepoll_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evplnxepoll_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    ul_msdiff_t msdiff;
    eb->time_act = ul_evpoll_get_current_time(base);
    ul_htime_sub2ms(&msdiff, &eb->time_next_expire, &eb->time_act);
    if(msdiff < 0)
      msdiff = 0;
    if((mstimeout < 0) || (mstimeout > msdiff))
      mstimeout = msdiff;
  }

  ret = epoll_wait(eb->epoll_fd, eb->epoll_buff,
                   eb->epoll_buff_capacity, mstimeout);
  events_cnt = ret;

  if(ret < 0 ) {
    ret = UL_EVP_DISPRET_ERROR;
  } else if(ret == 0) {
    ret = UL_EVP_DISPRET_TIMEOUT;
  } else if(ret > 0) {
    ul_evptrig_lnxepoll_t *ei;
    ret = UL_EVP_DISPRET_HANDLED;
    /* An evptrig on one of the fds has occurred. */
    for(i=0; i<events_cnt; i++) {
      int revents = eb->epoll_buff[i].events;
      int what = 0;
      ul_evplnxepoll_fdnode_t *efd;
     #ifndef UL_EVP_LNXEPOLL_CHECKFD
      efd = (ul_evplnxepoll_fdnode_t*)eb->epoll_buff[i].data.ptr;
     #else /*UL_EVP_LNXEPOLL_CHECKFD*/
      efd = ul_evplnxepoll_fd_find(eb, &eb->epoll_buff[i].data.fd);
      if(efd == NULL) {
        ul_logerr("ul_evplnxepoll_base_dispatch: epoll_wait returns unregisterred fd %d\n",
                  eb->epoll_buff[i].data.fd);
        continue;
      }
     #endif /*UL_EVP_LNXEPOLL_CHECKFD*/
      if(!revents) continue;
      if((UL_EVP_IN == EPOLLIN) && (UL_EVP_OUT == EPOLLOUT) &&
         (UL_EVP_PRI == EPOLLPRI) && (UL_EVP_ERR == EPOLLERR) &&
         (UL_EVP_HUP == EPOLLHUP)) {
        what |= revents & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_PRI |
                                   UL_EVP_ERR | UL_EVP_HUP);
      } else {
        if(revents & EPOLLIN)   what |= UL_EVP_IN;
        if(revents & EPOLLOUT)  what |= UL_EVP_OUT;
        if(revents & EPOLLPRI)  what |= UL_EVP_PRI;
        if(revents & EPOLLERR)  what |= UL_EVP_ERR;
        if(revents & EPOLLHUP)  what |= UL_EVP_HUP;
      }
      ul_list_for_each(ul_evplnxepoll_fdnode_trig, efd, ei) {
        if(!(ei->what_events & what) || !(ei->flags & EVP_LNXEPOLL_ARMED))
          continue;
        ei->pending_events |= ei->what_events & what;
        ul_evplnxepoll_trig_del_item(ei);
        ul_evplnxepoll_report_ins_tail(eb,ei);
      }
    }
    ul_list_for_each_cut(ul_evplnxepoll_report, eb, ei) {
      unsigned pending = ei->pending_events;
      ul_evplnxepoll_trig_insert(eb, ei);
      if(!(ei->what_events & pending) ||
         !(ei->flags & EVP_LNXEPOLL_ARMED))
        continue;
      ei->pending_events &= ~ei->what_events;
      if(ei->flags & EVP_LNXEPOLL_ONCE)
        ul_evplnxepoll_trig_disarm(ei->trig_ptr);
      if(ei->flags & EVP_LNXEPOLL_TIMEOUT)
        ul_evplnxepoll_trig_set_timeout(ei->trig_ptr, &ei->timeout);
      if(ei->trig_ptr->cb) {
        ei->trig_ptr->cb(ei->trig_ptr, ei->what_events & pending);
      }
    }
  }

  if(events_cnt == eb->epoll_buff_capacity) {
    eb->epoll_buff_full++;
    if(eb->epoll_buff_auto_fl && (eb->epoll_buff_full >= 3) &&
       (eb->epoll_buff_capacity < UL_EVPLNXEPOLL_BUFF_CAPACITY_MAX)) {
      int new_capacity = 2 * eb->epoll_buff_capacity;
      if(new_capacity > UL_EVPLNXEPOLL_BUFF_CAPACITY_MAX)
        new_capacity = UL_EVPLNXEPOLL_BUFF_CAPACITY_MAX;
      if(ul_evplnxepoll_buff_capacity_set_internal(eb, new_capacity) < 0)
        eb->epoll_buff_auto_fl = 0;
    }
  }

 #ifdef UL_EVP_LNXEPOLL_DELAYDEL
  if(events_cnt < eb->epoll_buff_capacity) {
    ul_evplnxepoll_fdnode_t *efd;
    while((efd = ul_evplnxepoll_delaydel_first(eb)) != NULL) {
      if((int)(efd->delaydel_generation - eb->delaydel_generation) >= 0)
        break;
      ul_evplnxepoll_delaydel_del_item(efd);
      if(ul_evplnxepoll_fdnode_trig_is_empty(efd)) {
        ul_evplnxepoll_fd_delete(eb, efd);
        free(efd);
      }
    }
    eb->delaydel_generation++;
  }
 #endif /*UL_EVP_LNXEPOLL_DELAYDEL*/

  if(ul_evplnxepoll_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evplnxepoll_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    ul_evptrig_lnxepoll_t *ei;
    eb->time_act = ul_evpoll_get_current_time(base);

    while((ei=ul_evplnxepoll_htimer_cut_expired(eb, &eb->time_act))) {
      if(ei->trig_ptr->cb)
        ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_TIMEOUT);
      ret = UL_EVP_DISPRET_HANDLED;
    }

    if(ul_evplnxepoll_htimer_first_changed(eb))
      eb->time_next_expire_fl=ul_evplnxepoll_htimer_next_expire(eb,&eb->time_next_expire);
  }

  return ret;
}

static int ul_evplnxepoll_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);

  switch(option) {
    case UL_EVP_OPTION_QUIT:
      if(val)
        eb->quit_loop = 1;
      return 0;
   }
  return -1;
}

#ifdef UL_EVP_LNXEPOLL_CASCADE
static int ul_evplnxepoll_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags);
#endif /*UL_EVP_LNXEPOLL_CASCADE*/

const ul_evpoll_ops_t ul_evpoll_ops_lnxepoll = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "lnxepoll",
  .trig_init = ul_evplnxepoll_trig_init,
  .trig_done = ul_evplnxepoll_trig_done,
  .trig_set_fd = ul_evplnxepoll_trig_set_fd,
  .trig_set_time = ul_evplnxepoll_trig_set_time,
  .trig_set_timeout = ul_evplnxepoll_trig_set_timeout,
  .trig_set_callback = ul_evplnxepoll_trig_set_callback,
  .trig_arm = ul_evplnxepoll_trig_arm,
  .trig_disarm = ul_evplnxepoll_trig_disarm,
  .trig_arm_once = ul_evplnxepoll_trig_arm_once,
  .trig_set_param = ul_evplnxepoll_trig_set_param,
  .trig_get_param = ul_evplnxepoll_trig_get_param,
  .base_new = ul_evplnxepoll_base_new,
  .base_destroy = ul_evplnxepoll_base_destroy,
  .base_update = ul_evplnxepoll_base_update,
  .base_dispatch = ul_evplnxepoll_base_dispatch,
  .base_get_current_time = ul_evplnxepoll_get_current_time,
  .base_set_option = ul_evplnxepoll_base_set_option,
 #ifdef UL_EVP_LNXEPOLL_CASCADE
  .base_cascade = ul_evplnxepoll_base_cascade,
 #endif /*UL_EVP_LNXEPOLL_CASCADE*/
};

#ifdef UL_EVP_LNXEPOLL_CASCADE

static const ul_evpoll_ops_t ul_evpoll_ops_lnxepoll_cascade;

static int ul_evplnxepollcas_update(ul_evpbase_lnxepoll_t *eb, int update_exptime)
{
  int arm_fl;

  if(ul_evptrig_is_detached(&eb->cascade_trig))
    return 0;

  if(ul_evplnxepoll_htimer_first_changed(eb)) {
    eb->time_next_expire_fl=ul_evplnxepoll_htimer_next_expire(eb,&eb->time_next_expire);
    update_exptime = 1;
  }
  arm_fl = eb->time_next_expire_fl;

 #ifdef UL_EVP_LNXEPOLL_DELAYDEL
  if(!ul_evplnxepoll_delaydel_is_empty(eb)) {
    arm_fl = 1;
    update_exptime = 2;
  }
 #endif

  if(!ul_evplnxepoll_fd_is_empty(eb))
    arm_fl = 1;

  if(eb->cascade_armed_fl && !arm_fl) {
    ul_evptrig_disarm(&eb->cascade_trig);
    eb->cascade_armed_fl = 0;
  }

  if(update_exptime) {
    int exptime_fl = eb->time_next_expire_fl;
    ul_htim_time_t exptime = eb->time_next_expire;
    if(update_exptime == 2) {
      exptime_fl = 1;
      exptime = ul_evpoll_get_current_time(eb->cascade_trig.base);
    }
    ul_evptrig_set_time(&eb->cascade_trig, exptime_fl? &exptime: NULL);
  }

  if(!eb->cascade_armed_fl) {
    ul_evptrig_arm(&eb->cascade_trig);
    eb->cascade_armed_fl = 1;
  }

  return 0;
}

static void ul_evplnxepollcas_cb(ul_evptrig_t *evptrig, int what)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig, ul_evpbase_lnxepoll_t, cascade_trig);

  if(what == UL_EVP_DONE) {
    ul_evptrig_done(&eb->cascade_trig);
    eb->base.ops = &ul_evpoll_ops_lnxepoll;
    if(eb->cascade_inerit_destroy_fl)
      ul_evpoll_destroy(&eb->base);
    return;
  }

  if(what & (UL_EVP_TIMEOUT | UL_EVP_IN)) {
    int exptime_fl = eb->time_next_expire_fl;
    ul_htim_time_t exptime = eb->time_next_expire;
    ul_htim_diff_t timeout_0 = 0;
    int update_exptime;
    int ret;

    ret = ul_evplnxepoll_base_dispatch(&eb->base, &timeout_0);
    if(ret == UL_EVP_DISPRET_QUIT) {
      /* ul_evptrig_disarm(&eb->cascade_trig);
         eb->cascade_armed_fl = 0; */
      /* ul_evpoll_destroy(&eb->base); */
      return;
    }

    if(what & UL_EVP_TIMEOUT)
      exptime_fl = 0;

    update_exptime = (exptime_fl != eb->time_next_expire_fl) || (exptime != eb->time_next_expire) ||
                     (what & UL_EVP_TIMEOUT);
    ul_evplnxepollcas_update(eb, update_exptime);
  }
}

static int ul_evplnxepoll_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);
  if(!ul_evptrig_is_detached(&eb->cascade_trig)) {
    ul_logerr("ul_evplnxepoll_base_cascade: already cascaded\n");
    return -1;
  }

  if(ul_evptrig_init(new_base, &eb->cascade_trig) < 0)
    return -1;

  if(ul_evptrig_set_callback(&eb->cascade_trig, ul_evplnxepollcas_cb) < 0)
    goto cascade_error;

  if(ul_evptrig_set_fd(&eb->cascade_trig, eb->epoll_fd, UL_EVP_IN) < 0)
    goto cascade_error;

  eb->base.ops = &ul_evpoll_ops_lnxepoll_cascade;

  eb->cascade_inerit_destroy_fl = (bc_flags & UL_EVP_CASFL_INHERIT_DESTROY)? 1: 0;
  eb->cascade_propagate_destroy_fl = (bc_flags & UL_EVP_CASFL_PROPAGATE_DESTROY)? 1: 0;

  ul_evplnxepollcas_update(eb, 1);

  return 0;

cascade_error:
  ul_logerr("ul_evplnxepoll_base_cascade: cascade setup error\n");
  ul_evptrig_done(&eb->cascade_trig);
  return -1;
}

static void ul_evplnxepollcas_trig_done(ul_evptrig_t *evptrig)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  ul_evplnxepoll_trig_done(evptrig);
  ul_evplnxepollcas_update(eb, 0);
}

static int ul_evplnxepollcas_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int ret;
  ret = ul_evplnxepoll_trig_set_fd(evptrig, fd, what);
  ul_evplnxepollcas_update(eb, 0);
  return ret;
}

static int ul_evplnxepollcas_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int ret;
  ret = ul_evplnxepoll_trig_set_time(evptrig, time);
  ul_evplnxepollcas_update(eb, 0);
  return ret;
}

static int ul_evplnxepollcas_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int ret;
  ret = ul_evplnxepoll_trig_set_timeout(evptrig, timeout);
  ul_evplnxepollcas_update(eb, 0);
  return ret;
}

static int ul_evplnxepollcas_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int ret;
  ret = ul_evplnxepoll_trig_arm(evptrig);
  ul_evplnxepollcas_update(eb, 0);
  return ret;
}

static int ul_evplnxepollcas_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int ret;
  ret = ul_evplnxepoll_trig_disarm(evptrig);
  ul_evplnxepollcas_update(eb, 0);
  return ret;
}

static int ul_evplnxepollcas_trig_arm_once(ul_evptrig_t *evptrig)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_lnxepoll_t, base);
  int ret;
  ret = ul_evplnxepoll_trig_arm_once(evptrig);
  ul_evplnxepollcas_update(eb, 0);
  return ret;
}

static void ul_evplnxepollcas_base_destroy(ul_evpbase_t *base)
{
  ul_evpbase_t *cascade_base = NULL;
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);
  if(eb->cascade_propagate_destroy_fl)
    cascade_base = ul_evptrig_get_base(&eb->cascade_trig);
  ul_evptrig_disarm(&eb->cascade_trig);
  ul_evptrig_done(&eb->cascade_trig);
  ul_evplnxepoll_base_destroy(base);
  if(cascade_base != NULL)
    ul_evpoll_destroy(cascade_base);
}

static int ul_evplnxepollcas_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);
  return ul_evpoll_dispatch(eb->cascade_trig.base, timeout);
}

static ul_htim_time_t ul_evplnxepollcas_get_current_time(ul_evpbase_t *base)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);
  return ul_evpoll_get_current_time(eb->cascade_trig.base);
}

static int ul_evplnxepollcas_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_lnxepoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_lnxepoll_t, base);
  ul_evplnxepoll_base_set_option(base, option, val);
  return ul_evpoll_set_option(eb->cascade_trig.base, option, val);
}

static const ul_evpoll_ops_t ul_evpoll_ops_lnxepoll_cascade = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "lnxepoll_cascade",
  .trig_init = ul_evplnxepoll_trig_init,
  .trig_done = ul_evplnxepollcas_trig_done,
  .trig_set_fd = ul_evplnxepollcas_trig_set_fd,
  .trig_set_time = ul_evplnxepollcas_trig_set_time,
  .trig_set_timeout = ul_evplnxepollcas_trig_set_timeout,
  .trig_set_callback = ul_evplnxepoll_trig_set_callback,
  .trig_arm = ul_evplnxepollcas_trig_arm,
  .trig_disarm = ul_evplnxepollcas_trig_disarm,
  .trig_arm_once = ul_evplnxepollcas_trig_arm_once,
  .trig_set_param = ul_evplnxepoll_trig_set_param,
  .trig_get_param = ul_evplnxepoll_trig_get_param,
  .base_new = ul_evplnxepoll_base_new,
  .base_destroy = ul_evplnxepollcas_base_destroy,
  .base_update = ul_evplnxepoll_base_update,
  .base_dispatch = ul_evplnxepollcas_base_dispatch,
  .base_get_current_time = ul_evplnxepollcas_get_current_time,
  .base_set_option = ul_evplnxepollcas_base_set_option,
  .base_cascade = NULL,
};

#endif /*UL_EVP_LNXEPOLL_CASCADE*/
