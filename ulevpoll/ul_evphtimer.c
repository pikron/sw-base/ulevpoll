/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evphtimer.c	- logic to bind root htimer into given base

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#include "ul_evpoll.h"
#include "ul_htimer.h"

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

typedef struct ul_evphtimerroot_t {
  ul_evptrig_t evptrig;
  int htim_options;
  ul_root_htimer_ops_t htim_ops_orig;
} ul_evphtimerroot_t;

ul_evphtimerroot_t *ul_evphtimerroot_ptr;

void ul_evphtimerroot_cb(ul_evptrig_t *evptrig, int what)
{
  ul_evphtimerroot_t *mt = UL_CONTAINEROF(evptrig, ul_evphtimerroot_t, evptrig);

  ul_htimer_queue_t *root_htimer;
  ul_htim_time_t actual_time;

  if(what == UL_EVP_DONE) {
    ul_evptrig_done(&mt->evptrig);
    root_htimer = mt->htim_ops_orig.timer_root_get(mt->htim_options, NULL);
    *ul_root_htimer_ops = mt->htim_ops_orig;
    ul_evphtimerroot_ptr = NULL;
    ul_root_htimer_put(root_htimer);
    free(mt);
    return;
  }

  if(!(what & UL_EVP_TIMEOUT))
    return;

  ul_root_htimer_current_time(mt->htim_options, &actual_time);
  root_htimer = ul_root_htimer_get(mt->htim_options, NULL);
  ul_htimer_run_expired(root_htimer,&actual_time);
  ul_root_htimer_put(root_htimer);
}

void ul_evphtimerroot_put(ul_htimer_queue_t *queue)
{
  ul_htim_time_t next_expire;
  ul_evphtimerroot_t *mt = ul_evphtimerroot_ptr;

  if(!ul_htimer_next_expire(queue,&next_expire)) {
     ul_evptrig_disarm(&mt->evptrig);
  } else {
    if(ul_evptrig_set_time(&mt->evptrig, &next_expire) < 0)
      ul_logerr("ul_evptrig_set_time for root htimer failed\n");
    else
      ul_evptrig_arm(&mt->evptrig);
  }
  mt->htim_ops_orig.timer_root_put(queue);
}

int ul_evpoll_setup_root_htimer(ul_evpbase_t *base, int options)
{
  ul_evphtimerroot_t *mt;
  ul_htimer_queue_t *root_htimer;
  ul_htim_time_t next_expire;
  int already_setup_fl;

  if(!base) {
    base = ul_evpoll_chose_implicit_base();
    if(!base)
      return -1;
  }

  if(ul_root_htimer_ops == NULL) {
    if(ul_root_htimer_init(options, NULL) < 0) {
      ul_logerr("ul_root_htimer_init failed\n");
      return -1;
    }
  }

  root_htimer = ul_root_htimer_get(options, NULL);
  if(root_htimer == NULL) {
    ul_logerr("ul_root_htimer_get failed\n");
    return -1;
  }
  already_setup_fl = ul_evphtimerroot_ptr != NULL;
  ul_root_htimer_put(root_htimer);

  if(already_setup_fl) {
    ul_logerr("root htimer is already set\n");
    return -1;
  }

  mt = malloc(sizeof(ul_evphtimerroot_t));
  if(!mt) {
    ul_logerr("cannot allocate space for root htimer\n");
    return -1;
  }
  memset(mt, 0, sizeof(*mt));

  if(ul_evptrig_init(NULL, &mt->evptrig)<0) {
    ul_logerr("cannot init trigger for root htimer\n");
    free(mt);
    return -1;
  }

  mt->htim_options = options;

  ul_evptrig_set_callback(&mt->evptrig, ul_evphtimerroot_cb);

  root_htimer = ul_root_htimer_get(mt->htim_options, NULL);
  if(ul_evphtimerroot_ptr == NULL) {
    ul_evphtimerroot_ptr = mt;
    if(ul_htimer_next_expire(root_htimer,&next_expire)) {
      if(ul_evptrig_set_time(&mt->evptrig, &next_expire) < 0)
        ul_logerr("ul_evptrig_set_time for root htimer failed\n");
      else
        ul_evptrig_arm(&mt->evptrig);
    }
    mt->htim_ops_orig = *ul_root_htimer_ops;
    ul_root_htimer_ops->timer_root_put = ul_evphtimerroot_put;
  } else {
    already_setup_fl = 1;
  }
  ul_root_htimer_put(root_htimer);

  if(already_setup_fl) {
    ul_logerr("root htimer is already set\n");
    ul_evptrig_done(&mt->evptrig);
    free(mt);
    return -1;
  }

  return 0;
}
