/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpsysvpoll.c	- monitoring of open file handles

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#include <poll.h>

#include "ul_evpoll.h"
#include "ul_evp_sysvpoll.h"

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

UL_HTIMER_DEC_SCOPE(UL_ATTR_UNUSED static,
	      ul_evpsysvpoll_htimer, ul_evpbase_sysvpoll_t, ul_evptrig_sysvpoll_t, \
	      htim_queue, htim)

UL_HTIMER_IMP(ul_evpsysvpoll_htimer, ul_evpbase_sysvpoll_t, ul_evptrig_sysvpoll_t, \
	      htim_queue, htim)

#ifdef UL_HTIMER_WITH_MSTIME

ul_htim_time_t ul_evpsysvpoll_get_current_time(ul_evpbase_t *base)
{
  ul_mstime_t mstime;
  ul_htim_time_t htime;

  ul_mstime_now(&mstime);
  ul_mstime2htime(&htime, &mstime);

  return htime;
}

#endif /*UL_HTIMER_WITH_MSTIME*/

static inline ul_htim_time_t ul_evpsysvpoll_next_timeout(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_htim_time_t htime = ul_evpoll_get_current_time(base);
  ul_htime_add(&htime, &htime, timeout);
  return htime;
}

static int ul_evpsysvpoll_resize_pollfd_array(ul_evpbase_sysvpoll_t *eb, int count)
{
  ul_evptrig_sysvpoll_t **trig_array;
  struct pollfd *pollfd_array;

  int capacity = eb->pollfd_capacity;
  if(!eb->pollfd_array || !eb->trig_array)
    capacity = 0;

  if(count+4 < capacity/2) {
    capacity /= 2;
  } else if(count > capacity) {
    if(count < 8)
      capacity = 8;
    else
      capacity = (count + capacity/4 + 7) & ~7;
  }

  if(eb->pollfd_array && eb->trig_array && (capacity == eb->pollfd_capacity))
    return 0;

  ul_logdeb("ul_evpsysvpoll_resize_pollfd_array: old capacity %d to new capacity %d\n",
            eb->pollfd_capacity, capacity);

  if(!eb->trig_array)
    trig_array = malloc(capacity * sizeof(*eb->trig_array));
  else
    trig_array = realloc(eb->trig_array, capacity * sizeof(*eb->trig_array));

  if(trig_array)
    eb->trig_array = trig_array;
  else
    return -1;

  if(!eb->pollfd_array)
    pollfd_array = malloc(capacity * sizeof(*eb->pollfd_array));
  else
    pollfd_array = realloc(eb->pollfd_array, capacity * sizeof(*eb->pollfd_array));

  if(pollfd_array)
    eb->pollfd_array = pollfd_array;
  else
    return -1;

  eb->pollfd_capacity = capacity;

  return 0;
}

static inline int ul_evpsysvpoll_is_fd(ul_evptrig_sysvpoll_t *ei)
{
  return (ei->what_events & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_STATE)) &&
          (ei->fd >= 0);
}

static inline void ul_evpsysvpoll_sync_pollfd_array(ul_evpbase_sysvpoll_t *eb)
{
  int i;
  struct pollfd *ff, *fr;
  ul_evptrig_sysvpoll_t **ef, **er, *ei;
  int new_count = eb->activefd_count;
  int old_count = eb->pollfd_count;

  eb->need_resync = 0;

  ul_logdeb("ul_evpsysvpoll_sync_pollfd_array: old_count %d new_count %d\n",
            old_count, new_count);

  if(new_count > old_count) {
    if(ul_evpsysvpoll_resize_pollfd_array(eb, new_count)<0){
      ul_logerr("ul_evpsysvpoll_sync_pollfd_array: resize failed\n");
      return;
    }
  }

  i = 0;
  ef = eb->trig_array;
  ff = eb->pollfd_array;

  if(old_count > 0) {
    /* pack sparse arrays first */
    er = ef + old_count;
    fr = ff + old_count;

    for(;ef != er; ef++, ff++, i++) {
      if(*ef)
        continue;
      do {
        er--;
	fr--;
      } while(!*er && (ef != er));

      if(ef == er) {
        break;
      }
      ei = *er;
      ei->idx = i;
      *ef = ei;
      *ff = *fr;
    }
  }

  eb->pollfd_count = ef - eb->trig_array;

  er = eb->trig_array + new_count;
  for(ei = ul_evpsysvpoll_active_last(eb); ei; ei = ul_evpsysvpoll_active_prev(eb, ei)) {
    if(ei->idx != -1)
      break;

    if(!ul_evpsysvpoll_is_fd(ei)){
      ei->idx = -2;
      continue;
    }

    if(ef == er) {
      ul_logfatal("ul_evpsysvpoll_sync_pollfd_array: new_count too small for fds\n");
      return;
    }

    ul_logdeb("ul_evpsysvpoll_sync_pollfd_array: adding fd %d\n", ei->fd);

    ff->fd = ei->fd;
    ff->revents = 0;
    ff->events = 0;

    if((UL_EVP_IN == POLLIN) && (UL_EVP_OUT == POLLOUT) &&
       (UL_EVP_PRI == POLLPRI) && (UL_EVP_ERR == POLLERR) &&
       (UL_EVP_HUP == POLLHUP) && (UL_EVP_NVAL == POLLNVAL)) {
      ff->events |= ei->what_events & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_PRI |
                                       UL_EVP_ERR | UL_EVP_HUP | UL_EVP_NVAL);
    } else {
      if(ei->what_events & UL_EVP_IN)   ff->events |= POLLIN;
      if(ei->what_events & UL_EVP_OUT)  ff->events |= POLLOUT;
      if(ei->what_events & UL_EVP_PRI)  ff->events |= POLLPRI;
      if(ei->what_events & UL_EVP_ERR)  ff->events |= POLLERR;
      if(ei->what_events & UL_EVP_HUP)  ff->events |= POLLHUP;
      if(ei->what_events & UL_EVP_NVAL) ff->events |= POLLNVAL;
    }
    ei->idx = i;
    *ef = ei;

    ef++, ff++, i++;
  }

  if(ef - eb->trig_array != new_count) {
    ul_logerr("ul_evpsysvpoll_sync_pollfd_array: new_count %d but array size is %d\n",
               new_count, (int)(ef - eb->trig_array));
  }
  eb->pollfd_count = ef - eb->trig_array;

  if(new_count < old_count) {
    if(ul_evpsysvpoll_resize_pollfd_array(eb, new_count)<0){
      ul_logfatal("ul_evpsysvpoll_sync_pollfd_array: shrinking failed\n");
    }
  }
}

static inline void ul_evpsysvpoll_arm_internal(ul_evpbase_sysvpoll_t *eb,
                                 ul_evptrig_sysvpoll_t *ei)
{
  ei->idx = -1;

  if(ei->flags & UL_SYSVPOLL_ARMED) {
    ul_logerr("ul_evpsysvpoll_arm_internal: tring to arm armed evptrig\n");
    return;
  }

  ul_evpsysvpoll_idle_delete(eb, ei);
  ei->flags |= UL_SYSVPOLL_ARMED;
  ul_evpsysvpoll_active_insert(eb, ei);

  if(ul_evpsysvpoll_is_fd(ei)) {
    eb->activefd_count++;
    eb->need_resync = 1;
  }
}

static inline void ul_evpsysvpoll_disarm_internal(ul_evpbase_sysvpoll_t *eb,
                                 ul_evptrig_sysvpoll_t *ei)
{
  if(!(ei->flags & UL_SYSVPOLL_ARMED)) {
    ul_logerr("ul_evpsysvpoll_arm_internal: tring to disarm idle evptrig\n");
    return;
  }

  if(ei->idx >= 0)
     eb->trig_array[ei->idx] = NULL;

  if(ul_evpsysvpoll_is_fd(ei)) {
    eb->activefd_count--;
    eb->need_resync = 1;
  }

  ul_evpsysvpoll_active_delete(eb, ei);
  ei->flags &= ~UL_SYSVPOLL_ARMED;
  ul_evpsysvpoll_idle_insert(eb, ei);
}


static int ul_evpsysvpoll_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evptrig_sysvpoll_t *ei;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);

  evptrig->cb = NULL;
  evptrig->base = base;
  evptrig->impl_data = (ul_evptrig_sysvpoll_t*)malloc(sizeof(ul_evptrig_sysvpoll_t));
  if(!evptrig->impl_data) {
    ul_logerr("ul_evpsysvpoll_trig_init: malloc failed\n");
    return -1;
  }
  ei = evptrig->impl_data;
  memset(ei, 0, sizeof(ul_evptrig_sysvpoll_t));
  ei->fd = -1;

  ul_evpsysvpoll_htimer_init_detached(ei);

  ei->trig_ptr = evptrig;

  ul_evpsysvpoll_idle_insert(eb, ei);

  return 0;
}

static void ul_evpsysvpoll_trig_done(ul_evptrig_t *evptrig)
{
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;

  if(!ei)
    return;

  if(ei->flags & UL_SYSVPOLL_ARMED)
    ul_evptrig_disarm(evptrig);

  ul_evpsysvpoll_idle_del_item(ei);

  free(ei);

  evptrig->impl_data = NULL;
}

static int ul_evpsysvpoll_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;

  if(ei->flags & UL_SYSVPOLL_ARMED) {
    ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
    ul_evpsysvpoll_disarm_internal(eb, ei);
    ei->fd = fd;
    ei->what_events = what;
    ul_evpsysvpoll_arm_internal(eb, ei);
  } else {
    ei->fd = fd;
    ei->what_events = what;
  }
  return 0;
}

static int ul_evpsysvpoll_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);

  if((ei->flags & UL_SYSVPOLL_ARMED) &&
     (ei->flags & UL_SYSVPOLL_TIMED))
    ul_evpsysvpoll_htimer_detach(eb, ei);

  ei->flags &= ~(UL_SYSVPOLL_TIMED | UL_SYSVPOLL_TIMEOUT | UL_SYSVPOLL_PERIODIC);

  if(!time)
    return 0;

  ul_evpsysvpoll_htimer_set_expire(ei, *time);

  ei->flags |= UL_SYSVPOLL_TIMED;

  if(ei->flags & UL_SYSVPOLL_ARMED)
    ul_evpsysvpoll_htimer_add(eb, ei);

  return 0;
}

static int ul_evpsysvpoll_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);

  if((ei->flags & UL_SYSVPOLL_ARMED) &&
     (ei->flags & UL_SYSVPOLL_TIMED))
    ul_evpsysvpoll_htimer_detach(eb, ei);

  ei->flags &= ~(UL_SYSVPOLL_TIMED | UL_SYSVPOLL_TIMEOUT | UL_SYSVPOLL_PERIODIC);

  if(!timeout)
    return 0;

  ei->timeout = *timeout;

  ei->flags |= UL_SYSVPOLL_TIMED | UL_SYSVPOLL_TIMEOUT;

  if(ei->flags & UL_SYSVPOLL_ARMED) {
    ul_evpsysvpoll_htimer_set_expire(ei, ul_evpsysvpoll_next_timeout(&eb->base, &ei->timeout));
    ul_evpsysvpoll_htimer_add(eb, ei);
  }

  return 0;
}

static int ul_evpsysvpoll_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  evptrig->cb = cb;
  return 0;
}

static int ul_evpsysvpoll_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);

  if(ei->flags & UL_SYSVPOLL_ARMED)
    return 0;

  if(ei->flags & UL_SYSVPOLL_TIMEOUT)
    ul_evpsysvpoll_htimer_set_expire(ei, ul_evpsysvpoll_next_timeout(&eb->base, &ei->timeout));

  if(ei->flags & UL_SYSVPOLL_TIMED) {
    ul_evpsysvpoll_htimer_add(eb, ei);
  }

  ei->flags &= ~UL_SYSVPOLL_ONCE;
  ul_evpsysvpoll_arm_internal(eb, ei);

  return 0;
}

static int ul_evpsysvpoll_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);

  if(!(ei->flags & UL_SYSVPOLL_ARMED))
    return 0;

  if(ei->flags & UL_SYSVPOLL_TIMED)
    ul_evpsysvpoll_htimer_detach(eb, ei);

  ul_evpsysvpoll_disarm_internal(eb, ei);

  return 0;
}

static int ul_evpsysvpoll_trig_arm_once(ul_evptrig_t *evptrig)
{
  int ret = 0;
  ul_evptrig_sysvpoll_t *ei = evptrig->impl_data;
  if(!(ei->flags & UL_SYSVPOLL_ARMED))
    ret = ul_evpsysvpoll_trig_arm(evptrig);
  ei->flags |= UL_SYSVPOLL_ONCE;
  return ret;
}

static int ul_evpsysvpoll_trig_set_param(ul_evptrig_t *evptrig,
                           int parnum, const void *parval, int parsize)
{
  return -1;
}

static int ul_evpsysvpoll_trig_get_param(ul_evptrig_t *evptrig,
                           int parnum, void *parval, int parmaxsize)
{
  return -1;
}

static ul_evpbase_t *ul_evpsysvpoll_base_new(void)
{
  ul_evpbase_sysvpoll_t *eb;

  eb = (ul_evpbase_sysvpoll_t*)malloc(sizeof(ul_evpbase_sysvpoll_t));
  if(!eb)
    return NULL;

  memset(eb,0,sizeof(ul_evpbase_sysvpoll_t));

  ul_evpsysvpoll_htimer_init_queue(eb);

  eb->trig_array = NULL;
  eb->pollfd_array = NULL;
  eb->pollfd_count = 0;

  ul_evpsysvpoll_idle_init_head(eb);
  ul_evpsysvpoll_active_init_head(eb);

  eb->base.ops = &ul_evpoll_ops_sysvpoll;

  return &eb->base;
}

static void ul_evpsysvpoll_base_destroy(ul_evpbase_t *base)
{
  ul_evptrig_sysvpoll_t *ei;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);

  while((ei = ul_evpsysvpoll_active_first(eb))!=NULL){
    ul_evpsysvpoll_trig_disarm(ei->trig_ptr);
  }

  while((ei = ul_evpsysvpoll_idle_first(eb))!=NULL){
    if(ei->trig_ptr->cb)
      ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE for %i when poll is being destroyed\n", ei->fd);
    if(ei == ul_evpsysvpoll_idle_first(eb)) {
      ul_logdeb("UL_EVP_DONE not handled for %i when poll is being destroyed\n", ei->fd);
      ul_evpsysvpoll_trig_done(ei->trig_ptr);
    }
  }

  if(eb->trig_array)
    free(eb->trig_array);

  if(eb->pollfd_array)
    free(eb->pollfd_array);

  free(eb);
}

static int ul_evpsysvpoll_base_update(ul_evpbase_t *base)
{
  return 0;
}

static int ul_evpsysvpoll_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  ul_msdiff_t mstimeout = -1;
  int ret;
  int i;

  if(eb->quit_loop)
    return UL_EVP_DISPRET_QUIT;

  if(eb->need_resync)
    ul_evpsysvpoll_sync_pollfd_array(eb);

  if(timeout!=NULL) {
    ul_htimdiff2ms(&mstimeout, timeout);
    if(mstimeout < 0)
      mstimeout = 0;
  }

  if(ul_evpsysvpoll_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evpsysvpoll_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    ul_msdiff_t msdiff;
    eb->time_act = ul_evpoll_get_current_time(base);
    ul_htime_sub2ms(&msdiff, &eb->time_next_expire, &eb->time_act);
    if(msdiff < 0)
      msdiff = 0;
    if((mstimeout < 0) || (mstimeout > msdiff))
      mstimeout = msdiff;
  }

  ret = poll(eb->pollfd_array, eb->pollfd_count, mstimeout);

  if(ret < 0 ) {
    ret = UL_EVP_DISPRET_ERROR;
  } else if(ret == 0) {
    ret = UL_EVP_DISPRET_TIMEOUT;
  } else if(ret > 0) {
    ret = UL_EVP_DISPRET_HANDLED;
    /* An evptrig on one of the fds has occurred. */
    for(i=0; i<eb->pollfd_count; i++) {
      int revents = eb->pollfd_array[i].revents;
      int what = 0;
      eb->pollfd_array[i].revents = 0;
      ul_evptrig_sysvpoll_t *ei;
      if(!revents) continue;
      if((UL_EVP_IN == POLLIN) && (UL_EVP_OUT == POLLOUT) &&
         (UL_EVP_PRI == POLLPRI) && (UL_EVP_ERR == POLLERR) &&
         (UL_EVP_HUP == POLLHUP) && (UL_EVP_NVAL == POLLNVAL)) {
        what |= revents & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_PRI |
                           UL_EVP_ERR | UL_EVP_HUP | UL_EVP_NVAL);
      } else {
        if(revents & POLLIN)   what |= UL_EVP_IN;
        if(revents & POLLOUT)  what |= UL_EVP_OUT;
        if(revents & POLLPRI)  what |= UL_EVP_PRI;
        if(revents & POLLERR)  what |= UL_EVP_ERR;
        if(revents & POLLHUP)  what |= UL_EVP_HUP;
        if(revents & POLLNVAL) what |= UL_EVP_NVAL;
      }
      ei = eb->trig_array[i];
      if(!ei) {
        if(!eb->need_resync) {
	  ul_logerr("ul_evpsysvpoll_base_dispatch: empty slot and no need_resync\n");
          eb->need_resync = 1;
	}
	continue;
      }
      if(ei->flags & UL_SYSVPOLL_ONCE)
        ul_evpsysvpoll_trig_disarm(ei->trig_ptr);
      if(ei->flags & UL_SYSVPOLL_TIMEOUT)
        ul_evpsysvpoll_trig_set_timeout(ei->trig_ptr, &ei->timeout);
      if(ei->trig_ptr->cb)
        ei->trig_ptr->cb(ei->trig_ptr, what);
    }
  }

  if(ul_evpsysvpoll_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evpsysvpoll_htimer_next_expire(eb,&eb->time_next_expire);

  if(!eb->need_resync && eb->time_next_expire_fl) {
    ul_evptrig_sysvpoll_t *ei;
    eb->time_act = ul_evpoll_get_current_time(base);

    while((ei=ul_evpsysvpoll_htimer_cut_expired(eb, &eb->time_act))){
      if(ei->trig_ptr->cb)
        ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_TIMEOUT);
      ret = UL_EVP_DISPRET_HANDLED;
    }
  }

  return ret;
}

static int ul_evpsysvpoll_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);

  switch(option) {
    case UL_EVP_OPTION_QUIT:
      if(val)
        eb->quit_loop = 1;
      return 0;
   }
  return -1;
}

#ifdef UL_EVP_SYSVPOLL_CASCADE
static int ul_evpsysvpoll_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags);
#endif /*UL_EVP_SYSVPOLL_CASCADE*/

const ul_evpoll_ops_t ul_evpoll_ops_sysvpoll = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "sysvpoll",
  .trig_init = ul_evpsysvpoll_trig_init,
  .trig_done = ul_evpsysvpoll_trig_done,
  .trig_set_fd = ul_evpsysvpoll_trig_set_fd,
  .trig_set_time = ul_evpsysvpoll_trig_set_time,
  .trig_set_timeout = ul_evpsysvpoll_trig_set_timeout,
  .trig_set_callback = ul_evpsysvpoll_trig_set_callback,
  .trig_arm = ul_evpsysvpoll_trig_arm,
  .trig_disarm = ul_evpsysvpoll_trig_disarm,
  .trig_arm_once = ul_evpsysvpoll_trig_arm_once,
  .trig_set_param = ul_evpsysvpoll_trig_set_param,
  .trig_get_param = ul_evpsysvpoll_trig_get_param,
  .base_new = ul_evpsysvpoll_base_new,
  .base_destroy = ul_evpsysvpoll_base_destroy,
  .base_update = ul_evpsysvpoll_base_update,
  .base_dispatch = ul_evpsysvpoll_base_dispatch,
  .base_get_current_time = ul_evpsysvpoll_get_current_time,
  .base_set_option = ul_evpsysvpoll_base_set_option,
 #ifdef UL_EVP_SYSVPOLL_CASCADE
  .base_cascade = ul_evpsysvpoll_base_cascade,
 #endif /*UL_EVP_SYSVPOLL_CASCADE*/
};

#ifdef UL_EVP_SYSVPOLL_CASCADE

/* *([^ ]*) \(\*([^()]*)\)\(([^()]*)\);*/

static int ul_evpsysvpollcas_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  return ul_evptrig_init(eb->cascade_base, evptrig);
}

static void ul_evpsysvpollcas_trig_done(ul_evptrig_t *evptrig)
{
  ul_evpbase_sysvpoll_t *eb = (evptrig->base == NULL)? NULL:
          UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  if(eb)
    eb->cascade_base->ops->trig_done(evptrig);
}

static int ul_evpsysvpollcas_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_set_fd(evptrig, fd, what);
}

static int ul_evpsysvpollcas_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_set_time(evptrig, time);
}

static int ul_evpsysvpollcas_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_set_timeout(evptrig, timeout);
}

static int ul_evpsysvpollcas_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_set_callback(evptrig, cb);
}

static int ul_evpsysvpollcas_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_arm(evptrig);
}

static int ul_evpsysvpollcas_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_disarm(evptrig);
}

static int ul_evpsysvpollcas_trig_arm_once(ul_evptrig_t *evptrig)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->trig_arm_once(evptrig);
}

static void ul_evpsysvpollcas_base_destroy(ul_evpbase_t *base)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  ul_evpbase_t *cascaded_base = NULL;
  if(eb->cascade_propagate_destroy_fl)
    cascaded_base = eb->cascade_base;
  ul_evpsysvpoll_base_destroy(base);
  if(cascaded_base != NULL)
    cascaded_base->ops->base_destroy(cascaded_base);
}

static int ul_evpsysvpollcas_base_update(ul_evpbase_t *base)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->base_update(eb->cascade_base);
}

static int ul_evpsysvpollcas_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->base_dispatch(eb->cascade_base, timeout);
}

static ul_htim_time_t ul_evpsysvpollcas_get_current_time(ul_evpbase_t *base)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->base_get_current_time(eb->cascade_base);
}

static int ul_evpsysvpollcas_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->base_set_option(eb->cascade_base, option, val);
}

static int ul_evpsysvpollcas_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  return eb->cascade_base->ops->base_cascade(eb->cascade_base, new_base, prioshift, bc_flags);
}

const ul_evpoll_ops_t ul_evpoll_ops_sysvpollcas = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "sysvpoll_cascade",
  .trig_init = ul_evpsysvpollcas_trig_init,
  .trig_done = ul_evpsysvpollcas_trig_done,
  .trig_set_fd = ul_evpsysvpollcas_trig_set_fd,
  .trig_set_time = ul_evpsysvpollcas_trig_set_time,
  .trig_set_timeout = ul_evpsysvpollcas_trig_set_timeout,
  .trig_set_callback = ul_evpsysvpollcas_trig_set_callback,
  .trig_arm = ul_evpsysvpollcas_trig_arm,
  .trig_disarm = ul_evpsysvpollcas_trig_disarm,
  .trig_arm_once = ul_evpsysvpollcas_trig_arm_once,
  .trig_set_param = ul_evpsysvpoll_trig_set_param,
  .trig_get_param = ul_evpsysvpoll_trig_get_param,
  .base_new = NULL,
  .base_destroy = ul_evpsysvpollcas_base_destroy,
  .base_update = ul_evpsysvpollcas_base_update,
  .base_dispatch = ul_evpsysvpollcas_base_dispatch,
  .base_get_current_time = ul_evpsysvpollcas_get_current_time,
  .base_set_option = ul_evpsysvpollcas_base_set_option,
  .base_cascade = ul_evpsysvpollcas_base_cascade,
};

typedef struct {
  ul_evptrig_t evptrig_my_base;
  ul_evpbase_t *my_base;
  ul_evptrig_t evptrig_new_base;
} ul_evpsysvpollcas_monitor_t;

static void ul_evpsysvpollcas_monitor_done(ul_evpsysvpollcas_monitor_t *mon)
{
  ul_evpsysvpoll_trig_done(&mon->evptrig_my_base);
  ul_evptrig_done(&mon->evptrig_new_base);
  free(mon);
}

static void ul_evpsysvpollcas_monitor_cb_my(ul_evptrig_t *evptrig, int what)
{
  ul_evpsysvpollcas_monitor_t *mon =
    UL_CONTAINEROF(evptrig, ul_evpsysvpollcas_monitor_t, evptrig_my_base);

  if(!(what & UL_EVP_DONE))
    return;
  ul_evpsysvpollcas_monitor_done(mon);
}

static void ul_evpsysvpollcas_monitor_cb_new(ul_evptrig_t *evptrig, int what)
{
  ul_evpsysvpollcas_monitor_t *mon =
    UL_CONTAINEROF(evptrig, ul_evpsysvpollcas_monitor_t, evptrig_new_base);
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(mon->my_base, ul_evpbase_sysvpoll_t, base);

  if(!(what & UL_EVP_DONE))
    return;
  ul_evpsysvpollcas_monitor_done(mon);

  eb->cascade_base = NULL;
  eb->base.ops = &ul_evpoll_ops_sysvpoll;

  if(eb->cascade_inerit_destroy_fl)
    ul_evpoll_destroy(&eb->base);
}

static int ul_evpsysvpoll_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  ul_evptrig_sysvpoll_t *ei;
  ul_evpbase_sysvpoll_t *eb = UL_CONTAINEROF(base, ul_evpbase_sysvpoll_t, base);
  ul_evpsysvpollcas_monitor_t *mon;
  ul_evptrig_t *evptrig;
  int res;
  int ret = 0;

  ul_evpoll_cb_t cb;
  unsigned       flags;
  unsigned       what_events;
  ul_evfd_t      fd;
  ul_htim_diff_t timeout;
  ul_htim_time_t exptime;

  while(1) {
    ei = ul_evpsysvpoll_idle_cut_first(eb);
    if(ei == NULL) {
      ei = ul_evpsysvpoll_active_cut_first(eb);
      if(ei == NULL)
        break;
    }
    evptrig = ei->trig_ptr;
    cb = evptrig->cb;
    flags = ei->flags;
    what_events = ei->what_events;
    fd = ei->fd;
    timeout = ei->timeout;
    exptime = ul_evpsysvpoll_htimer_get_expire(ei);

    ul_evpsysvpoll_trig_done(evptrig);

    res = ul_evptrig_init(new_base, evptrig);
    if(res < 0) {
      ul_logerr("ul_evpsysvpoll_base_cascade: trig reinsertion failded for fd %ld\n",
                (long) fd);
      ret = -1;
      continue;
    }

    ul_evptrig_set_callback(evptrig, cb);
    if((what_events & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_STATE)) && (fd >= 0))
      if(ul_evptrig_set_fd(evptrig, fd, what_events) < 0)
        goto cascade_failed;
    if(flags & UL_SYSVPOLL_TIMEOUT) {
      if(ul_evptrig_set_timeout(evptrig, &timeout) < 0)
        goto cascade_failed;
    } else if(flags & UL_SYSVPOLL_TIMED) {
        if(ul_evptrig_set_time(evptrig, &exptime) < 0)
          goto cascade_failed;
    }
    if(flags & UL_SYSVPOLL_ARMED) {
      if(flags & UL_SYSVPOLL_ONCE) {
        if(ul_evptrig_arm_once(evptrig) < 0)
          goto cascade_failed;
      } else {
        if(ul_evptrig_arm(evptrig) < 0)
          goto cascade_failed;
      }
    }
    continue;

   cascade_failed:
    ul_logerr("ul_evpsysvpoll_base_cascade: parameters mimic failed fd %ld\n",
                (long) fd);
  }

  while((ei = ul_evpsysvpoll_idle_cut_first(eb))!=NULL){
    ul_evpsysvpoll_trig_done(ei->trig_ptr);
  }

  if(eb->trig_array)
    free(eb->trig_array);
  eb->trig_array = NULL;
  if(eb->pollfd_array)
    free(eb->pollfd_array);
  eb->pollfd_array = NULL;
  eb->pollfd_count = 0;
  eb->activefd_count = 0;

  mon = malloc(sizeof(*mon));
  if(mon != NULL) {
    memset(mon, 0, sizeof(*mon));
    ul_evptrig_init(base, &mon->evptrig_my_base);
    ul_evptrig_set_callback(&mon->evptrig_my_base, ul_evpsysvpollcas_monitor_cb_my);
    ul_evptrig_init(new_base, &mon->evptrig_new_base);
    ul_evptrig_set_callback(&mon->evptrig_new_base, ul_evpsysvpollcas_monitor_cb_new);
    mon->my_base = base;
  }

  eb->cascade_base = new_base;
  eb->base.ops = &ul_evpoll_ops_sysvpollcas;

  eb->cascade_inerit_destroy_fl = (bc_flags & UL_EVP_CASFL_INHERIT_DESTROY)? 1: 0;
  eb->cascade_propagate_destroy_fl = (bc_flags & UL_EVP_CASFL_PROPAGATE_DESTROY)? 1: 0;

  return ret;
}

#endif /*UL_EVP_SYSVPOLL_CASCADE*/
