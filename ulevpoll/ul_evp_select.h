/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evpollint.h	- monitoring of open file handles

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_EVPOLLINT_H
#define _UL_EVPOLLINT_H

#include "ul_utdefs.h"

#include <string.h>

#include "ul_evpoll.h"
#include <ul_list.h>
#include <ul_htimer.h>

#ifndef _WIN32
#include <sys/types.h>
#include <sys/select.h>
#else /*_WIN32*/
#include <winsock2.h>
#include <windows.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Provide cascade support
 */
#define UL_EVP_SELECT_CASCADE

#define UL_EVPSELECT_ARMED    0x01
#define UL_EVPSELECT_ONCE     0x02
#define UL_EVPSELECT_TIMED    0x04
#define UL_EVPSELECT_TIMEOUT  0x08
#define UL_EVPSELECT_PERIODIC 0x10

typedef struct ul_evptrig_data_t {
  unsigned flags;
  unsigned what_events;
  unsigned pending_events;

  ul_evfd_t      fd;

  ul_htim_diff_t timeout;
  ul_htim_node_t htim;

  ul_list_node_t list_node;

  ul_evptrig_t *trig_ptr;

} ul_evptrig_select_t;

typedef struct ul_evpbase_select_t {
  ul_evpbase_t base;

  int fds_size;
  int nfd_max;
  size_t fds_mem_size;

  fd_set *readfds;
  fd_set *writefds;
  fd_set *exceptfds;
  fd_set *exceptfds_forboth;

  fd_set *out_readfds;
  fd_set *out_writefds;
  fd_set *out_exceptfds;

  int time_next_expire_fl:1;
  int quit_loop:1;
  int cascade_inerit_destroy_fl:1;
  int cascade_propagate_destroy_fl:1;

  ul_list_head_t active_list;
  ul_list_head_t report_list;
  ul_list_head_t idle_list;

  ul_htim_time_t time_act;
  ul_htim_time_t time_next_expire;
  ul_htim_queue_t htim_queue;

 #ifdef UL_EVP_SELECT_CASCADE
  ul_evpbase_t   *cascade_base;
 #endif /*UL_EVP_SELECT_CASCADE*/
} ul_evpbase_select_t;


UL_LIST_CUST_DEC(ul_evpselect_active, ul_evpbase_select_t, ul_evptrig_select_t,
                active_list, list_node)

UL_LIST_CUST_DEC(ul_evpselect_idle, ul_evpbase_select_t, ul_evptrig_select_t,
                idle_list, list_node)

UL_LIST_CUST_DEC(ul_evpselect_report, ul_evpbase_select_t, ul_evptrig_select_t,
                report_list, list_node)

extern const ul_evpoll_ops_t ul_evpoll_ops_select;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_EVPOLLINT_H */
