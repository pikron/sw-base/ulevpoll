/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_evp_select.c	- monitoring of open file handles

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#ifndef _WIN32
#include <sys/types.h>
#include <sys/select.h>
#else /*_WIN32*/
#include <winsock2.h>
#include <windows.h>
#endif

#include "ul_evpoll.h"
#include "ul_evp_select.h"

#include <ul_log.h>

extern UL_LOG_CUST(ulogd_evpoll);

UL_HTIMER_DEC_SCOPE(UL_ATTR_UNUSED static,
	      ul_evpselect_htimer, ul_evpbase_select_t, ul_evptrig_select_t, \
	      htim_queue, htim)

UL_HTIMER_IMP(ul_evpselect_htimer, ul_evpbase_select_t, ul_evptrig_select_t, \
	      htim_queue, htim)

#ifdef UL_HTIMER_WITH_MSTIME

ul_htim_time_t ul_evpselect_get_current_time(ul_evpbase_t *base)
{
  ul_mstime_t mstime;
  ul_htim_time_t htime;

  ul_mstime_now(&mstime);
  ul_mstime2htime(&htime, &mstime);

  return htime;
}

#endif /*UL_HTIMER_WITH_MSTIME*/

static inline ul_htim_time_t ul_evpselect_next_timeout(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_htim_time_t htime = ul_evpoll_get_current_time(base);
  ul_htime_add(&htime, &htime, timeout);
  return htime;
}

static int ul_evpselect_resize_fds(ul_evpbase_select_t *eb, int new_fds_size)
{
  int i;
  size_t new_fds_mem_size;
  size_t copy_fds_mem_size;
  fd_set *new_fds[7];
  fd_set **fds_ptr[7] = {&eb->readfds, &eb->writefds, &eb->exceptfds, &eb->exceptfds_forboth,
                         &eb->out_readfds, &eb->out_writefds, &eb->out_exceptfds};


  if(!new_fds_size || (new_fds_size < FD_SETSIZE)) {
    new_fds_size = FD_SETSIZE;
    new_fds_mem_size = sizeof(fd_set);
  } else {
    new_fds_size = (new_fds_size + FD_SETSIZE - 1) / FD_SETSIZE;
    new_fds_mem_size = sizeof(fd_set) * new_fds_size;
    new_fds_size *= FD_SETSIZE;
  }

  for(i = 0; i < 7; i++) {
    new_fds[i] = (fd_set *)malloc(new_fds_mem_size);
    if(new_fds[i] == NULL) {
      while(i-- > 0) {
        free(new_fds[i]);
      }
      return -1;
    }
  }
  copy_fds_mem_size = eb->fds_mem_size;
  if(copy_fds_mem_size > new_fds_mem_size)
    copy_fds_mem_size = new_fds_mem_size;
  for(i = 0; i < 7; i++) {
    memset(new_fds[i], 0, new_fds_mem_size);
    if(*fds_ptr[i] != NULL) {
      memcpy(new_fds[i], *fds_ptr[i], copy_fds_mem_size);
    }
    *fds_ptr[i] = new_fds[i];
  }
  eb->fds_size = new_fds_size;
  eb->fds_mem_size = new_fds_mem_size;
  return 0;
}

static void ul_evpselect_free_fds(ul_evpbase_select_t *eb)
{
  int i;
  fd_set **fds_ptr[7] = {&eb->readfds, &eb->writefds, &eb->exceptfds, &eb->exceptfds_forboth,
                         &eb->out_readfds, &eb->out_writefds, &eb->out_exceptfds};
  for(i = 0; i < 7; i++) {
    free(*fds_ptr[i]);
    *fds_ptr[i] = NULL;
  }
  eb->fds_size = 0;
  eb->nfd_max = 0;
  eb->fds_mem_size = 0;
}

static inline int ul_evpselect_is_fd(ul_evptrig_select_t *ei)
{
  return (ei->what_events & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_STATE)) &&
          (ei->fd >= 0);
}

static inline void ul_evpselect_arm_internal(ul_evpbase_select_t *eb,
                                 ul_evptrig_select_t *ei)
{
  if(ei->flags & UL_EVPSELECT_ARMED) {
    ul_logerr("ul_evpselect_arm_internal: tring to arm armed evptrig\n");
    return;
  }

  ul_evpselect_idle_delete(eb, ei);
  ei->flags |= UL_EVPSELECT_ARMED;
  ul_evpselect_active_insert(eb, ei);

  if(ul_evpselect_is_fd(ei)) {
    if(ei->what_events & UL_EVP_IN)
      FD_SET(ei->fd, eb->readfds);
    if(ei->what_events & UL_EVP_OUT)
      FD_SET(ei->fd, eb->writefds);
    if(ei->what_events & UL_EVP_STATE) {
      if(FD_ISSET(ei->fd, eb->exceptfds))
        FD_SET(ei->fd, eb->exceptfds_forboth);
      FD_SET(ei->fd, eb->exceptfds);
    }
  }
}

static inline void ul_evpselect_disarm_internal(ul_evpbase_select_t *eb,
                                 ul_evptrig_select_t *ei)
{
  if(!(ei->flags & UL_EVPSELECT_ARMED)) {
    ul_logerr("ul_evpselect_arm_internal: tring to disarm idle evptrig\n");
    return;
  }

  if(ul_evpselect_is_fd(ei)) {
    if(ei->what_events & UL_EVP_IN)
      FD_CLR(ei->fd, eb->readfds);
    if(ei->what_events & UL_EVP_OUT)
      FD_CLR(ei->fd, eb->writefds);
    if(ei->what_events & UL_EVP_STATE) {
      if(!FD_ISSET(ei->fd, eb->exceptfds_forboth))
        FD_CLR(ei->fd, eb->exceptfds);
      FD_CLR(ei->fd, eb->exceptfds_forboth);
    }
  }

  ul_evpselect_active_del_item(ei);
  ei->flags &= ~UL_EVPSELECT_ARMED;
  ul_evpselect_idle_insert(eb, ei);
}


static int ul_evpselect_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evptrig_select_t *ei;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);

  evptrig->cb = NULL;
  evptrig->base = base;
  evptrig->impl_data = (ul_evptrig_select_t*)malloc(sizeof(ul_evptrig_select_t));
  if(!evptrig->impl_data) {
    ul_logerr("ul_evpselect_trig_init: malloc failed\n");
    return -1;
  }
  ei = evptrig->impl_data;
  memset(ei, 0, sizeof(ul_evptrig_select_t));
  ei->fd = -1;

  ul_evpselect_htimer_init_detached(ei);

  ei->trig_ptr = evptrig;

  ul_evpselect_idle_insert(eb, ei);

  return 0;
}

static void ul_evpselect_trig_done(ul_evptrig_t *evptrig)
{
  ul_evptrig_select_t *ei = evptrig->impl_data;

  if(!ei)
    return;

  if(ei->flags & UL_EVPSELECT_ARMED)
    ul_evptrig_disarm(evptrig);

  ul_evpselect_idle_del_item(ei);

  free(ei);

  evptrig->impl_data = NULL;
}

static int ul_evpselect_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evptrig_select_t *ei = evptrig->impl_data;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);

  if(fd >= eb->nfd_max) {
    if(fd >= eb->fds_size) {
      if(ul_evpselect_resize_fds(eb, fd + 1) < 0)
        return -1;
    }
    eb->nfd_max = fd + 1;
  }

  if(ei->flags & UL_EVPSELECT_ARMED) {
    ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
    ul_evpselect_disarm_internal(eb, ei);
    ei->fd = fd;
    ei->what_events = what;
    ul_evpselect_arm_internal(eb, ei);
  } else {
    ei->fd = fd;
    ei->what_events = what;
  }
  return 0;
}

static int ul_evpselect_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evptrig_select_t *ei = evptrig->impl_data;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);

  if((ei->flags & UL_EVPSELECT_ARMED) &&
     (ei->flags & UL_EVPSELECT_TIMED))
    ul_evpselect_htimer_detach(eb, ei);

  ei->flags &= ~(UL_EVPSELECT_TIMED | UL_EVPSELECT_TIMEOUT | UL_EVPSELECT_PERIODIC);

  if(!time)
    return 0;

  ul_evpselect_htimer_set_expire(ei, *time);

  ei->flags |= UL_EVPSELECT_TIMED;

  if(ei->flags & UL_EVPSELECT_ARMED)
    ul_evpselect_htimer_add(eb, ei);

  return 0;
}

static int ul_evpselect_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evptrig_select_t *ei = evptrig->impl_data;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);

  if((ei->flags & UL_EVPSELECT_ARMED) &&
     (ei->flags & UL_EVPSELECT_TIMED))
    ul_evpselect_htimer_detach(eb, ei);

  ei->flags &= ~(UL_EVPSELECT_TIMED | UL_EVPSELECT_TIMEOUT | UL_EVPSELECT_PERIODIC);

  if(!timeout)
    return 0;

  ei->timeout = *timeout;

  ei->flags |= UL_EVPSELECT_TIMED | UL_EVPSELECT_TIMEOUT;

  if(ei->flags & UL_EVPSELECT_ARMED) {
    ul_evpselect_htimer_set_expire(ei, ul_evpselect_next_timeout(&eb->base, &ei->timeout));
    ul_evpselect_htimer_add(eb, ei);
  }

  return 0;
}

static int ul_evpselect_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  evptrig->cb = cb;
  return 0;
}

static int ul_evpselect_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evptrig_select_t *ei = evptrig->impl_data;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);

  if(ei->flags & UL_EVPSELECT_ARMED)
    return 0;

  if(ei->flags & UL_EVPSELECT_TIMEOUT)
    ul_evpselect_htimer_set_expire(ei, ul_evpselect_next_timeout(&eb->base, &ei->timeout));

  if(ei->flags & UL_EVPSELECT_TIMED) {
    ul_evpselect_htimer_add(eb, ei);
  }

  ei->flags &= ~UL_EVPSELECT_ONCE;
  ul_evpselect_arm_internal(eb, ei);

  return 0;
}

static int ul_evpselect_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evptrig_select_t *ei = evptrig->impl_data;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);

  if(!(ei->flags & UL_EVPSELECT_ARMED))
    return 0;

  if(ei->flags & UL_EVPSELECT_TIMED)
    ul_evpselect_htimer_detach(eb, ei);

  ul_evpselect_disarm_internal(eb, ei);

  return 0;
}

static int ul_evpselect_trig_arm_once(ul_evptrig_t *evptrig)
{
  int ret = 0;
  ul_evptrig_select_t *ei = evptrig->impl_data;
  if(!(ei->flags & UL_EVPSELECT_ARMED))
    ret = ul_evpselect_trig_arm(evptrig);
  ei->flags |= UL_EVPSELECT_ONCE;
  return ret;
}

static int ul_evpselect_trig_set_param(ul_evptrig_t *evptrig,
                           int parnum, const void *parval, int parsize)
{
  return -1;
}

static int ul_evpselect_trig_get_param(ul_evptrig_t *evptrig,
                           int parnum, void *parval, int parmaxsize)
{
  return -1;
}

static ul_evpbase_t *ul_evpselect_base_new(void)
{
  ul_evpbase_select_t *eb;

  eb = (ul_evpbase_select_t*)malloc(sizeof(ul_evpbase_select_t));
  if(!eb)
    return NULL;

  memset(eb,0,sizeof(ul_evpbase_select_t));

  ul_evpselect_htimer_init_queue(eb);

  eb->nfd_max = 0;

  if(ul_evpselect_resize_fds(eb, 0) < 0)
    return NULL;

  ul_evpselect_idle_init_head(eb);
  ul_evpselect_active_init_head(eb);
  ul_evpselect_report_init_head(eb);

  eb->base.ops = &ul_evpoll_ops_select;

  return &eb->base;
}

static void ul_evpselect_base_destroy(ul_evpbase_t *base)
{
  ul_evptrig_select_t *ei;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);

  while((ei = ul_evpselect_report_first(eb))!=NULL){
    ul_evpselect_trig_disarm(ei->trig_ptr);
  }

  while((ei = ul_evpselect_active_first(eb))!=NULL){
    ul_evpselect_trig_disarm(ei->trig_ptr);
  }

  while((ei = ul_evpselect_idle_first(eb))!=NULL){
    if(ei->trig_ptr->cb)
      ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_DONE);
    else
      ul_logdeb("No callback for UL_EVP_DONE for %i when poll is being destroyed\n", ei->fd);
    if(ei == ul_evpselect_idle_first(eb)) {
      ul_logdeb("UL_EVP_DONE not handled for %i when poll is being destroyed\n", ei->fd);
      ul_evpselect_trig_done(ei->trig_ptr);
    }
  }

  ul_evpselect_free_fds(eb);

  free(eb);
}

static int ul_evpselect_base_update(ul_evpbase_t *base)
{
  return 0;
}

static int ul_evpselect_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  ul_htim_diff_t timeout_min = -1;
  ul_htim_diff_t time_diff;
  struct timeval timeout_tv;
  int ret;

  if(eb->quit_loop)
    return UL_EVP_DISPRET_QUIT;

  if(timeout!=NULL) {
    timeout_min = *timeout;
    if(timeout_min < 0)
      timeout_min = 0;
  }

  if(ul_evpselect_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evpselect_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    eb->time_act = ul_evpoll_get_current_time(base);
    ul_htime_sub(&time_diff, &eb->time_next_expire, &eb->time_act);
    if(time_diff < 0)
      time_diff = 0;
    if((timeout_min < 0) || (timeout_min > time_diff))
      timeout_min = time_diff;
  }

  if(timeout_min >= 0) {
    timeout_tv.tv_sec = timeout_min / UL_HTIM_RESOLUTION;
    timeout_tv.tv_usec = (timeout_min % UL_HTIM_RESOLUTION) * (1000000 / UL_HTIM_RESOLUTION);
  }

  memcpy(eb->out_readfds, eb->readfds, eb->fds_mem_size);
  memcpy(eb->out_writefds, eb->writefds, eb->fds_mem_size);
  memcpy(eb->out_exceptfds, eb->exceptfds, eb->fds_mem_size);

  ret = select(eb->nfd_max, eb->out_readfds, eb->out_writefds,
               eb->out_exceptfds, timeout_min >= 0? &timeout_tv: NULL);

  if(ret < 0 ) {
    ret = UL_EVP_DISPRET_ERROR;
  } else if(ret == 0) {
    ret = UL_EVP_DISPRET_TIMEOUT;
  } else if(ret > 0) {
    ul_evptrig_select_t *ei;
    ul_evptrig_select_t *ei_next;


    ul_list_for_each(ul_evpselect_report, eb, ei) {
      unsigned what = 0;

      if(!ul_evpselect_is_fd(ei))
        continue;

      if(FD_ISSET(ei->fd, eb->out_readfds) && (ei->what_events && UL_EVP_IN)) {
        what |= UL_EVP_IN;
        ret--;
      }
      if(FD_ISSET(ei->fd, eb->out_writefds) && (ei->what_events && UL_EVP_OUT)) {
        what |= UL_EVP_OUT;
        ret--;
      }
      if(FD_ISSET(ei->fd, eb->out_exceptfds) && (ei->what_events && UL_EVP_STATE)) {
        what |= UL_EVP_PRI;
        ret--;
      }
      ei->pending_events |= what;
      if(ret <= 0)
        break;
    }

    for(ei = ul_evpselect_active_first(eb); (ei != NULL) && (ret >= 0); ei = ei_next) {
      unsigned what = 0;
      ei_next = ul_evpselect_active_next(eb, ei);

      if(!ul_evpselect_is_fd(ei))
        continue;

      if(FD_ISSET(ei->fd, eb->out_readfds) && (ei->what_events && UL_EVP_IN)) {
        what |= UL_EVP_IN;
        ret--;
      }
      if(FD_ISSET(ei->fd, eb->out_writefds) && (ei->what_events && UL_EVP_OUT)) {
        what |= UL_EVP_OUT;
        ret--;
      }
      if(FD_ISSET(ei->fd, eb->out_exceptfds) && (ei->what_events && UL_EVP_STATE)) {
        what |= UL_EVP_PRI;
        ret--;
      }
      if(what) {
        ei->pending_events |= what;
        ul_evpselect_active_delete(eb, ei);
        ul_evpselect_report_insert(eb, ei);
      }
    }

    ul_list_for_each_cut(ul_evpselect_report, eb, ei) {
      unsigned pending = ei->pending_events;
      ul_evpselect_active_ins_head(eb, ei);
      if(!(ei->what_events & pending) ||
         !(ei->flags & UL_EVPSELECT_ARMED))
        continue;
      ei->pending_events &= ~ei->what_events;
      if(ei->flags & UL_EVPSELECT_ONCE)
        ul_evpselect_trig_disarm(ei->trig_ptr);
      if(ei->flags & UL_EVPSELECT_TIMEOUT)
        ul_evpselect_trig_set_timeout(ei->trig_ptr, &ei->timeout);
      if(ei->trig_ptr->cb) {
        ei->trig_ptr->cb(ei->trig_ptr, ei->what_events & pending);
      }
    }
    ret = UL_EVP_DISPRET_HANDLED;
  }

  if(ul_evpselect_htimer_first_changed(eb))
    eb->time_next_expire_fl=ul_evpselect_htimer_next_expire(eb,&eb->time_next_expire);

  if(eb->time_next_expire_fl) {
    ul_evptrig_select_t *ei;
    eb->time_act = ul_evpoll_get_current_time(base);

    while((ei=ul_evpselect_htimer_cut_expired(eb, &eb->time_act))){
      if(ei->trig_ptr->cb)
        ei->trig_ptr->cb(ei->trig_ptr, UL_EVP_TIMEOUT);
      ret = UL_EVP_DISPRET_HANDLED;
    }
  }

  return ret;
}

static int ul_evpselect_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);

  switch(option) {
    case UL_EVP_OPTION_QUIT:
      if(val)
        eb->quit_loop = 1;
      return 0;
   }
  return -1;
}

#ifdef UL_EVP_SELECT_CASCADE
static int ul_evpselect_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags);
#endif /*UL_EVP_SELECT_CASCADE*/

const ul_evpoll_ops_t ul_evpoll_ops_select = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "select",
  .trig_init = ul_evpselect_trig_init,
  .trig_done = ul_evpselect_trig_done,
  .trig_set_fd = ul_evpselect_trig_set_fd,
  .trig_set_time = ul_evpselect_trig_set_time,
  .trig_set_timeout = ul_evpselect_trig_set_timeout,
  .trig_set_callback = ul_evpselect_trig_set_callback,
  .trig_arm = ul_evpselect_trig_arm,
  .trig_disarm = ul_evpselect_trig_disarm,
  .trig_arm_once = ul_evpselect_trig_arm_once,
  .trig_set_param = ul_evpselect_trig_set_param,
  .trig_get_param = ul_evpselect_trig_get_param,
  .base_new = ul_evpselect_base_new,
  .base_destroy = ul_evpselect_base_destroy,
  .base_update = ul_evpselect_base_update,
  .base_dispatch = ul_evpselect_base_dispatch,
  .base_get_current_time = ul_evpselect_get_current_time,
  .base_set_option = ul_evpselect_base_set_option,
 #ifdef UL_EVP_SELECT_CASCADE
  .base_cascade = ul_evpselect_base_cascade,
 #endif /*UL_EVP_SELECT_CASCADE*/
};

#ifdef UL_EVP_SELECT_CASCADE

/* *([^ ]*) \(\*([^()]*)\)\(([^()]*)\);*/

static int ul_evpselectcas_trig_init(ul_evpbase_t *base, ul_evptrig_t *evptrig)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  return ul_evptrig_init(eb->cascade_base, evptrig);
}

static void ul_evpselectcas_trig_done(ul_evptrig_t *evptrig)
{
  ul_evpbase_select_t *eb = (evptrig->base == NULL)? NULL:
          UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  if(eb)
    eb->cascade_base->ops->trig_done(evptrig);
}

static int ul_evpselectcas_trig_set_fd(ul_evptrig_t *evptrig, ul_evfd_t fd, int what)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_set_fd(evptrig, fd, what);
}

static int ul_evpselectcas_trig_set_time(ul_evptrig_t *evptrig, ul_htim_time_t *time)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_set_time(evptrig, time);
}

static int ul_evpselectcas_trig_set_timeout(ul_evptrig_t *evptrig, ul_htim_diff_t *timeout)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_set_timeout(evptrig, timeout);
}

static int ul_evpselectcas_trig_set_callback(ul_evptrig_t *evptrig, ul_evpoll_cb_t cb)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_set_callback(evptrig, cb);
}

static int ul_evpselectcas_trig_arm(ul_evptrig_t *evptrig)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_arm(evptrig);
}

static int ul_evpselectcas_trig_disarm(ul_evptrig_t *evptrig)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_disarm(evptrig);
}

static int ul_evpselectcas_trig_arm_once(ul_evptrig_t *evptrig)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(evptrig->base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->trig_arm_once(evptrig);
}

static void ul_evpselectcas_base_destroy(ul_evpbase_t *base)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  ul_evpbase_t *cascaded_base = NULL;
  if(eb->cascade_propagate_destroy_fl)
    cascaded_base = eb->cascade_base;
  ul_evpselect_base_destroy(base);
  if(cascaded_base != NULL)
    cascaded_base->ops->base_destroy(cascaded_base);
}

static int ul_evpselectcas_base_update(ul_evpbase_t *base)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->base_update(eb->cascade_base);
}

static int ul_evpselectcas_base_dispatch(ul_evpbase_t *base, ul_htim_diff_t *timeout)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->base_dispatch(eb->cascade_base, timeout);
}

static ul_htim_time_t ul_evpselectcas_get_current_time(ul_evpbase_t *base)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->base_get_current_time(eb->cascade_base);
}

static int ul_evpselectcas_base_set_option(ul_evpbase_t *base, int option, int val)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->base_set_option(eb->cascade_base, option, val);
}

static int ul_evpselectcas_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  return eb->cascade_base->ops->base_cascade(eb->cascade_base, new_base, prioshift, bc_flags);
}

const ul_evpoll_ops_t ul_evpoll_ops_selectcas = {
  .ops_version = UL_EVP_OPS_VERSION,
  .ops_size = sizeof(ul_evpoll_ops_t),
  .name = "select_cascade",
  .trig_init = ul_evpselectcas_trig_init,
  .trig_done = ul_evpselectcas_trig_done,
  .trig_set_fd = ul_evpselectcas_trig_set_fd,
  .trig_set_time = ul_evpselectcas_trig_set_time,
  .trig_set_timeout = ul_evpselectcas_trig_set_timeout,
  .trig_set_callback = ul_evpselectcas_trig_set_callback,
  .trig_arm = ul_evpselectcas_trig_arm,
  .trig_disarm = ul_evpselectcas_trig_disarm,
  .trig_arm_once = ul_evpselectcas_trig_arm_once,
  .trig_set_param = ul_evpselect_trig_set_param,
  .trig_get_param = ul_evpselect_trig_get_param,
  .base_new = NULL,
  .base_destroy = ul_evpselectcas_base_destroy,
  .base_update = ul_evpselectcas_base_update,
  .base_dispatch = ul_evpselectcas_base_dispatch,
  .base_get_current_time = ul_evpselectcas_get_current_time,
  .base_set_option = ul_evpselectcas_base_set_option,
  .base_cascade = ul_evpselectcas_base_cascade,
};

typedef struct {
  ul_evptrig_t evptrig_my_base;
  ul_evpbase_t *my_base;
  ul_evptrig_t evptrig_new_base;
} ul_evpselectcas_monitor_t;

static void ul_evpselectcas_monitor_done(ul_evpselectcas_monitor_t *mon)
{
  ul_evpselect_trig_done(&mon->evptrig_my_base);
  ul_evptrig_done(&mon->evptrig_new_base);
  free(mon);
}

static void ul_evpselectcas_monitor_cb_my(ul_evptrig_t *evptrig, int what)
{
  ul_evpselectcas_monitor_t *mon =
    UL_CONTAINEROF(evptrig, ul_evpselectcas_monitor_t, evptrig_my_base);

  if(!(what & UL_EVP_DONE))
    return;
  ul_evpselectcas_monitor_done(mon);
}

static void ul_evpselectcas_monitor_cb_new(ul_evptrig_t *evptrig, int what)
{
  ul_evpselectcas_monitor_t *mon =
    UL_CONTAINEROF(evptrig, ul_evpselectcas_monitor_t, evptrig_new_base);
  ul_evpbase_select_t *eb = UL_CONTAINEROF(mon->my_base, ul_evpbase_select_t, base);

  if(!(what & UL_EVP_DONE))
    return;
  ul_evpselectcas_monitor_done(mon);

  eb->cascade_base = NULL;
  eb->base.ops = &ul_evpoll_ops_select;

  if(eb->cascade_inerit_destroy_fl)
    ul_evpoll_destroy(&eb->base);
}

static int ul_evpselect_base_cascade(ul_evpbase_t *base, ul_evpbase_t *new_base, int prioshift, int bc_flags)
{
  ul_evptrig_select_t *ei;
  ul_evpbase_select_t *eb = UL_CONTAINEROF(base, ul_evpbase_select_t, base);
  ul_evpselectcas_monitor_t *mon;
  ul_evptrig_t *evptrig;
  int res;
  int ret = 0;

  ul_evpoll_cb_t cb;
  unsigned       flags;
  unsigned       what_events;
  ul_evfd_t      fd;
  ul_htim_diff_t timeout;
  ul_htim_time_t exptime;

  while(1) {
    ei = ul_evpselect_idle_cut_first(eb);
    if(ei == NULL) {
      ei = ul_evpselect_active_cut_first(eb);
      if(ei == NULL) {
        ei = ul_evpselect_report_cut_first(eb);
        if(ei == NULL)
          break;
      }
    }
    evptrig = ei->trig_ptr;
    cb = evptrig->cb;
    flags = ei->flags;
    what_events = ei->what_events;
    fd = ei->fd;
    timeout = ei->timeout;
    exptime = ul_evpselect_htimer_get_expire(ei);

    ul_evpselect_trig_done(evptrig);

    res = ul_evptrig_init(new_base, evptrig);
    if(res < 0) {
      ul_logerr("ul_evpselect_base_cascade: trig reinsertion failded for fd %ld\n",
                (long) fd);
      ret = -1;
      continue;
    }

    ul_evptrig_set_callback(evptrig, cb);
    if((what_events & (UL_EVP_IN | UL_EVP_OUT | UL_EVP_STATE)) && (fd >= 0))
      if(ul_evptrig_set_fd(evptrig, fd, what_events) < 0)
        goto cascade_failed;
    if(flags & UL_EVPSELECT_TIMEOUT) {
      if(ul_evptrig_set_timeout(evptrig, &timeout) < 0)
        goto cascade_failed;
    } else if(flags & UL_EVPSELECT_TIMED) {
        if(ul_evptrig_set_time(evptrig, &exptime) < 0)
          goto cascade_failed;
    }
    if(flags & UL_EVPSELECT_ARMED) {
      if(flags & UL_EVPSELECT_ONCE) {
        if(ul_evptrig_arm_once(evptrig) < 0)
          goto cascade_failed;
      } else {
        if(ul_evptrig_arm(evptrig) < 0)
          goto cascade_failed;
      }
    }
    continue;

   cascade_failed:
    ul_logerr("ul_evpselect_base_cascade: parameters mimic failed fd %ld\n",
                (long) fd);
  }

  while((ei = ul_evpselect_idle_cut_first(eb))!=NULL) {
    ul_evpselect_trig_done(ei->trig_ptr);
  }

  ul_evpselect_free_fds(eb);

  mon = malloc(sizeof(*mon));
  if(mon != NULL) {
    memset(mon, 0, sizeof(*mon));
    ul_evptrig_init(base, &mon->evptrig_my_base);
    ul_evptrig_set_callback(&mon->evptrig_my_base, ul_evpselectcas_monitor_cb_my);
    ul_evptrig_init(new_base, &mon->evptrig_new_base);
    ul_evptrig_set_callback(&mon->evptrig_new_base, ul_evpselectcas_monitor_cb_new);
    mon->my_base = base;
  }

  eb->cascade_base = new_base;
  eb->base.ops = &ul_evpoll_ops_selectcas;

  eb->cascade_inerit_destroy_fl = (bc_flags & UL_EVP_CASFL_INHERIT_DESTROY)? 1: 0;
  eb->cascade_propagate_destroy_fl = (bc_flags & UL_EVP_CASFL_PROPAGATE_DESTROY)? 1: 0;

  return ret;
}

#endif /*UL_EVP_SELECT_CASCADE*/
